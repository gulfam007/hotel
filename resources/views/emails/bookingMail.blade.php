<div>

    Bom dia,

    Favor reservar como segue:


    @if  (isset($booking['room']))

        <table cellspacing="0" style="margin-left:25px;margin-top:10px">
            <tbody>

            <tr>
                <th style="padding:5px;border:1px solid black">HOTEL</th>
                <th style="padding:5px;border:1px solid black">Checkin</th>
                <th style="padding:5px;border:1px solid black">Voo C</th>
                <th style="padding:5px;border:1px solid black">Hora C</th>
                <th style="padding:5px;border:1px solid black">Nº A</th>
                <th style="padding:5px;border:1px solid black">Nº C</th>
                {{--<th style="padding:5px;border:1px solid black">Saída</th>--}}
                <th style="padding:5px;border:1px solid black">Checkout</th>
                <th style="padding:5px;border:1px solid black">Voo S</th>
                <th style="padding:5px;border:1px solid black">Hora S</th>
                <th style="padding:5px;border:1px solid black">Nome</th>
                <th style="padding:5px;border:1px solid black">Essen</th>
                <th style="padding:5px;border:1px solid black">Nº Reserva</th>
                <th style="padding:5px;border:1px solid black">Estado</th>
                <th style="padding:5px;border:1px solid black">Tip. Quarto</th>
                <th style="padding:5px;border:1px solid black">Nº Noites</th>
                <th style="padding:5px;border:1px solid black">Obs</th>
                <th style="padding:5px;border:1px solid black">Data de Reserva</th>
                <th style="padding:5px;border:1px solid black">Pacotes</th>
                  <th style="padding:5px;border:1px solid black">Idades</th>
            </tr>

            @foreach ($booking['room'] as $room)


                <tr>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->hotel  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->check_in  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->departure_flight:''  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->departure_time:''  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->adults  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->kids  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->check_out  }}</td>
                    {{--<td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->arrival_date:''  }}</td>--}}
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->arrival_flight:''  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->arrival_time:''  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->members  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->essen  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->booking_number:''  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">

                        @if ($booking['booking'][0]->state == 'BU')
                            Nova reserva
                        @elseif ($booking['booking'][0]->state == 'UN')
                            Favor notar Alteração
                        @elseif($booking['booking'][0]->state == 'ST')
                            Favor notar Cancelamento
                        @elseif($booking['booking'][0]->state == 'US')
                            US
                        @endif


                    </td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->room_type  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->nights  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->room_notes  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->booking_date:''  }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ !empty($room->packages )?$room->packages :' ' }}</td>
                    <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->ages  }}</td>

                </tr>

            @endforeach


            </tbody>
        </table>

    @endif

    <br>

    @if( isset($booking['car']) )

        <table cellspacing="0" style="margin-left:25px;margin-top:10px">
            <tbody>

            <tr>
                <th style="padding:5px;border:1px solid black">RentaCar</th>
                <th style="padding:5px;border:1px solid black">Nomes</th>
                <th style="padding:5px;border:1px solid black">Chegada</th>
                <th style="padding:5px;border:1px solid black">Voo C</th>
                <th style="padding:5px;border:1px solid black">Hora C</th>
                <th style="padding:5px;border:1px solid black">Voo S</th>
                <th style="padding:5px;border:1px solid black">Hora S</th>
                <th style="padding:5px;border:1px solid black">Partida</th>
                <th style="padding:5px;border:1px solid black">Nº Reserva</th>
                <th style="padding:5px;border:1px solid black">Estado</th>
                <th style="padding:5px;border:1px solid black">Tipo</th>
                {{--<th style="padding:5px;border:1px solid black">Valor</th>--}}
                <th style="padding:5px;border:1px solid black">Obs</th>
                <th style="padding:5px;border:1px solid black">Data de Reserva</th>
            </tr>


            <tr>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['car']?$booking['car']->rentacar :'' }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking->members  }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->arrival_date:''  }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->departure_flight:''  }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->departure_time:'' }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->arrival_flight:''  }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->arrival_time:''  }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{  $booking['booking'][0]?$booking['booking'][0]->departure_date :'' }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking'][0]?$booking['booking'][0]->booking_number:''  }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">

                    @if ($booking['booking'][0]->state == 'BU')
                        Nova reserva
                    @elseif ( $booking['booking'][0]->state == 'UN')
                        Favor notar Alteração
                    @elseif($booking['booking'][0]->state == 'ST')
                        Favor notar Cancelamento
                    @elseif($booking['booking'][0]->state == 'US')
                        US
                    @endif

                </td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['car']?$booking['car']->type:''  }}</td>
                {{--<td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['car']?$booking['car']->price:''  }}</td>--}}
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['car']?$booking['car']->notes :'' }}</td>
                <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $booking['booking']?$booking['booking'][0]->booking_date:''  }}</td>

            </tr>


            </tbody>
        </table>

    @endif

    @if  ( isset($booking[0]['room']) )

        <table cellspacing="0" style="margin-left:25px;margin-top:10px">
            <tbody>

            <tr>
                <th style="padding:5px;border:1px solid black">HOTEL</th>
                <th style="padding:5px;border:1px solid black">Checkin</th>
                <th style="padding:5px;border:1px solid black">Voo C</th>
                <th style="padding:5px;border:1px solid black">Hora C</th>
                <th style="padding:5px;border:1px solid black">Nº A</th>
                <th style="padding:5px;border:1px solid black">Nº C</th>
                {{--<th style="padding:5px;border:1px solid black">Saída</th>--}}
                <th style="padding:5px;border:1px solid black">Checkout</th>
                <th style="padding:5px;border:1px solid black">Voo S</th>
                <th style="padding:5px;border:1px solid black">Hora S</th>
                <th style="padding:5px;border:1px solid black">Nome</th>
                <th style="padding:5px;border:1px solid black">Essen</th>
                <th style="padding:5px;border:1px solid black">Nº Reserva</th>
                <th style="padding:5px;border:1px solid black">Estado</th>
                <th style="padding:5px;border:1px solid black">Tip. Quarto</th>
                <th style="padding:5px;border:1px solid black">Nº Noites</th>
                <th style="padding:5px;border:1px solid black">Obs</th>
                <th style="padding:5px;border:1px solid black">Data de Reserva</th>
                <th style="padding:5px;border:1px solid black">Pacotes</th>
                 <th style="padding:5px;border:1px solid black">Idades</th>
            </tr>
            @endif

            @if ( isset($booking[0]['car']) )

                <table cellspacing="0" style="margin-left:25px;margin-top:10px">
                    <tbody>

                    <tr>
                        <th style="padding:5px;border:1px solid black">RentaCar</th>
                        <th style="padding:5px;border:1px solid black">Nomes</th>
                        <th style="padding:5px;border:1px solid black">Chegada</th>
                        <th style="padding:5px;border:1px solid black">Voo C</th>
                        <th style="padding:5px;border:1px solid black">Hora C</th>
                        <th style="padding:5px;border:1px solid black">Voo S</th>
                        <th style="padding:5px;border:1px solid black">Hora S</th>
                        <th style="padding:5px;border:1px solid black">Partida</th>
                        <th style="padding:5px;border:1px solid black">Nº Reserva</th>
                        <th style="padding:5px;border:1px solid black">Estado</th>
                        <th style="padding:5px;border:1px solid black">Tipo</th>
                        {{--<th style="padding:5px;border:1px solid black">Valor</th>--}}
                        <th style="padding:5px;border:1px solid black">Obs</th>
                        <th style="padding:5px;border:1px solid black">Data de Reserva</th>
                    </tr>

                    @endif


                    @if( isset($booking[0]) )

                        @foreach($booking as $index => $item)

                            @foreach($booking[$index]['booking'] as $book)

                                @if( isset( $booking[$index]['room'] ) )

                                    @foreach ($booking[$index]['room'] as $room)


                                        <tr>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->hotel  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->check_in  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->departure_flight  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->departure_time }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->adults  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->kids  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->check_out  }}</td>
                                            {{--<td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->arrival_date  }}</td>--}}
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->arrival_flight  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->arrival_time  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->members  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->essen  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->booking_number  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">

                                                @if ($book->state  == 'BU')
                                                    Nova reserva
                                                @elseif ($book->state  == 'UN')
                                                    Favor notar Alteração
                                                @elseif($book->state  == 'ST')
                                                    Favor notar Cancelamento
                                                @elseif($book->state  == 'US')
                                                    US
                                                @endif

                                            </td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->room_type  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->nights  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->room_notes  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->booking_date  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ !empty($room->packages )?$room->packages :' '  }}</td>
                                              <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $room->ages  }}</td>

                                        </tr>

                                    @endforeach

                                @endif

                                @if( isset( $booking[$index]['car'] ) )
                                    @foreach ($booking[$index]['car'] as $car)
                                        <tr>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $car->rentacar  }}</td>

                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->members  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->arrival_date  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->departure_flight  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->departure_time  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->arrival_flight  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->arrival_time  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->departure_date  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->booking_number  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">
                                                @if ($book->state  == 'BU')
                                                    Nova reserva
                                                @elseif ($book->state  == 'UN')
                                                    Favor notar Alteração
                                                @elseif($book->state  == 'ST')
                                                    Favor notar Cancelamento
                                                @elseif($book->state  == 'US')
                                                    US
                                                @endif
                                            </td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $car->type  }}</td>
                                            {{--<td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $car->price  }}</td>--}}
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $car->notes  }}</td>
                                            <td style="padding:5px;border:1px solid;border-collapse:collapse">{{ $book->booking_date  }}</td>

                                        </tr>
                                    @endforeach

                                @endif

                            @endforeach

                        @endforeach


                    </tbody>
                </table>







    @endif


</div>

@if($errors->count() > 0)
    <p>erros:</p>

    <ul>
        @foreach($errors->all() as $message)
            <li>{{$message}}</li>
        @endforeach
    </ul>
@endif