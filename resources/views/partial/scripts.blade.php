<!-- BEGIN VENDOR JS -->
<script src="{{ asset ('plugins/feather-icons/feather.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/tether/js/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ asset ('plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/classie/classie.js') }}"></script>
<script src="{{ asset ('plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-validation/js/localization/messages_pt_PT.min.js') }}" type="text/javascript"></script>

<script src="{{ asset ('plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/jquery-autonumeric/autoNumeric.js') }}"></script>

<script type="text/javascript" src="{{ asset ('plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/jquery-inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ asset ('plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/summernote/js/summernote.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset ('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset ('plugins/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset ('plugins/bootstrap-typehead/typeahead.bundle.min.js') }}"></script>
<script src="{{ asset ('plugins/bootstrap-typehead/typeahead.jquery.min.js') }}"></script>
<script src="{{ asset ('plugins/handlebars/handlebars-v4.0.5.js') }}"></script>
<script src="{{ asset ('plugins/bootstrap-collapse/bootstrap-tabcollapse.js') }}" type="text/javascript"></script>

<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{ asset ('pages/js/pages.js') }}"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="{{ asset ('js/notifications.js') }}" type="text/javascript"></script>
<script src="{{ asset ('js/scripts.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function() {

        $(".dateMask").mask("99/99/9999");
        $(".timeMask").mask("99:99");
        $('.autonumeric').autoNumeric('init');

        $('.timePicker').timepicker();
        // $('.timePicker').timepicker().on('show.timepicker', function(e) {
        //     var widget = $('.bootstrap-timepicker-widget');
        //     widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
        //     widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
        // });
    });


</script>
<!-- END PAGE LEVEL JS -->

