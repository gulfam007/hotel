<form id="editBooking" role="form" class="frm"  autocomplete="off" novalidate="novalidate">



    <p>Dados da Reserva</p>
    <div container="bookings" class="form-group-attached">
        <div class="row clearfix">
            <div class="col-md-3">
                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">TOM</label>
                    <select id="tms_id" name="tms_id" class="form-control full-width initSelect2" >
                        <option value="0"></option>
                        @foreach($tomOptions as $tomOption)
                            <option value="{{ trim($tomOption->id) }}">
                                {{ trim($tomOption->text) }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Nº Reserva</label>
                    <input type="text" class="form-control" id="booking" name="book_number" 
                           >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>Data da Reserva</label>
                    <input type="text" class="form-control dateMask" id="bookingdate" name="book_date"
                            >
                </div>
            </div>
            <div class="col-md-2">

                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">Estado</label>
                    <select id="state" name="state" class="full-width initSelect2" >
                        <option value=""></option>
                        <option value="BU">BU</option>
                        <option value="UN">UN</option>
                        <option value="US">US</option>
                        <option value="ST">ST</option>

                    </select>
                </div>


            </div>

            <div class="col-md-2">

                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">Transfer</label>
                    <select id="transfer" name="transfer" class="full-width initSelect2" >
                        <option value="NaoTem">Não Tem</option>
                        <option value="IN">IN</option>
                        <option value="OUT">OUT</option>
                        <option value="IN+OUT">IN+OUT</option>
                        <option value="Verificar">Verificar</option>
                    </select>
                </div>


            </div>


        </div>


    </div>
    <p class="m-t-10">Dados do Cliente  <a id="addClient" onclick="$('#addClientModal').modal();" class="fa fs-16 fa-plus-circle text-danger m-r-10"></a> </p>


    <div id="clientContainer" container="clients" class="form-group-attached">


    </div>


    <p class="m-t-10">Dados do Voo</p>
    <div container="flights" rid="" id="flights" class="form-group-attached">





        <div class="row ">
            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>Chegada</label>
                    <input type="text" class="form-control dateMask" id="arrivalDate" name="arrival_date"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Voo </label>
                    <input type="text" class="form-control" id="arrivalFlight" name="arrival_flight"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Hora</label>
                    <input type="text" class="form-control timeMask" id="time" name="arrival_time"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Dia</label>
                    <input type="text" class="form-control" id="day" name="day" disabled >
                </div>
            </div>




        </div>

        <div class="row clearfix">
            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>Partida</label>
                    <input type="text" class="form-control dateMask" id="departureDate" name="departure_date"  >
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Voo</label>
                    <input type="text" class="form-control" id="departureFlight" name="departure_flight"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Hora</label>
                    <input type="text" class="form-control timeMask" id="departureTime" name="departure_time" 
                           >
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Destino</label>
                    <input type="text" class="form-control" id="destiny" name="departure_location" 
                           >
                </div>
            </div>
        </div>


    </div>

    <!--    <p class="m-t-10">Dados da Hotel</p>
        <div class="form-group-attached">
            <div class="row clearfix">
                <div class="col-md-5">
                    <div class="form-group form-group-default ">
                        <label>Hotel</label>
                        <input type="text" class="form-control" id="hotel" name="hotel" 
                               >
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group form-group-default ">
                        <label>Essen</label>
                        <input type="text" class="form-control" id="essen" name="essen" 
                               >
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group form-group-default ">

                    </div>
                </div>


            </div>
        </div>
        -->


    <p class="m-t-10">Quartos  <a id="addRoom" onclick="$('#addRoomModal').modal();" class="fa fs-16 fa-plus-circle text-danger m-r-10"></a> </p>
    <div id="roomContainer" class="">

    </div>


    <p class="m-t-10">Pacotes  <a id="addPackage" onclick="$('#addPackageModal').modal();" class="fa fs-16 fa-plus-circle text-danger m-r-10"></a> </p>
    <div container="packages" class="form-group-attached">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="form-group form-group-default"><label></label>
                    <input class="tagsinput custom-tag-input" type="text" value="hello World, quotes, inspiration" style="display: none;">
                    <div class="bootstrap-tagsinput" id="packagesContainer">

                        <input size="1" type="text">
                    </div>
                </div>
            </div>


        </div>
    </div>

    <p class="m-t-10">Rent-a-Car</p>
    <div container="car" class="form-group-attached">

        <div class="row clearfix">

            <div class="col-md-2">


                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">Tip. Auto</label>
                    <select id="type" name="type" class="full-width initSelect2" >
                        <option value="null">Selecione...</option>
                        <option value="A">Grupo A</option>
                        <option value="B">Grupo B</option>
                        <option value="C">Grupo C</option>
                        <option value="D">Grupo D</option>
                        <option value="E">Grupo E</option>
                        <option value="MWF">Grupo F</option>
                    </select>
                </div>


            </div>

            <div class="col-md-4">
                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">Serviço RC</label>
                    <select id="rentacar_id" name="rentacar_id" class="full-width initSelect2" >
                        <option value="null">Selecione...</option>
                        @foreach($rentacarOptions as $rentacarOption)
                            <option value="{{ trim($rentacarOption->id) }}">
                                {{ trim($rentacarOption->text) }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>


                <div class="col-md-2">
                    <div class="form-group form-group-default " >
                        <label>Valor</label>
                        <input type="text" class="form-control" id="price" name="price">
                    </div>
                </div>


        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-group-default ">
                    <label>OBS</label>

                    <textarea id="rentACarNotes" name="notes" class="form-control" rows="5">
                                        </textarea>
                </div>
            </div>
        </div>
    </div>
    <br>


</form>


<div class="col-md-9">
    <button class="btn btn-danger" onclick="saveEditModal()" type="button">Gravar</button>
    <button class="btn btn-default"  data-dismiss="modal"> Fechar</button>
</div>