

    <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
        <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu">
        </a>
        <ul>
            <li>
                <a href="{{ route('home') }}">Home</a>
            </li>
            <li class="">
                <a href="{{route('hotel.index')}}">Hotel</a>
            </li>

            <li>
                <a href="javascript:;"><span class="title">Reservas</span>
                    <span class=" arrow"></span></a>
                <ul class="">
                    <li class="">
                        <a href="{{ route('lista') }}">Lista</a>
                    </li>
                    <li class="">

                        @if( !empty($findImport))

                            <a href="{{ route('minhaImportacao') }}">Minha Importação</a>

                            @else

                            <a href="{{ route('importarReservas') }}">Importar Reservas</a>

                        @endif



                    </li>
                    <li class="">
                        <a href="{{ route('reset') }}">Reset de Base de Dados</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="index.html">Exportar Reservas</a>
            </li>

            <li>
                <a href="index.html">Alterar Voos</a>
            </li>

            <li>
                <a href="javascript:;"><span class="title">Gestão de Entidades</span>
                    <span class=" arrow"></span></a>
                <ul class="">

                    <li class="">
                        <a href="calendar.html">Pacotes</a>
                    </li>

                    <li class="">
                        <a href="calendar_lang.html">Guias</a>
                    </li>

                    <li class="">
                        <a href="calendar_month.html">Hotéis</a>
                    </li>

                    <li class="">
                        <a href="calendar_month.html">TMS</a>
                    </li>


                    <li class="">
                        <a href="calendar_month.html">Rent-a-Car</a>
                    </li>

                    <li class="">
                        <a href="calendar_month.html">Localidades</a>
                    </li>

                    <li class="">
                        <a href="calendar_month.html">Preis</a>
                    </li>

                    <li class="">
                        <a href="calendar_month.html">Preis HF</a>
                    </li>

                </ul>
            </li>

            <li>
                <a href="javascript:;"><span class="title">Gestão de Email</span>
                    <span class=" arrow"></span></a>
                <ul class="">
                    <li class="">
                        <a href="calendar.html">Configurações Hotéis</a>
                    </li>
                    <li class="">
                        <a href="calendar_lang.html">Configurações Rent-a-Car</a>
                    </li>
                    <li class="">
                        <a href="calendar_month.html">Caixa de Saída</a>
                    </li>

                </ul>
            </li>





        </ul>

    </div>

