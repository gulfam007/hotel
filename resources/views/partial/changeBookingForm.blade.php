<form id="changeBooking" role="form" autocomplete="off" novalidate="novalidate">




    <p>Dados da Alteraçao</p>
    <div container="changeBookings" class="form-group-attached">
        <div class="row clearfix">

            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>TOM</label>
                    <input type="text" class="form-control" id="historytom" name="historytom"  >
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Nº Reserva</label>
                    <input type="text" class="form-control" id="historynumber" name="historynumber" 
                           >
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>Data da Reserva</label>
                    <input type="text" class="form-control dateMask" id="historydate" name="book_date"
                            >
                </div>
            </div>

            <div class="col-md-2">

                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">Estado</label>
                    <select id="historystate" name="historystate" class="full-width initSelect2" >
                        <option value=""></option>
                        <option value="BU">BU</option>
                        <option value="UN">UN</option>
                        <option value="US">US</option>
                        <option value="ST">ST</option>

                    </select>
                </div>


            </div>

            <div class="col-md-2">

                <div class="form-group form-group-default form-group-default-select2 ">
                    <label class="">Transfer</label>
                    <select id="historyTransfer" name="historyTransfer" class="full-width initSelect2" >
                        <option value="Naotem">Não Tem</option>
                        <option value="IN">IN</option>
                        <option value="OUT">OUT</option>
                        <option value="IN+OUT">IN+OUT</option>
                        <option value="Verificar">Verificar</option>

                    </select>
                </div>


            </div>


        </div>





    </div>




    <p class="m-t-10">Dados do Clients</p>
    <div container="historyClients" rid="" id="historyClientContainer" class="form-group-attached">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="form-group form-group-default ">
                    <label>Nome</label>
                    <input type="text" class="form-control " id="historyClient" name="historyClient"  >
                </div>
            </div>
        </div>

        <div class="row clearfix">

            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Idade</label>
                    <input type="text" class="form-control " id="historyAge" name="historyAge"  >
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group form-group-default ">
                    <label>Gênero</label>
                    <input type="text" class="form-control " id="historyGender" name="historyGender"  >
                </div>
            </div>


        </div>


    </div>




    <p class="m-t-10">Dados do Voo</p>
    <div container="historyflights" rid="" id="historyflights" class="form-group-attached">
        <div class="row ">
            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>Chegada</label>
                    <input type="text" class="form-control dateMask" id="historydepartureDate" name="historydepartureDate"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Voo </label>
                    <input type="text" class="form-control" id="historyarrivalFlight" name="historyarrivalFlight"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Hora</label>
                    <input type="text" class="form-control timeMask" id="historytime" name="historytime"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Destino</label>
                    <input type="text" class="form-control" id="historyarrivalLocation" name="historyarrivalLocation" 
                           >
                </div>
            </div>

        </div>
        <div class="row clearfix">
            <div class="col-md-3">
                <div class="form-group form-group-default ">
                    <label>Partida</label>
                    <input type="text" class="form-control dateMask" id="historyarrivalDate" name="historyarrivalDate"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Voo</label>
                    <input type="text" class="form-control" id="historydepartureFlight" name="historydepartureFlight"  >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Hora</label>
                    <input type="text" class="form-control timeMask" id="historydepartureTime" name="historydepartureTime" 
                           >
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group form-group-default ">
                    <label>Local</label>
                    <input type="text" class="form-control" id="historyDestiny" name="historyDestiny" 
                           >
                </div>
            </div>

        </div>


    </div>



       <p class="m-t-10">Quartos </p>
        <div class="form-group-attached">
            <div class="row clearfix">
                <div class="col-md-3">
                    <div class="form-group form-group-default ">
                        <label>Hotel</label>
                        <input type="text" class="form-control" id="historyHotel" name="historyHotel" 
                               >
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group form-group-default ">
                        <label>Cód Hotel</label>
                        <input type="text" class="form-control" id="historyHotelCode" name="historyHotelCode" 
                               >
                    </div>
                </div>



                <div class="col-md-2">
                    <div class="form-group form-group-default ">
                        <label>Cód Quarto</label>
                        <input type="text" class="form-control" id="historyRoomCode" name="historyRoomCode" 
                               >
                    </div>
                </div>






            </div>


<div class="row">

    <div class="col-md-5">
        <div class="form-group form-group-default ">
            <label>Quarto</label>
            <input type="text" class="form-control" id="historyRoom" name="historyRoom" 
                   >
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default ">
            <label>Noites</label>
            <input type="text" class="form-control" id="historyNights" name="historyNights" 
                   >
        </div>
    </div>



</div>

            <div class="row clearfix">

                <div class="col-md-3">
                    <div class="form-group form-group-default ">
                        <label>Checkin</label>
                        <input type="text" class="form-control" id="historyCheckIn" name="historyCheckIn" 
                               >
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group form-group-default ">
                        <label>Checkout</label>
                        <input type="text" class="form-control" id="historyCheckOut" name="historyCheckOut" 
                               >
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group form-group-default ">
                        <label>Valor</label>
                        <input type="text" class="form-control" id="historyValue" name="historyValue" 
                               >
                    </div>
                </div>



            </div>

            <div class="row">

                <div class="col-md-2">
                    <div class="form-group form-group-default ">
                        <label>Dia</label>
                        <input type="text" class="form-control" id="historyDay" name="historyDay" 
                               >
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group form-group-default ">
                        <label>Qtd Quartos</label>
                        <input type="text" class="form-control" id="historyRoomsQty" name="historyRoomsQty" 
                               >
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group form-group-default ">
                        <label>Membros</label>
                        <input type="text" class="form-control" id="historyMembers" name="historyMembers" 
                               >
                    </div>
                </div>

            </div>

        </div>




    <p class="m-t-10">OBS</p>
    <div container="historyObs" class="form-group-attached">

        <div class="row">
            <div class="col-md-8">
                <div class="form-group form-group-default ">
                    <label>OBS</label>

                    <textarea id="historyNotes" class="form-control" rows="5">
                                        </textarea>
                </div>
            </div>
        </div>

    </div>
    <br>


</form>



{{--<div class="col-md-9">--}}
    {{--<button class="btn btn-danger" onclick="updateBooking()" type="button">Gravar</button>--}}
    {{--<button class="btn btn-default"  data-dismiss="modal"> Fechar</button>--}}
{{--</div>--}}