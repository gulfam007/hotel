<div class="modal fade slide-up" id="bookingEditModal"  role="dialog" style="display: none;"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content" style="width: auto">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                    </button>
                    <h5>Reserva <span class="semi-bold"  id="bookingNumber">B0000</span></h5>

                </div>
                <div class="modal-body">

                    <div class="row">

                        <div id="editBookingDiv" class="col-md-6">

                            @include('partial.editBookingForm')

                        </div>

                        <div id="changeBookingDiv" style="display:none" class="col-md-6">

                            @include('partial.changeBookingForm')



                        </div>

                    </div>




                </div>
            </div>
        </div>

    </div>
</div>

{{-- REMOVE CLIENT MODAL --}}

<div class="modal fade slide-up disable-scroll" id="removeClientModal"  role="dialog"
     style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                    </button>
                    <h5>Remover Cliente</h5>
                </div>
                <div class="modal-body">
                    <p class="no-margin">
                        Deseja remover este cliente?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="removeClientButton"
                            class="btn btn-danger btn-cons  pull-left inline">Remover
                    </button>
                    <button type="button" class="btn btn-default btn-cons no-margin pull-left inline"
                            data-dismiss="modal">Fechar
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>

{{-- REMOVE ROOM MODAL --}}

<div class="modal fade slide-up disable-scroll" id="removeRoomModal"  role="dialog"
     style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                class="pg-close fs-14"></i>
                    </button>
                    <h5>Remover Quarto</h5>
                </div>
                <div class="modal-body">
                    <p class="no-margin">
                        Deseja remover este Quarto?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="removeRoomButton"
                            class="btn btn-danger btn-cons  pull-left inline" >Remover
                    </button>
                    <button type="button" class="btn btn-default btn-cons no-margin pull-left inline"
                            data-dismiss="modal">Fechar
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>


{{-- ADD CLIENT MODAL --}}

<div class="modal fade slide-up disable-scroll " id="addClientModal"  role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Adicionar Cliente</span></h5>

                </div>
                <div class="modal-body">

                    <form role="form" method="POST" id="clientModalForm">

                        <div class="form-group-attached">

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Nome</label>
                                        <input  type="text" name="c_name" id="c_name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group form-group-default">
                                        <label>Idade</label>
                                        <input maxlength="2" type="text" name="c_age" id="c_age" class="form-control" required>
                                    </div>
                                </div>

                                <div class="col-md-2">

                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Gênero</label>
                                        <select  name="c_gender" id="c_gender" class="full-width initSelect2" >
                                            <option value=""></option>
                                            <option value="H">H</option>
                                            <option value="F">F</option>
                                            <option value="K">K</option>
                                        </select>
                                    </div>


                                </div>



                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Quarto</label>
                                        <select name="c_room" id="c_room" class="full-width required" required>
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>


                    <div class="row">

                        <div class="col-md-4 m-t-10 sm-m-t-10">
                            <button type="button" id="c_button" onclick="createClient();" class="btn btn-danger btn-block m-t-5">Gravar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>



{{-- ADD ROOM MODAL --}}

<div class="modal fade slide-up disable-scroll " id="addRoomModal"  role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Adicionar Quarto</span></h5>

                </div>
                <div class="modal-body">
                    <form role="form" id="roomModalForm">
                        <div class="form-group-attached">


                            <div class="row">



                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Tipo de Quarto</label>
                                        <input type="text" required id="r_room" class="form-control">
                                    </div>
                                </div>



                                <div class="col-md-2">
                                    <div class="form-group form-group-default">
                                        <label>Código</label>
                                        <input type="text" required id="r_code" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group form-group-default">
                                        <label>Noites</label>
                                        <input type="text" required id="r_nights" class="form-control">
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-7">

                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Hotel</label>
                                        <select id="r_hotel" required class="full-width initSelect2" >
                                            <option value="0"></option>
                                            @foreach($hotelOptions as $roomOption)
                                                <option value="{{ trim($roomOption->id) }}">
                                                    {{ trim($roomOption->text) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>


                                <div class="col-md-2">
                                    <div class="form-group form-group-default">
                                        <label>Essen</label>
                                        <input id="r_essen" required class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Valor</label>
                                        <input id="r_price"  required type="text" data-a-dec="," data-a-sep="." class="autonumeric form-control">
                                    </div>
                                </div>

                            </div>

                            <div class="row">




                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Checkin</label>
                                        <input id="r_checkin" required class="form-control dateMask">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Checkout</label>
                                        <input id="r_checkout" required class="form-control dateMask">
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        <label>Obs</label>
                                        <textarea class="form-control" id="r_notes"  rows="5"></textarea>
                                    </div>
                                </div>



                            </div>

                        </div>
                    </form>
                    <div class="row">

                        <div class="col-md-4 m-t-10 sm-m-t-10">
                            <button type="button" onclick="createRoom()" id="r_button" class="btn btn-danger btn-block m-t-5">Gravar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

{{-- ADD PACKAGE MODAL --}}

<div class="modal fade slide-up disable-scroll " id="addPackageModal"  role="dialog" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                    </button>
                    <h5><span class="semi-bold">Adicionar Pacote</span></h5>

                </div>
                <div class="modal-body">
                    <form role="form" action="POST" id="packageModalForm">
                        <div class="form-group-attached">

                            <div class="row">

                                <div class="col-md-10">

                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Pacote</label>
                                        <select id="p_package" class="full-width initSelect2" >
                                            <option value="0"></option>
                                            @foreach($packageOptions as $packageOption)
                                                <option value="{{ trim($packageOption->id) }}">
                                                    {{ trim($packageOption->text) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>




                                <div class="col-md-2">

                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Estado</label>
                                        <select id="p_state" class="full-width initSelect2" >
                                            <option value="0"></option>
                                            <option value="BU">BU</option>
                                            <option value="UN">UN</option>
                                            <option value="US">US</option>
                                            <option value="ST">ST</option>

                                        </select>
                                    </div>


                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>CheckIn</label>
                                        <input type="text" id="p_checkIn" class="form-control dateMask">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>CheckOut</label>
                                        <input type="text" id="p_checkOut" class="form-control dateMask">
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-9">

                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Hotel</label>
                                        <select id="p_hotel" class="full-width initSelect2" >
                                            <option value="0"></option>
                                            @foreach($hotelOptions as $roomOption)
                                                <option value="{{ trim($roomOption->id) }}">
                                                    {{ trim($roomOption->text) }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-group-default">
                                        <label>Valor</label>
                                        <input  id="p_price"  type="text" data-a-dec="," data-a-sep="." class="autonumeric form-control" >
                                    </div>
                                </div>

                            </div>

                            <div id="tuachen_sec" style="display:none" class="row">

                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Tuachen Pax</label>
                                        <input type="text" id="p_pax" class="form-control">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>Tuachen Quantidade</label>
                                        <input type="text" id="p_tqty" class="form-control">
                                    </div>
                                </div>

                            </div>

                            <div  class="row">

                                <div class="col-md-12">

                                    <div class="form-group form-group-default form-group-default-select2 ">
                                        <label class="">Cliente</label>
                                        <select id="p_clients" class="full-width initSelect2" >
                                            <option value="0"></option>

                                        </select>
                                    </div>


                                </div>




                            </div>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        <label>Obs</label>
                                        <textarea class="form-control" id="p_notes"  rows="5"></textarea>
                                    </div>
                                </div>



                            </div>

                        </div>
                    </form>
                    <div class="row">

                        <div class="col-md-4 m-t-10 sm-m-t-10">
                            <button type="button" onclick="createPackage()" id="p_button" class="btn btn-danger btn-block m-t-5">Gravar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

