@extends('layouts.app')

@section('content')
    <title>Hotel | View</title>

<div class="container">
    @if ((Session::has('success-message')))
        <div class="alert alert-success col-md-12">{{
					Session::get('success-message') }}</div>
    @endif @if ((Session::has('fail-message')))
        <div class="alert alert-danger col-md-12">{{
					Session::get('fail-message') }}</div>
    @endif
        <div class="container">
        <div class="row">
        <div class="col-md-3">
            <a href="{{route('hotel.create')}}" id="cancel" name="cancel" class="btn btn-success">Add New Hotel</a>

        </div>
            <div class="col-md-6"></div>
                <div class="col-md-3">

            <div class="agileits_search">
            <form action="{{route('hotelSearch')}}" method="POST">
                {{csrf_field()}}
                <input name="name" type="search" placeholder="Search hotel name" required="">
                <button type="submit" class="btn btn-success" aria-label="Left Align">
                    <span class="fa fa-search" aria-hidden="true"> </span>
                </button>
            </form>
        </div>
                </div>
        </div>
        </div>
<div class="table-responsive" style="overflow: auto; height: 500px">
    <table class="table">
        <thead>
        <tr class="active">
            <th scope="col">ID</th>
            <th scope="col">CODIGO</th>
            <th scope="col">NAME</th>
            <th scope="col">EMAIL</th>
            <th scope="col">GUIDE</th>
            <th scope="col">LOCATION</th>
            {{--<th scope="col">PRICE</th>--}}
            {{--<th scope="col">STATUS</th>--}}
            <th scope="col">ACTION</th>
        </tr>
        </thead>
        <tbody>
        @forelse($hotels as $hotel)

            <tr>
            <th scope="row">{{$hotel->id}}</th>
            <td>{{$hotel->code}}</td>
            <td>{{$hotel->name}}</td>
            <td>{{$hotel->email}}</td>
            <td>{{$hotel->guide_id}}</td>
            <td>{{$hotel->location_id}}</td>
            {{--<td>{{$hotel->transfer_price_mod}}</td>--}}
                     {{--@if($hotel->active==1)--}}
                    {{--<td><span class="label-default label label-success">Active</span></td>--}}
                    {{--@else--}}
                    {{--<td><span class="label-default label label-danger">Inactive</span></td>--}}
                    {{--@endif--}}
            <td>
                <form action="{{route('hotel.destroy',$hotel->id)}}"  method="POST">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button type="submit" class="btn btn-sm btn-danger "><i class="fa fa-remove" style="font-size: 15px;"></i></button>
                </form>
                <a href="{{route('hotel.edit',$hotel->id)}}" class="btn btn-sm btn-success "><i class="fa fa-edit" style="font-size: 11px;"></i></a>

            </td>
        </tr>
        @empty
        @endforelse
        </tbody>
    </table>
</div>
</div>
@endsection