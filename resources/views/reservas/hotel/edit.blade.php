@extends('layouts.app')

@section('content')
    <title>Hotel | Edit</title>
    <div class="container">
        <div class="row">

            <div class="col-md-2"></div>
        <div class="col-md-8">

        @if ((Session::has('success-message')))
            <div class="alert alert-success col-md-12">{{
					Session::get('success-message') }}</div>
        @endif @if ((Session::has('fail-message')))
            <div class="alert alert-danger col-md-12">{{
					Session::get('fail-message') }}</div>
        @endif
                    {!! Form::model($hotel,['route' => ['hotel.update',$hotel->id], 'method' => 'PUT', 'files' => true]) !!}
                    <div class="form-group">
                        {{ Form::label('code', 'CODE') }}
                        {{ Form::text('code', null, array('class' => 'form-control','required'=>'')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('name', 'NAME') }}
                        {{ Form::text('name', null, array('class' => 'form-control','required'=>'')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('email', 'EMAIL') }}
                        {{ Form::email('email', null, array('class' => 'form-control', 'required'=>'')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('guide_id', 'GUIDE') }}
                        {{ Form::select('guide_id', $guide, null, ['class' => 'form-control','placeholder'=>'Select Guide', 'required'=>'']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('location_id', 'LOCATION') }}
                        {{ Form::select('location_id', $location, null, ['class' => 'form-control','placeholder'=>'Select Location', 'required'=>'']) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('transfer_price_mod', 'TRANSFER PRICE') }}
                        {{ Form::text('transfer_price_mod', null, array('class' => 'form-control','required'=>'')) }}
                    </div>
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="active" value="1" required> ACTIVE</label>
                        <label class="radio-inline"><input type="radio" name="active" value="0"> INACTIVE</label>
                    </div>
                    {{ Form::submit('Update', array('class' => 'btn btn-success')) }}
            <a href="{{route('hotel.index')}}" id="cancel" name="cancel" class="btn btn-danger">Cancel</a>

            {!! Form::close() !!}



    </div>
        </div>
    </div>
    <br>

@endsection