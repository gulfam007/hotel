@extends('layouts.app')

@section('content')
<title>Hotel | Create</title>
    <div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">

        @if ((Session::has('success-message')))
            <div class="alert alert-success col-md-12">{{
					Session::get('success-message') }}</div>
        @endif @if ((Session::has('fail-message')))
            <div class="alert alert-danger col-md-12">{{
					Session::get('fail-message') }}</div>
        @endif
            <form action="{{route('hotel.store')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label class="control-label">CODE</label>
                    <input type="text" class="form-control" id="code" name="code" placeholder="ENTER CODIGO HERE.." required>
                </div>
                <div class="form-group">
                    <label class="control-label">NAME</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="ENTER NAME HERE.." required>
                </div>
                <div class="form-group">
                    <label class="control-label">EMAIL</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required>
                </div>

                <div class="form-group">
                    {{ Form::label('guide_id', 'GUIDE') }}
                    {{ Form::select('guide_id', $guide, null, ['class' => 'form-control','placeholder'=>'Select Guide', 'required'=>'']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('location_id', 'LOCATION') }}
                    {{ Form::select('location_id', $location, null, ['class' => 'form-control','placeholder'=>'Select Location', 'required'=>'']) }}
                </div>
                <div class="form-group">
                    <label class="control-label">TRANSFER PRICE</label>
                    <input type="number" class="form-control" id="transfer_price_mod" name="transfer_price_mod" placeholder="ENTER PRICE HERE.." required>
                </div>
                <div class="form-group">
                    <label class="radio-inline"><input type="radio" name="active" value="1" required> ACTIVE</label>
                    <label class="radio-inline"><input type="radio" name="active" value="0"> INACTIVE</label>
                </div>
                <button type="submit" class="btn btn-success">Submit</button>
                <a href="{{route('hotel.index')}}" id="cancel" name="cancel" class="btn btn-danger">Cancel</a>

            </form>
        </div>
    </div>
    </div>
    <br>
    @endsection