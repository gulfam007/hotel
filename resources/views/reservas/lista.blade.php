@extends('layouts.app')

<link href="{{ asset ('plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet"
      type="text/css"/>
<link href="{{ asset ('plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}"
      rel="stylesheet" type="text/css"/>
<link href="{{ asset ('plugins/datatables-buttons/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset ('plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet"
      type="text/css" media="screen"/>
<link href="{{ asset ('plugins/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet') }}" type="text/css"
      media="screen">


@section('content')

    <style>
        .table tbody tr.highlight td {
            background: #FEEFB3;
        }

        .table tbody tr.highlight2 td {
            background: #FFD2D2;
        }

        .my-notify-info, .my-notify-success, .my-notify-warning, .my-notify-error {
            padding: 10px;
            margin: 10px 0;

        }

        .my-notify-info:before, .my-notify-success:before, .my-notify-warning:before, .my-notify-error:before {
            font-family: FontAwesome;
            font-style: normal;
            font-weight: 400;
            speak: none;
            display: inline-block;
            text-decoration: inherit;
            width: 1em;
            margin-right: .2em;
            text-align: center;
            font-variant: normal;
            text-transform: none;
            line-height: 1em;
            margin-left: .2em;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale
        }

        .my-notify-info:before {
            content: "\f05a";
        }

        .my-notify-success:before {
            content: '\f00c';
        }

        .my-notify-warning:before {
            content: '\f071';
        }

        .my-notify-error:before {
            content: '\f057';
        }

        .my-notify-info {
            color: #B45F04;
            background-color: #F5D0A9;
        }

        .my-notify-success {
            color: #4F8A10;
            background-color: #DFF2BF;
        }

        .my-notify-warning {
            color: #9F6000;
            background-color: #FEEFB3;
        }


        .my-notify-error {
            color: #D8000C;
            background-color: #FFD2D2;
        }


    </style>

    <!--
    ORIGINAL DIV CONTAINER
    <div class=" container    container-fixed-lg">




    </div>
    -->





    <div class="m-0 row card-block">


        <div class="col-lg-12 sm-no-padding">
            <div class="card card-transparent">
                <div class="card-block no-padding">
                    <div id="card-advance" class="card card-default">


                        <style>

                            tfoot {
                                display: table-header-group;
                            }

                            table {
                                /*table-layout: fixed;*/
                                width: 100% !important;
                            }

                            table td,
                            table th {
                                /*width: auto !important;*/
                                white-space: normal;
                                text-overflow: ellipsis;
                                overflow: hidden;
                            }

                            td{width:1px;white-space:nowrap;}

                            /*
@author: http://coderexample.com
@reference : http://codepen.io/Akiletour/pen/emGOgd
*/
                            .dataTables_wrapper .dataTables_processing {
                                position: absolute !important;
                                top: 50% !important;
                                right: 0 !important;
                                bottom: 0 !important;
                                margin-right: 50% !important;
                                margin-top: 15px !important;
                                background: none;
                                -webkit-transform: translate(-50%, -50%);
                                -ms-transform: translate(-50%, -50%);
                                transform: translate(-50%, -50%);
                            }

                            .dataTables_processing > div {
                                content: "";
                                width: 10px;
                                height: 10px;
                                background: red;
                                border-radius: 100%;
                                position: absolute;
                                -webkit-animation: animate 2s linear infinite;
                                animation: animate 2s linear infinite;
                            }

                            .dataTables_processing > div:nth-of-type(1) {
                                -webkit-animation-delay: -.4s;
                                animation-delay: -.4s;
                            }

                            .dataTables_processing > div:nth-of-type(2) {
                                -webkit-animation-delay: -.8s;
                                animation-delay: -.8s;
                            }

                            .dataTables_processing > div:nth-of-type(3) {
                                -webkit-animation-delay: -1.2s;
                                animation-delay: -1.2s;
                            }

                            .dataTables_processing > div:nth-of-type(4) {
                                -webkit-animation-delay: -1.6s;
                                animation-delay: -1.6s;
                            }

                            @-webkit-keyframes animate {
                                0% {
                                    left: 100px;
                                    top: 0;
                                }
                                80% {
                                    left: 0;
                                    top: 0;
                                }
                                85% {
                                    left: 0;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                90% {
                                    width: 40px;
                                    height: 15px;
                                }
                                95% {
                                    left: 100px;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                100% {
                                    left: 100px;
                                    top: 0;
                                }
                            }

                            @keyframes animate {
                                0% {
                                    left: 100px;
                                    top: 0;
                                }
                                80% {
                                    left: 0;
                                    top: 0;
                                }
                                85% {
                                    left: 0;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                90% {
                                    width: 20px;
                                    height: 10px;
                                }
                                95% {
                                    left: 100px;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                100% {
                                    left: 100px;
                                    top: 0;
                                }
                            }

                        </style>
                        <div class="card-block">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                                <li class="nav-item">
                                    <a href="#" class="active" data-toggle="tab"
                                       data-target="#fade1"><span>Novas</span> {{$new}}  </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" data-toggle="tab"
                                       data-target="#fade2"><span>Alterações</span> {{$changed}}</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" data-toggle="tab"
                                       data-target="#fade3"><span>Cancelamentos</span> {{$canceled}}</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="fade1">

                                    <table class="table table-hover demo-table-search table-responsive-block"
                                           id="minhaImportacaoTable">
                                        <thead>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Checkout</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Nr. Quartos</th>
                                            <th>Nr. Pessoas</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>


                                        </tr>


                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Checkout</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Nr. Quartos</th>
                                            <th>Nr. Pessoas</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>


                                        </tr>
                                        </tfoot>

                                    </table>

                                </div>
                                <div class="tab-pane fade" id="fade2">
                                    <table class="table table-hover demo-table-search table-responsive-block"
                                           id="minhaImportacaoTable2">
                                        <thead>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Checkout</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Nr. Quartos</th>
                                            <th>Nr. Pessoas</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>


                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Checkout</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Nr. Quartos</th>
                                            <th>Nr. Pessoas</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>


                                        </tr>
                                        </tfoot>


                                    </table>
                                </div>
                                <div class="tab-pane fade" id="fade3">
                                    <table class="table table-hover demo-table-search table-responsive-block"
                                           id="minhaImportacaoTable3">
                                        <thead>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Checkout</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Nr. Quartos</th>
                                            <th>Nr. Pessoas</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>


                                        </tr>


                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Checkout</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Nr. Quartos</th>
                                            <th>Nr. Pessoas</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>


                                        </tr>
                                        </tfoot>


                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>




    <div class="modal fade slide-up disable-scroll" id="destroyBookingModal" tabindex="-1" role="dialog"
         style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="pg-close fs-14"></i>
                        </button>
                        <h5>Excluir Reserva</h5>
                    </div>
                    <div class="modal-body">
                        <p class="no-margin">Excluir Reserva <span id="modalBooking"></span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="destroyBookingButton"
                                class="btn btn-danger btn-cons  pull-left inline" data-dismiss="modal">Excluir
                        </button>
                        <button type="button" class="btn btn-default btn-cons no-margin pull-left inline"
                                data-dismiss="modal">Fechar
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade slide-up disable-scroll" id="cancelImportModal" tabindex="-1" role="dialog"
         style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="pg-close fs-14"></i>
                        </button>
                        <h5>Cancelar Importação</h5>
                    </div>
                    <div class="modal-body">
                        <p class="no-margin">Deseja cancelar a importação?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="cancelImportButton" class="btn btn-danger btn-cons  pull-left inline"
                                data-dismiss="modal">Cancelar
                        </button>
                        <button type="button" class="btn btn-default btn-cons no-margin pull-left inline"
                                data-dismiss="modal">Fechar
                        </button>
                    </div>
                </div>
            </div>

        </div>
    </div>





    <div class="modal fade slide-up disable-scroll " id="FilterBookingModal" tabindex="-1" role="dialog" style="">
        <div class="modal-dialog">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                                    class="pg-close fs-14"></i>
                        </button>
                        <h5><span class="semi-bold">Filtro</span></h5>

                    </div>
                    <div class="modal-body">
                        <form role="form">
                            <div class="form-group-attached">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group-default form-group-default-select2 ">
                                            <label class="">Coluna</label>
                                            <select id="column" name="column" class="full-width initSelect2">


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group-default">
                                            <label>Texto</label>
                                            <input id="text" name="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row">

                            <div class="col-md-4 m-t-10 sm-m-t-10 pull-right">
                                <button type="button" id="filterButton" class="btn btn-danger btn-block m-t-5">Filtrar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



    </div>



    @include('partial.editModal')

    @include('partial.scripts')


    <script src="{{ asset ('plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset ('plugins/datatables-buttons/dataTables.buttons.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/datatables-buttons/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/buttons.print.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/jquery-datatable/media/js/dataTables.bootstrap.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset ('plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-responsive/js/datatables.responsive.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/bootstrap-collapse/bootstrap-tabcollapse.js') }}" type="text/javascript"></script>









    <script>
        //text area


        (function() {
            var days = ["D", "S", "T", "QT", "QN", "SX", "SB"];


            Date.prototype.getDayName = function() {
                return days[ this.getDay() ];
            };

        })();



        var _format = function (d) {
            // `d` is the original data object for the row
            return '<td colspan="4"><p>' +
                ' <b>Nome:</b> ' + d.name + ' <br> ' +


                ' <b>Saudação:</b> ' + d.salutati + ' ' +
                ' <b>Idade: </b>' + d.age + ' ' +


                ' <b>Check In:</b> ' + d.check_in + ' ' +
                ' <b>Check Out:</b> ' + d.check_out + '<br>  ' +


                ' <b>Código Hotel:</b> ' + d.hotel + ' ' +
                ' <b>Hotel:</b> ' + d.hotel_name + ' <br> ' +


                ' <b>Cate:</b> ' + d.cate + ' ' +
                ' <b>Changed:</b> ' + d.changed + ' <br>  ' +


                ' <b>Voo:</b> ' + d.flight_number + ' ' +
                ' <b>Connected load:</b> ' + d.connected_load + ' <br>  ' +


                ' <b>Membros:</b> ' + d.members + ' ' +
                ' <b>Dias:</b> ' + d.days + '<br>  ' +

                ' <b>Tipo Quarto:</b> ' + d.room_name +
                ' <b>Código Quarto:</b> ' + d.room + ' ' +
                ' <b>Valor:</b> ' + d.price + '<br>  ' +


                ' <b>Temporada:</b> ' + d.season + ' ' +
                ' <b>Estado:</b> ' + d.state + ' ' +
                '</p></td>';
        }


        function tableSettings(state) {

            var settings = {

                dom: "< <'col-md-4 pull-left' l> <'col-md-4 pull-left' B> <'col-md-4 pull-right'f> >" +
                "tr" +
                "< <'col-md-6 pull-left'i> <'col-md-6 pull-right'p> >",


                language:
                    {
                        "sProcessing": "<div></div><div></div><div></div><div></div><div></div>",
                        "sLengthMenu": "Mostrar _MENU_ registos",
                        "sZeroRecords": "Não foram encontrados resultados",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                        "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                        "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                        "sInfoPostFix": "",
                        "sSearch": "Procurar:",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "Primeiro",
                            "sPrevious": "Anterior",
                            "sNext": "Seguinte",
                            "sLast": "Último"
                        }


                    },

                buttons: [
                    {
                        text: 'Exportar Reservas Excel',
                        action: function (e, dt, node, config) {
                            var state = $("ul.nav-tabs li a.active span").text();
                            window.open("{{ route('exportImport') }}?locked=0&view=import&state=" + state);
                        }
                    },
                    {
                        text: 'Exportar Reservas XML',
                        action: function (e, dt, node, config) {
                            var state = $("ul.nav-tabs li a.active span").text();
                            window.open("{{ route('exportImportXML') }}?locked=0&view=import&state=" + state);
                        }
                    }
                ],
                deferRender: true,
                processing: true,
                serverSide: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                iDisplayLength: -1,
                ajax: {
                    "url": "{{ url('listData') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                        state: state,
                        view: 'import'
                    }
                },




                columns: [
                    {
                        "data": "Tom",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {"data": "Booking"},
                    {"data": "State"},
                    {"data": "checkIn"},
                    {
                        "data": "Day",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {"data": "ArrivalDate"},
                    {"data": "ArrivalFligh"},
                    {"data": "ArrivalTime"},
                    {"data": "DepartureLocation"},
                    {"data": "DepartureFlight"},
                    {"data": "DepartureDate"},
                    {"data": "checkOut"},
                    {"data": "DepartureTime"},
                    {
                        "data": "ArrivalLocation",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {"data": "Names"},
                    {
                        "data": "Essen",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {"data": "Hotel"},
                    {
                        "data": "Guide",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {
                        "data": "ROOMS",
                        "searchable": false
                    },
                    {
                        "data": "MEMBERS",
                        "searchable": false
                    },
                    {
                        "data": "RoomType",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {
                        "data": "RoomCode",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {
                        "data": "Nights",
                        "visible": false,
                        "searchable": false,
                        "orderable": false
                    },
                    {
                        mRender: function (data, type, row) {

                            // console.log(row)
                            var rdata = row;

                            return '<button class="btn btn-complete btn-xs m-t-10 optns" rentacarexists="' + row.RENTACAREXISTS + '" exists="' + row.EXISTS + '" onclick="editBooking(' + "'" + rdata.ID + "'" + ')">Editar</button> <button  onclick="" class="btn  btn-success btn-xs m-t-10">Reenviar</button> <button token=""  onclick="destroyBooking(' + "'" + row.ID + "'" + ',' + "'" + row.Booking + "'" + ',' + "'" + row.State + "'" + ')" class="btn btn-danger btn-xs m-t-10">Excluir</button>'
                        },
                        orderable: false
                    }


                    //{ "data": "options" }
                ],

            };

            return settings;
        }
        function serializeJSON(serialized){

            var result = { };
            $.each(serialized, function() {
                result[this.name] = this.value;
            });
            return result

        }

        function saveEditModal() {
            var fid = $('#flights').attr('rid')

            var formData = {
                booking: [],
                clients: [],
                flights: [],
                rooms: [],
                cars: []
            };

            formData.booking.push( serializeJSON( $("[container='bookings'] .form-control").serializeArray() ) );
            formData.clients.push( serializeJSON($("[container='clients'] .form-control").serializeArray() ) );
            formData.flights.push( serializeJSON( $("[container='flights'] .form-control").serializeArray() ) );
            //formData.rooms.push( serializeJSON( $("[container='rooms'] .form-control").serializeArray() ) );


            $.each($('[container="rooms"]'), function( index, value ) {
                formData.rooms.push(serializeJSON($(this).find('.form-control').serializeArray()));

            });


            formData.cars.push( serializeJSON( $("[container='car'] .form-control").serializeArray() ) );




            var i = 0;

            $.each($('[container="rooms"]').find('select'), function( index, value ) {

                formData.rooms[i].hotel_id = $(this).val();
                formData.rooms[i].aroom_id = $(this).attr('rid');
                i = i + 1

            });

            //

            formData.booking[0].state = $("#state").select2('val');
            formData.booking[0].transfer = $("#transfer").select2('val');

            formData.cars[0].rentacar_id = $("#rentacar_id").select2('val');
            formData.cars[0].type = $("#type").select2('val');

            console.log(formData.cars[0])




            var bookId = $('#bookingNumber').attr('bookid');
            var book = $('#bookingNumber').text();



            var url = "{{ route('bookings.update', ['id' => ':id']) }}";
            url = url.replace(':id', book);

            var form = $('#editBooking');

            console.log(formData);
            if (form.valid()) {

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'PUT',
                    contentType: 'application/json',
                    data:
                        JSON.stringify({
                            "_token": "{{csrf_token()}}",
                            "form": formData,
                            "bookNumber": book,
                            "bookId": bookId,
                            "flightId": fid


                        }),
                    complete: function (xhr, textStatus) {

                        if (xhr.status == '200' || xhr.status == 200) {

                            $('body').pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                        } else {

                            $('body').pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });

            }



            $('#bookingEditModal').modal('hide');
            table1.ajax.reload();
            table2.ajax.reload();
            table3.ajax.reload();
            //console.log(formData)

        }

        function inputHtml(placeHolder, type) {


            return '<div class="col-md-5">\n' +
                '<div class="form-group form-group-default ">\n' +
                '\n' +
                '<input type="text" dType="' + type + '" class="form-control ' + type + '" name="" placeholder="' + placeHolder + '">\n' +
                '</div>\n' +
                '</div>'
        }

        function formatDate(d) {

            if (d !== '' || d !== null) {
                let parts = d.split('/');
                return parts[2] + '-' + parts[1] + '-' + parts[0]
            } else {
                return ''
            }
        }

        function isDate(d) {

            let regex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;


            return regex.test(d);

        }

        function renderTable(tableName, state) {


            $('#' + tableName + ' tfoot th').each(function () {
                let classType;
                let title = $(this).text();


                if (title !== '') {

                    if (title === 'Data' | title === 'Chegada' | title === 'Saída') {
                        classType = 'datePickers';
                    } else if (title === 'Hora C' | title === 'Hora S') {
                        classType = 'timeMask';
                    } else {
                        classType = '';
                    }


                    $(this).html(inputHtml('Buscar', classType));
                }

            });


            var table = $('#' + tableName).DataTable(tableSettings(state));


            $('.datePickers').datepicker({
                format: 'dd/mm/yyyy'
            })

            // search box for table
            $('#search-table').keyup(function () {
                table.fnFilter($(this).val());
            });


            //filter

            table.columns().every(function () {
                var that = this;


                //$('#test').datepicker('getDate').toLocaleDateString()

                $('input', this.footer()).on('change keyup', function () {

                    var inputType = $(this)["0"].attributes[1].value;

                    var value = inputType === 'datePickers' ? this.value : this.value;

                    if (that.search() !== value) {
                        that
                            .search(value)
                            .draw();
                    }
                });


            });


            return table;

        }

        var table1 = renderTable('minhaImportacaoTable', 'BU');
        var table2 = renderTable('minhaImportacaoTable2', 'UN');
        var table3 = renderTable('minhaImportacaoTable3', 'ST');


        //modal

        var tom = $('#tms_id');
        var booking = $('#booking');
        var bookingNumber = $('#bookingNumber');
        var bookingdate = $('#bookingdate');
        var state = $('#state');
        var price = $('#price');

        //  [array]
        var name = $('#name');
        var age = $('#age');
        var gender = $('#gender');
        var room = $('#room');

        var flight = $('#flights');
        var arrivalDate = $('#arrivalDate');
        var arrivalFlight = $('#arrivalFlight');
        var time = $('#time');
        var day = $('#day');
        var departureDate = $('#departureDate');
        var departureFlight = $('#departureFlight');
        var departureTime = $('#departureTime');
        var destiny = $('#destiny');



        var hotel = $('#hotel');
        var essen = $('#essen');
        var transfer = $('#transfer');

        // [array]
        var roomType = $('#roomType');
        var nights = $('#nights');
        var room_code = $('#room_code'); //(roomcode)
        var members = $('#members');
        var roomNotes = $('#roomNotes');

        var typeAuto = $('#type');
        var rentacarOption = $('#rentacarOption');
        var carNotes = $('#rentACarNotes');
        var carPrice = $('#price');


        function drawClientCard(client, mapBookRooms) {

            var clientCard = '';

            clientCard += '<div class="row clearfix">';

            clientCard += '<div class="col-md-3">';
            clientCard += '<div class="form-group form-group-default " >';
            clientCard += '<label>Nome</label>';
            clientCard += '<input type="text" rid="' + client.id + '" class="form-control" value="' + client.name + '" id="name_' + client.id + '" name="name_' + client.id + '" >';
            clientCard += '</div>';
            clientCard += '</div>';

            clientCard += '<div class="col-md-2">';
            clientCard += '<div class="form-group form-group-default " >';
            clientCard += '<label>Idade</label>';
            clientCard += '<input type="text" rid="' + client.id + '" class="form-control" value="' + client.age + '" id="age' + client.id + '" name="age_' + client.id + '" >';
            clientCard += '</div>';
            clientCard += '</div>';

            clientCard += '<div class="col-md-2">';
            clientCard += '<div class="form-group form-group-default form-group-default-select2 ">';
            clientCard += '<label class="">Gênero</label>';
            clientCard += '<select  rid="' + client.id + '" class="form-control" value="' + client.gender + '" id="gender' + client.id + '" name="gender_' + client.id + '" class="full-width initSelect2" >';
            clientCard += '<option value=""></option>';
            clientCard += '<option value="H">H</option>';
            clientCard += '<option value="F">F</option>';
            clientCard += '<option value="K">K</option>';
            clientCard += '<option value="I">I</option>';
            clientCard += '</select>';
            clientCard += '</div>';
            clientCard += '</div>';

            clientCard += '<div class="col-md-4">';
            clientCard += '<div class="form-group form-group-default form-group-default-select2 ">';
            clientCard += '<label class="">Quarto</label>';
            clientCard += '<select  rid="' + client.id + '" class="form-control full-width initSelect2" id="room' + client.id + '" name="room_' + client.id + '_'+ client.room +'"  >';
            clientCard += '<option value=""></option>';
            clientCard += '</select>';
            clientCard += '</div>';
            clientCard += '</div>';

//            clientCard += '<div class="col-md-5">';
//            clientCard += '<div class="form-group form-group-default form-group-default-select2 clientRoom">';
//            clientCard += '<label class="">Quarto</label>';
//            clientCard += '<select  rid="' + client.id + '"  id="room' + client.id + '" name="room_id" class="full-width initSelect2 required" required>';
//            clientCard += '<option value=""></option>';
//            clientCard += '</select>';
//            clientCard += ' </div>';
//            clientCard += '</div>';

            clientCard += '<a id="removeClient" onclick="removeClient(' + client.id + ',$(this))" class="fa fs-16 fa-minus-circle text-danger m-r-10" style="';
            clientCard += 'border-left-color: rgba(0, 0, 0, 0.07);';
            clientCard += 'border-left-style: solid;';
            clientCard += 'border-left-width: 1px;';
            clientCard += 'padding-left: 5px;';
            clientCard += '"></a>';

            clientCard += '</div>';


            return clientCard;

        }

        function removeClient(clientId,element) {
            // removeClientModal
            // removeClientButton
            // removeRoomModal
            // removeRoomButton

//console.log(element.parent());

            // console.log($('#clientContainer a').css('border-left-width: 1px'));

            $('#removeClientModal').modal().one('click', '#removeClientButton', function (e) {

                $.ajax({
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: clientId
                    },
                    url: "{!! route('removeClient' ) !!}",
                    complete: function (xhr, textStatus) {

                        if (xhr.status == '200' || xhr.status == 200) {

                            $('.page-container').pgNotification({
                                style: 'flip',
                                message: 'Cliente removido com sucesso.',
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();
                            $('#removeClientModal').modal('toggle');
                            element.parent().remove();




                        }

                    }
                });


            })



        }

        function removeRoom(roomId,element) {
            // removeClientModal
            // removeClientButton
            // removeRoomModal
            // removeRoomButton



            $('#removeRoomModal').modal().one('click', '#removeRoomButton', function (e) {

                $.ajax({
                    type: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        id: roomId
                    },
                    url: "{!! route('removeRoom' ) !!}",
                    complete: function (xhr, textStatus) {

                        $('.page-container').pgNotification({
                            style: 'flip',
                            message: 'Quarto removido com sucesso.',
                            position: 'top-right',
                            timeout: 4500,
                            type: 'Success'
                        }).show();
                        $('#removeRoomModal').modal('toggle');

                        $("#roomContainer_"+roomId).remove()

                    }
                });


            })



        }

        function hotelOptions(hotel) {

            var sel = '';




            var opt = '';
            var ax = true
            @foreach($hotelOptions as $hotelOption)
                opt += '<option ' + sel  + ' value="{{ trim($hotelOption->id) }}">';
            opt += '{{ trim($hotelOption->text) }}';
            opt += '</option>';
            @endforeach

                return opt
        }

        function drawRoomsCards(room) {

            var members = '';



            if (room.members || room.members !== null) {

                $.each(room.members.split("/"), function (index, member) {
                    members += '<span class="tag label label-danger">';
                    members += member;
                    members += '</span>';
                });
            } else {
                members += '<span class="tag label label-danger">';
                //members += member;
                members += '</span>';

            }


            var roomCard = '';



            roomCard += '<div id="roomContainer_'+room.id+'" container="rooms" class="form-group-attached">';

            roomCard += '<div class="row clearfix">';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default form-group-default-select2 ">';
            roomCard += '<label class="">Hotel</label>';
            roomCard += '<select rid="' + room.id + '"  id="hotel_' + room.id + '" name="hotel_' + room.id + '" class="full-width initSelect2" >';
            roomCard += '<option value="0"></option>';
            roomCard += hotelOptions(room.id);
            roomCard += '</select>';
            roomCard += '</div>';
            roomCard += ' </div>';

            roomCard += '<div class="col-md-2">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Catering</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" id="catering' + room.id + '" name="catering" value="' + room.catering + '">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<div class="col-md-1">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Essen</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" id="essen' + room.id + '" name="essen" value="' + room.essen + '">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Quarto + Regime</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" value="' + room.room_code + '" id="room_code' + room.id + '" name="room_code">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<a id="removeRoom" onclick="removeRoom(' + room.id + ',$(this))" class="fa fs-16 fa-minus-circle text-danger m-r-10" style="';
            roomCard += 'border-left-color: rgba(0, 0, 0, 0.07);';
            roomCard += 'border-left-style: solid;';
            roomCard += 'border-left-width: 1px;';
            roomCard += 'padding-left: 5px;';
            roomCard += '"></a>';

            roomCard += '</div>';


            roomCard += '<div class="row clearfix">';

            roomCard += '<div class="col-md-8">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Tipo de Quarto</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" value="' + room.room_type + '" id="roomType' + room.id + '" name="room_type">';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '<div class="col-md-1">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Noites</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" value="' + room.nights + '" id="nights' + room.id + '" name="nights">';
            roomCard += '</div>';
            roomCard += '</div>';


            roomCard += '</div>';


            roomCard += '<div class="row">';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default">';
            roomCard += '<label>Checkin</label>';
            roomCard += '<input rid="' + room.id + '" id="checkIn' + room.id + '" value="' + room.check_in + '" name="check_in" class="form-control dateMask">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default">';
            roomCard += '<label>Checkout</label>';
            roomCard += '<input rid="' + room.id + '" id="checkOut' + room.id + '" value="' + room.check_out + '"  name="check_out"  class="form-control dateMask">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default">';
            roomCard += '<label>Valor</label>';
            roomCard += '<input rid="' + room.id + '" id="price' + room.id + '" value="' + room.price + '"  name="price" type="text" data-a-dec="," data-a-sep="." class="autonumeric form-control">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '</div>';

            roomCard += '<div class="row">';
            roomCard += '<div class="col-md-9">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Membros</label>';
            roomCard += '<input class="tagsinput custom-tag-input" type="text" value="hello World, quotes, inspiration" style="display: none;">';
            roomCard += '<div class="bootstrap-tagsinput" id="members' + room.id + '" name="members_' + room.id + '">';

            roomCard += members;

            roomCard += '<input size="1" type="text">';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '<div class="row">';
            roomCard += '<div class="col-md-9">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>OBS</label>';

            roomCard += '<textarea rid="' + room.id + '" class="form-control"   id="roomNotes' + room.id + '" name="notes" rows="5">';
            roomCard += room.room_notes;
            roomCard += '</textarea>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</br>';


            return roomCard;

        }

        function drawPackageCards(package) {


            var packagecard = '';
            var tauchen = '';


            if (package.tuachen_pax !== '') {

                tauchen += ' Pax: ' + package.pack_pax;
                tauchen += ' Qty: ' + package.pack_qty;

            }

            packagecard += '<span class="tag label label-danger">' + package.name + ' Est: ' + package.state + ' Cliente: ' + package.client + tauchen + ' Valor: ' + package.price + ' In: ' + package.checkIn + ' Out: ' + package.checkOut;
            packagecard += '<span data-role="remove" onclick="removePackage(' + package.id + ')" id="' + package.id + '" ></span>';
            packagecard += '</span>';

            return packagecard;


        }

        function convertDigitIn(d){

            if (d) {
                let parts = d.split('-');
                return parts[2] + '/' + parts[1] + '/' + parts[0]
            } else {
                return ''
            }
        }

        function editBooking(id) {




            var form = $('#editBooking');
            $(document).ready(function () {
                $('.initSelect2').select2();


            });

            $.get("bookings/" + id, function (data) {
                var result = (JSON.parse(data));
                // console.log(result);

                var changeForm = $("#changeBooking");

                // result.booking["0"].arrival_flight

                if (result.booking["0"]) {


                    $('#editBookingDiv').removeClass( "col-md-6" ).addClass( "col-md-12" );
                    $('#editBookingDiv').show();

                    $('#tms_id').val(result.booking["0"].tms_id).trigger('change');

                    booking.val(result.booking["0"].booking_number);
                    bookingNumber.attr('bookId', result.booking["0"].booking_id);
                    bookingNumber.text(result.booking["0"].booking_number);
                    bookingdate.val(result.booking["0"].booking_date);
                    // state.val( result.booking["0"].state ).trigger('change');
                    $('#state').val(result.booking["0"].state).trigger('change');
                    arrivalDate.val(result.booking["0"].arrival_date);
                    arrivalFlight.val(result.booking["0"].arrival_flight);
                    time.val(result.booking["0"].arrival_time);
                    console.log(result.booking["0"])
                    flight.attr('rid', result.booking["0"].flightid);
                    departureDate.val(result.booking["0"].departure_date);
                    //   console.log(result.booking["0"].departure_date)
                    if(result.booking["0"].departure_date !== null && result.booking["0"].arrival_date !== null ){
                        var now = new Date(formatDate(result.booking["0"].arrival_date));
                        console.log(now)
                        var dayVal = now.getDayName();
                        day.val(dayVal);

                    }

                    departureFlight.val(result.booking["0"].departure_flight);
                    departureTime.val(result.booking["0"].departure_time);
                    destiny.val(result.booking["0"].destiny);

                    hotel.val(result.booking["0"].hotel);
                    essen.val(result.booking["0"].essen);
                    var transferVal = result.booking["0"].transfer;

                    if (transferVal == '' || !transferVal || transferVal == null) {
                        transferVal = 'Verificar'
                    }

                    $('#transfer').val(transferVal).trigger('change');
                    //[array]
                    roomType.val();
                    // // nights.val();
                    //   room_code.val();   //(roomcode)
                    //  members.val();
                    //   roomNotes.val();

                    if (result.car["0"]) {

                        rentacarOption.val(result.car["0"].rentacar_id);
                        carNotes.val(result.car["0"].notes);
                        carPrice.val(result.car["0"].price);
                        $('#type').val(result.car["0"].type).trigger('change');
                        $('#rentacar_id').val(result.car["0"].rentacar_id).trigger('change');
                    }


                    var mapBookRooms = $.map(result.rooms, function (room) {
                        return {id: room.id, text: room.room_type};
                    });

                    console.log(result.clients)
                    console.log(result.rooms)
                    $.each(result.clients, function (index, client) {
                        $('#clientContainer').append(drawClientCard(client, mapBookRooms));
                        var gender = $('#gender' + client.id);
                        var room = $('#room' + client.id);
                        gender.select2();
                        room.select2({
                            data: mapBookRooms
                        });
                        room.val(client.room).trigger('change');
                        gender.val(client.gender).trigger('change');

                    });

                    $.each(result.rooms, function (index, room) {
                        $('#roomContainer').append(drawRoomsCards(room));

                        var hotelSelector = $('#hotel_'+room.id);
                        hotelSelector.select2();
                        hotelSelector.val(room.hotel_id).change();
                        // $('#hotel_' + room.id + ' option').filter(function() {
                        //     return $(this).text() == room.hotel;
                        // }).prop('selected', true);

                    });


                    $.each(result.packages, function (index, package) {
                        if (package.name !== null) {
                            $('#packagesContainer').append(drawPackageCards(package));
                        }
                    });

                    state.val(result.booking["0"].state).trigger('change');


                    //  $('#bookingEditModal').modal();


                    $("#c_room").select2({
                        data: mapBookRooms
                    });

                    var mapBookClients = $.map(result.clients, function (client) {
                        return {id: client.id, text: client.name};
                    });


                    $("#p_clients").select2({
                        data: mapBookClients
                    });



                }

                if (result.cbooking["0"]) {

                    $('#changeBookingDiv').show();
                    if(result.booking["0"]){
                        $('#bookingEditModal .modal-content').css('width', '1800px');
                        $('#editBookingDiv').show();
                        $('#changeBookingDiv').removeClass( "col-md-12" ).addClass( "col-md-6" );
                        $('#editBookingDiv').removeClass( "col-md-12" ).addClass( "col-md-6" );

                    }else{
                        $('#editBookingDiv').hide();
                        $('#changeBookingDiv').removeClass( "col-md-6" ).addClass( "col-md-12" );
                        $('#editBookingDiv').removeClass( "col-md-12" ).addClass( "col-md-6" );
                    }


                    bookingNumber.text(result.cbooking["0"].invoice);
                    $('#historytom').val(result.cbooking["0"].tom);
                    $('#historynumber').val(result.cbooking["0"].invoice);
                    $('#historydate').val(result.cbooking["0"].booked);
                    $('#historystate').val(result.cbooking["0"].state).trigger('change');
                    $('#historyClient').val(result.cbooking["0"].name);
                    $('#historyAge').val(result.cbooking["0"].age);

                    $('#historyarrivalDate').val(result.cbooking["0"].season);
                    $('#historyarrivalFlight').val(result.cbooking["0"].flight_number);
                    $('#historytime').val(result.cbooking["0"].arrival_time);
                    $('#historyarrivalLocation').val(result.cbooking["0"].arrival_location);

                    $('#historydepartureDate').val(result.cbooking["0"].departure_date);
                    $('#historydepartureFlight').val(result.cbooking["0"].connected_load);
                    $('#historydepartureTime').val(result.cbooking["0"].departure_time);
                    $('#historyDestiny').val(result.cbooking["0"].departure_location);

                    $('#historyHotel').val(result.cbooking["0"].hotel_name);
                    $('#historyHotelCode').val(result.cbooking["0"].hotel);
                    $('#historyRoom').val(result.cbooking["0"].room_name);
                    $('#historyRoomCode').val(result.cbooking["0"].room);
                    $('#historyValue').val(result.cbooking["0"].price);
                    $('#historyCheckIn').val(result.cbooking["0"].check_in);
                    $('#historyCheckOut').val(result.cbooking["0"].check_out);
                    $('#historyNights').val(result.cbooking["0"].days);
                    $('#historyDay').val(result.cbooking["0"].day);
                    $('#historyRoomsQty').val(result.cbooking["0"].number_of_rooms);
                    $('#historyNotes').val(result.cbooking["0"].notes);

                    var historytransferVal = result.cbooking["0"].transfer;

                    if (historytransferVal == '' || !historytransferVal || historytransferVal == null) {
                        historytransferVal = 'Verificar'
                    }

                    $('#historyTransfer').val(historytransferVal).trigger('change');

                }



                $('#bookingEditModal').modal();

                $( "#bookingEditModal" ).on('shown.bs.modal', function(){
                    $("textarea").each(function(textarea) {
                        $(this).height( $(this)[0].scrollHeight );
                    });
                });


                $('#bookingEditModal').on("hidden.bs.modal", function () {


                    $('#clientContainer').empty();
                    $('#roomContainer').empty();
                    $('#packagesContainer').empty();

                    $('#changeBookingDiv').hide();
                    $('#editBookingDiv').hide();
                    $('#bookingEditModal .modal-content').css('width', 'auto');
                    $('#changeBooking').trigger('reset');
                    $('#editBooking').trigger('reset');



                })
            });



        }

        function destroyBooking(id, bookingNumber, state) {

            $('#modalBooking').text(bookingNumber);

            $('#destroyBookingModal').modal().one('click', '#destroyBookingButton', function (e) {


                var state = $("ul.nav-tabs li a.active span").text();

                $.ajax({
                    type: 'DELETE',
                    data: {
                        _token: '{{ csrf_token() }}',
                        state: 'state'
                    },
                    url: "{!! url('bookings' ) !!}" + "/" + id,
                    success: function (data) {


                        $('.page-container').pgNotification({
                            style: 'flip',
                            message: 'Reserva ' + bookingNumber + ' excluída com sucesso.',
                            position: 'top-right',
                            timeout: 4500,
                            type: 'Success'
                        }).show();

                        switch (state) {
                            case 'Novas':
                                table1.ajax.reload();
                                break;
                            case 'Alterações':
                                table2.ajax.reload();
                                break;
                            case 'Cancelamentos':
                                table3.ajax.reload();
                                break;
                        }


                    }
                });

            });

        }



        function updateBooking() {

            var changedFields = $('.changed');

            $.each(changedFields, function (index, field) {
                console.log($('bookingNumber').attr('bookid'));
                console.log($(this).val());
                console.log($(this).attr('name'));
            });

        }

        function createClient() {

            var form = $('#clientModalForm');
            var modalForm = $('#bookingEditModal');

            if (form.valid()) {

                $.ajax({
                    url: '{{ route('bookings.store') }}',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data:
                        JSON.stringify({
                            "_token": "{{csrf_token()}}",
                            "bookId": $('#bookingNumber').attr('bookid'),
                            "roomId": $('#c_room').val(),
                            "gender": $('#c_gender').val(),
                            "age": $('#c_age').val(),
                            "name": $('#c_name').val(),
                            "container": "Cliente"
                        }),
                    complete: function (xhr, textStatus) {

                        if (xhr.status == '200' || xhr.status == 200) {

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                            $('#addClientModal').modal('toggle')

                        } else {

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });

                modalForm.modal('hide');
            }


        }

        function createRoom() {

            var form = $('#roomModalForm');
            var modalForm = $('#bookingEditModal');

            if (form.valid()) {

                $.ajax({
                    url: '{{ route('bookings.store') }}',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data:
                        JSON.stringify({
                            "_token": "{{csrf_token()}}",
                            "bookId": $('#bookingNumber').attr('bookid'),
                            "roomType": $('#r_room').val(),
                            "roomCode": $('#r_code').val(),
                            "nights": $('#r_nights').val(),
                            "checkOut": $('#r_checkout').val(),
                            "checkIn": $('#r_checkin').val(),
                            "hotelId": $('#r_hotel').val(),
                            "essen": $('#r_essen').val(),
                            "price": $('#r_price').val(),
                            "notes": $('#r_notes').val(),
                            "container": "Quarto"


                        }),
                    complete: function (xhr, textStatus) {

                        if (xhr.status == '200' || xhr.status == 200) {

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                            $('#addRoomModal').modal('toggle')


                        } else {

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });

                modalForm.modal('hide');

            }

        }

        function createPackage() {

            var form = $('#packageModalForm');
            var modalForm = $('#bookingEditModal');

            if (form.valid()) {

                $.ajax({
                    url: '{{ route('bookings.store') }}',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data:
                        JSON.stringify({
                            "_token": "{{csrf_token()}}",
                            "bookId": $('#bookingNumber').attr('bookid'),
                            "package": $('#p_package').val(),
                            "state": $('#p_state').val(),
                            "days": $('#p_days').val(),
                            "hotelId": $('#p_hotel').val(),
                            "price": $('#p_price').val(),
                            "tpax": $('#p_pax').val(),
                            "tqty": $('#p_tqty').val(),
                            "clientId": $('#p_clients').val(),
                            "notes": $('#p_notes').val(),
                            "checkin": $('#p_checkIn').val(),
                            "checkout": $('#p_checkOut').val(),
                            "container": "Pacote"


                        }),
                    complete: function (xhr, textStatus) {

                        if (xhr.status == '200' || xhr.status == 200) {

                            $('body').pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                            $('#addPackageModal').modal('toggle');
                            modalForm.modal('hide');

                        } else {

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });


            }

        }

        function removePackage(id){


            var book = $('#bookingNumber').text();



            var url = "{{ route('bookings.update', ['id' => ':id']) }}";
            url = url.replace(':id', book);

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'PUT',
                contentType: 'application/json',
                data:
                    JSON.stringify({
                        "_token": "{{csrf_token()}}",
                        "package_id": id,
                        "bookNumber": book,
                        "package": true


                    }),
                complete: function (xhr, textStatus) {

                    if (xhr.status == '200' || xhr.status == 200) {

                        $('body').pgNotification({
                            style: 'flip',
                            message: xhr.responseText,
                            position: 'top-right',
                            timeout: 4500,
                            type: 'Success'
                        }).show();

                    } else {

                        $('body').pgNotification({
                            style: 'flip',
                            message: xhr.responseText,
                            position: 'top-right',
                            timeout: 3500,
                            type: 'Danger'
                        }).show();

                    }
                }
            });

            $('#bookingEditModal').modal('hide');
            table1.ajax.reload();
            table2.ajax.reload();
            table3.ajax.reload();

        }




    </script>

@endsection