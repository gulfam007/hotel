@extends('layouts.app')

<link href="{{ asset ('plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset ('plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset ('plugins/datatables-buttons/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset ('plugins/datatables-responsive/css/datatables.responsive.css') }}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{ asset ('plugins/jquery-dynatree/skin/ui.dynatree.css" rel="stylesheet') }}" type="text/css" media="screen">


@section('content')

    <style>
        .table tbody tr.highlight td   {
            background: #FEEFB3;
        }

        .table tbody tr.highlight2 td   {
            background: #FFD2D2;
        }

        .my-notify-info, .my-notify-success, .my-notify-warning, .my-notify-error {
            padding:10px;
            margin:10px 0;

        }
        .my-notify-info:before, .my-notify-success:before, .my-notify-warning:before, .my-notify-error:before {
            font-family:FontAwesome;
            font-style:normal;
            font-weight:400;
            speak:none;
            display:inline-block;
            text-decoration:inherit;
            width:1em;
            margin-right:.2em;
            text-align:center;
            font-variant:normal;
            text-transform:none;
            line-height:1em;
            margin-left:.2em;
            -webkit-font-smoothing:antialiased;
            -moz-osx-font-smoothing:grayscale
        }
        .my-notify-info:before {
            content:"\f05a";
        }
        .my-notify-success:before {
            content:'\f00c';
        }
        .my-notify-warning:before {
            content:'\f071';
        }
        .my-notify-error:before {
            content:'\f057';
        }
        .my-notify-info {
            color: #00529B;
            background-color: #BDE5F8;
        }
        .my-notify-success {
            color: #4F8A10;
            background-color: #DFF2BF;
        }
        .my-notify-warning {
            color: #9F6000;
            background-color: #FEEFB3;
        }
        .my-notify-error {
            color: #D8000C;
            background-color: #FFD2D2;
        }


    </style>

    <!--
    ORIGINAL DIV CONTAINER
    <div class=" container    container-fixed-lg">




    </div>
    -->





    <div class="m-0 row card-block">

        <div class="col-lg-12 sm-no-padding">
            <div class="card card-transparent">
                <div class="card-block no-padding">
                    <div id="card-advance" class="card card-default">
                        <div class="card-header  ">
                            <div class="card-title">
                                Lista

                            </div>



                        </div>

                        <style>

                            tfoot {
                                display: table-header-group;
                            }

                            #listTable {
                                table-layout: fixed;
                                width: 100% !important;
                            }
                            #listTable td,
                            #listTable th{
                                width: auto !important;
                                white-space: normal;
                                text-overflow: ellipsis;
                                overflow: hidden;
                            }

                            .dataTables_wrapper .dataTables_processing{
                                position: absolute!important;
                                top: 50%!important;
                                right: 0!important;
                                bottom: 0!important;
                                margin-right: 50%!important;
                                margin-top: 15px!important;
                                background:none;
                                -webkit-transform: translate(-50%, -50%);
                                -ms-transform: translate(-50%, -50%);
                                transform: translate(-50%, -50%);
                            }

                            .dataTables_processing > div {
                                content: "";
                                width: 10px;
                                height: 10px;
                                background: red;
                                border-radius: 100%;
                                position: absolute;
                                -webkit-animation: animate 2s linear infinite;
                                animation: animate 2s linear infinite;
                            }
                            .dataTables_processing > div:nth-of-type(1) {
                                -webkit-animation-delay: -.4s;
                                animation-delay: -.4s;
                            }
                            .dataTables_processing > div:nth-of-type(2) {
                                -webkit-animation-delay: -.8s;
                                animation-delay: -.8s;
                            }
                            .dataTables_processing > div:nth-of-type(3) {
                                -webkit-animation-delay: -1.2s;
                                animation-delay: -1.2s;
                            }
                            .dataTables_processing > div:nth-of-type(4) {
                                -webkit-animation-delay: -1.6s;
                                animation-delay: -1.6s;
                            }

                            @-webkit-keyframes animate {
                                0% {
                                    left: 100px;
                                    top: 0;
                                }
                                80% {
                                    left: 0;
                                    top: 0;
                                }
                                85% {
                                    left: 0;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                90% {
                                    width: 40px;
                                    height: 15px;
                                }
                                95% {
                                    left: 100px;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                100% {
                                    left: 100px;
                                    top: 0;
                                }
                            }

                            @keyframes animate {
                                0% {
                                    left: 100px;
                                    top: 0;
                                }
                                80% {
                                    left: 0;
                                    top: 0;
                                }
                                85% {
                                    left: 0;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                90% {
                                    width: 20px;
                                    height: 10px;
                                }
                                95% {
                                    left: 100px;
                                    top: -10px;
                                    width: 10px;
                                    height: 10px;
                                }
                                100% {
                                    left: 100px;
                                    top: 0;
                                }
                            }

                        </style>
                        <div class="card-block">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-linetriangle" data-init-reponsive-tabs="dropdownfx">
                                <li class="nav-item">
                                    <a href="#" class="active" data-toggle="tab" data-target="#fade1"><span>Atuais</span>  {{$new}}</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" data-toggle="tab" data-target="#fade2"><span>Antigas</span> {{$changed}}</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" data-toggle="tab" data-target="#fade3"><span>Cancelamentos</span> {{$canceled}}</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="fade1">

                                    <table class="table table-hover demo-table-search table-responsive-block" id="listTable">
                                        <thead>
                                        <tr>

                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>




                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>




                                        </tr>
                                        </tfoot>

                                    </table>

                                </div>
                                <div class="tab-pane fade" id="fade2">
                                    <table class="table table-hover demo-table-search table-responsive-block" id="listTable2">
                                        <thead>
                                        <tr>

                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>




                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>




                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                                <div class="tab-pane fade" id="fade3">
                                    <table class="table table-hover demo-table-search table-responsive-block" id="listTable3">
                                        <thead>
                                        <tr>

                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>




                                        </tr>
                                        </thead>

                                        <tfoot>
                                        <tr>
                                            <th>TOM</th>
                                            <th>Reserva</th>
                                            <th>Estado</th>
                                            <th>Checkin</th>
                                            <th>Dia</th>
                                            <th>Chegada</th>
                                            <th>Voo C</th>
                                            <th>Hora C</th>
                                            <th>Destino</th>
                                            <th>Voo S</th>
                                            <th>Saída</th>
                                            <th>Hora S</th>
                                            <th>Destino</th>
                                            <th>Nomes</th>
                                            <th>Essen</th>
                                            <th>Hotel</th>
                                            <th>Guia</th>
                                            <th>Tip. Quarto</th>
                                            <th>Cod. Quarto + Regime</th>
                                            <th>Noites</th>
                                            <th></th>



                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>








                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade slide-up disable-scroll" id="destroyBookingModal" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h5>Excluir Reserva</h5>
                    </div>
                    <div class="modal-body">
                        <p class="no-margin">Excluir Reserva <span id="modalBooking"></span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="destroyBookingButton" class="btn btn-danger btn-cons  pull-left inline" data-dismiss="modal">Excluir</button>
                        <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>







    <div class="modal fade slide-up disable-scroll" id="validateBookingModal" tabindex="-1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content-wrapper">
                <div class="modal-content">
                    <div class="modal-header clearfix text-left">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                        </button>
                        <h5>Validar Reserva</h5>
                    </div>
                    <div class="modal-body">
                        <p class="no-margin">Validar Reserva <span id="validateBooking"></span> ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="validarBookingButton" class="btn btn-success btn-cons  pull-left inline" data-dismiss="modal">Validar</button>
                        <button type="button" class="btn btn-default btn-cons no-margin pull-left inline" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>



    @include('partial.editModal')

    @include('partial.scripts')


    <script src="{{ asset ('plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/datatables-buttons/dataTables.buttons.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/buttons.bootstrap4.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/datatables-buttons/buttons.flash.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/jszip.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/pdfmake.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/vfs_fonts.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/buttons.html5.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-buttons/buttons.print.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/jquery-datatable/media/js/dataTables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-responsive/js/datatables.responsive.js') }}"  type="text/javascript"></script>
    <script src="{{ asset ('plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset ('plugins/bootstrap-collapse/bootstrap-tabcollapse.js') }}" type="text/javascript"></script>









    <script>
        // Initialize datatable showing a search box at the top right corner

        (function() {
            var days = ["D", "S", "T", "QT", "QN", "SX", "SB"];


            Date.prototype.getDayName = function() {
                return days[ this.getDay() ];
            };

        })();

        function tableSettings(state) {

            var settings = {

                dom: "< <'col-md-4 pull-left' l> <'col-md-4 pull-left' B> <'col-md-4 pull-right'f> >" +
                "tr" +
                "< <'col-md-6 pull-left'i> <'col-md-6 pull-right'p> >",


                language:
                    {
                        "sProcessing": "<div></div><div></div><div></div><div></div><div></div>",
                        "sLengthMenu": "Mostrar _MENU_ registos",
                        "sZeroRecords": "Não foram encontrados resultados",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                        "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                        "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                        "sInfoPostFix": "",
                        "sSearch": "Procurar:",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "Primeiro",
                            "sPrevious": "Anterior",
                            "sNext": "Seguinte",
                            "sLast": "Último"
                        }


                    },
                buttons: [
                    {
                        text: 'Exportar Reservas',
                        action: function ( e, dt, node, config ) {
                            var state = $("ul.nav-tabs li a.active span").text();
                            window.open("{{ route('exportImport') }}?view=import&state=" + state);
                        }
                    }
                ],

                deferRender: true,
                processing: true,
                serverSide: true,
                lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                ajax: {
                    "url": "{{ url('listData') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {
                        _token: "{{csrf_token()}}",
                        state: state
                    }
                },



                columns: [
                    {
                        "data": "Tom",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {"data": "Booking"},
                    {"data": "State"},
                    {"data": "checkIn"},
                    {
                        "data": "Day",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {"data": "DepartureDate"},
                    {"data": "ArrivalFligh"},
                    {"data": "ArrivalTime"},
                    {"data": "DepartureLocation"},
                    {"data": "DepartureFlight"},
                    {"data": "ArrivalDate"},
                    {"data": "DepartureLocation"},
                    { "data": "ArrivalLocation",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false  },
                    {"data": "Names"},
                    {
                        "data": "Essen",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {"data": "Hotel"},
                    {
                        "data": "Guide",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {
                        "data": "RoomType",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {
                        "data": "RoomCode",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {
                        "data": "Nights",
                        "visible": false,
                        "searchable" : false,
                        "orderable" : false
                    },
                    {
                        mRender: function (data, type, row) {

                            return '<button class="btn btn-complete btn-xs m-t-10" onclick="editBooking(' + row.ID + ')">Editar</button> <button class="btn btn-success btn-xs m-t-10">Reenviar</button> <button token=""  onclick="destroyBooking(' + "'" + row.ID + "'" + ',' + "'" + row.Booking + "'" + ')" class="btn btn-danger btn-xs m-t-10">Excluir</button> '
                        },
                        orderable: false
                    }


                    //{ "data": "options" }
                ],

            };

            return settings;
        }


        function inputHtml(placeHolder,type){



            return '<div class="col-md-5">\n' +
                '<div class="form-group form-group-default ">\n' +
                '\n' +
                '<input type="text" dType="' + type + '" class="form-control ' + type + '" name="" placeholder="' + placeHolder + '">\n' +
                '</div>\n' +
                '</div>'
        }

        function formatDate(d) {
            if(d !== ''){
                let parts = d.split('/');
                return parts[2] + '-' + parts[1] + '-' + parts[0]
            }else{
                return ''
            }
        }

        function isDate(d) {

            let regex = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;



            return regex.test(d);

        }

        function renderTable(tableName,state){

            let classType;
            let title = $(this).text();



            $('#' + tableName + ' tfoot th').each( function () {
                let classType;
                let title = $(this).text();



                if(title !== ''){

                    if(title === 'Data' | title === 'Chegada' | title === 'Saída' ){
                        classType = 'datePickers';
                    }else if(title === 'Hora C' | title === 'Hora S'){
                        classType = 'timeMask';
                    }else{
                        classType = '';
                    }



                    $(this).html( inputHtml('Buscar',classType) );
                }

            } );




        var table = $('#'+tableName).DataTable(tableSettings(state));

            $('.datePickers').datepicker({
                format: 'dd/mm/yyyy'
            })

            // search box for table
            $('#search-table').keyup(function() {
                table.fnFilter($(this).val());
            });



            //filter

            table.columns().every( function () {
                var that = this;


                //$('#test').datepicker('getDate').toLocaleDateString()

                $( 'input', this.footer() ).on( 'change keyup', function () {

                    var inputType = $(this)["0"].attributes[1].value;

                    var value = inputType === 'datePickers' ?formatDate(this.value):this.value;

                    if ( that.search() !== value ) {
                        that
                            .search( value )
                            .draw();
                    }
                } );


            } );


            return table;

        }

        var table1 = renderTable('listTable','BU');
        var table2 =  renderTable('listTable2','UN');
        var table3 = renderTable('listTable3','ST');




        //modal

        var tom = $('#tom');
        var   booking  =  $('#booking');
        var  bookingNumber =  $('#bookingNumber');
        var bookingdate =  $('#bookingdate');
        var  state =  $('#state');
        var  price =  $('#price');

        //  [array]
        var  name  = $('#name');
        var age =  $('#age');
        var  gender =  $('#gender');
        var  room  = $('#room');

        var flight  = $('#flights');
        var arrivalDate  = $('#arrivalDate');
        var  arrivalFlight =  $('#arrivalFlight');
        var time =  $('#time');
        var day =  $('#day');
        var  departureDate =  $('#departureDate');
        var  departureFlight =  $('#departureFlight');
        var  departureTime  = $('#departureTime');
        var  destiny  = $('#destiny');

        var hotel  = $('#hotel');
        var essen  = $('#essen');
        var transfer  = $('#transfer');

        // [array]
        var roomType =  $('#roomType');
        var nights =  $('#nights');
        var quartoRegime  =  $('#quartoRegime'); //(roomcode)
        var members =  $('#members');
        var roomNotes =  $('#roomNotes');

        var typeAuto  = $('#typeAuto');
        var rentacarOption  = $('#rentacarOption');
        var carNotes  = $('#rentACarNotes');

        function rowDetails(xhr) {

            var table  = $('#detailedTable tbody');

            $.each(xhr, function( index, data ) {

                table.append(
                    '<tr>' +
                    '<td>' +  data.invoice + ' </td>' +
                    '<td>' +  data.state + ' </td>' +
                    '<td>' +  data.name + ' </td>' +
                    '<td>' +  data.booked + ' </td>' +
                    '</tr>'+
                    '<tr class="dtl">' + _format(data) + '</tr>'
                )

                console.log( index.id + ": " + data.id );
            });

            $(function() {
                $("#detailedTable tbody tr.dtl").hide();

                $("#detailedTable tbody").on('click', 'tr', function(event) {
                    event.stopPropagation();

                    var $target = $(event.target);

                    if(!$target.closest("tr").next().is(":visible")){
                        $target.closest("tr").next().removeAttr( "style" )
                    }




//            if ( $target.find("td").attr("colspan") == 4 ) {
//                console.log('1')
//                $target.slideUp();
//            } else {
//                $target.closest("tr").next();
//            }
                });
            });

//    $('#detailedTable tbody').on('click', 'tr', function (event) {
//
//        event.stopPropagation();
//
//        var tr = $(this).closest('tr');
//        var row = tablex.row( tr );
//
//        if ( row.child.isShown() ) {
//            // This row is already open - close it
//            row.child.hide();
//            tr.removeClass('shown');
//
//        }
//        else {
//            // Open this row
//            row.child( _format(row.data()) ).show();
//            tr.addClass('shown');
//        }
//
//
//
//
//    } );





        }


        function drawClientCard(client,mapBookRooms){

            var clientCard = '';

            clientCard += '<div class="row clearfix">';

            clientCard += '<div class="col-md-3">';
            clientCard += '<div class="form-group form-group-default " >';
            clientCard += '<label>Nome</label>';
            clientCard += '<input type="text" rid="' + client.id + '" class="form-control" value="' + client.name + '" id="name_' + client.id + '" name="name" >';
            clientCard += '</div>';
            clientCard += '</div>';

            clientCard += '<div class="col-md-2">';
            clientCard += '<div class="form-group form-group-default " >';
            clientCard += '<label>Idade</label>';
            clientCard += '<input type="text" rid="' + client.id + '" class="form-control" value="' + client.age + '" id="age' + client.id + '" name="age" >';
            clientCard += '</div>';
            clientCard += '</div>';

            clientCard += '<div class="col-md-2">';
            clientCard += '<div class="form-group form-group-default form-group-default-select2 ">';
            clientCard += '<label class="">Gênero</label>';
            clientCard += '<select  rid="' + client.id + '" class="form-control" value="' + client.gender + '" id="gender' + client.id + '" name="gender" class="full-width initSelect2" >';
            clientCard += '<option value=""></option>';
            clientCard += '<option value="H">H</option>';
            clientCard += '<option value="F">F</option>';
            clientCard += '<option value="K">K</option>';
            clientCard += '</select>';
            clientCard += '</div>';
            clientCard += '</div>';

            clientCard += '<div class="col-md-5">';
            clientCard += '<div class="form-group form-group-default form-group-default-select2 ">';
            clientCard += '<label class="">Quarto</label>';
            clientCard += '<select  rid="' + client.id + '" id="room' + client.id + '" name="room_id" class="full-width initSelect2 " >';
            clientCard += '<option value=""></option>';
            clientCard += '</select>';
            clientCard += '</div>';
            clientCard += '</div>';

//            clientCard += '<div class="col-md-5">';
//            clientCard += '<div class="form-group form-group-default form-group-default-select2 clientRoom">';
//            clientCard += '<label class="">Quarto</label>';
//            clientCard += '<select  rid="' + client.id + '"  id="room' + client.id + '" name="room_id" class="full-width initSelect2 required" required>';
//            clientCard += '<option value=""></option>';
//            clientCard += '</select>';
//            clientCard += ' </div>';
//            clientCard += '</div>';



            clientCard += '</div>';


            return clientCard;

        }

        function drawRoomsCards(room){

            var members = '';

            /*

                     */

            if( room.members  ) {

                $.each(room.members.split("/"), function (index, member) {
                    members += '<span class="tag label label-danger">';
                    members += member;
                    members += '</span>';
                });
            }else{
                members += '<span class="tag label label-danger">';
                //members += member;
                members += '</span>';

            }



            var roomCard = '';

            roomCard += '<div container="rooms" class="form-group-attached">';
            roomCard += '<div class="row clearfix">';
            roomCard += '<div class="col-md-5">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Hotel</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" id="hotel' + room.id + '" name="hotel_id" value="' + room.hotel + '" >';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '<div class="col-md-1">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Essen</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" id="essen' + room.id + '" name="essen" value="' + room.essen + '">';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Quarto + Regime</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" value="' + room.room_code + '" id="quartoRegime' + room.id + '" name="room_code">';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';



            roomCard += '<div class="row clearfix">';

            roomCard += '<div class="col-md-8">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Tipo de Quarto</label>';
            roomCard += '<input type="text" rid="' + room.id +'" class="form-control" value="' + room.room_type + '" id="roomType' + room.id + '" name="room_type">';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '<div class="col-md-1">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Noites</label>';
            roomCard += '<input type="text" rid="' + room.id + '" class="form-control" value="' + room.nights + '" id="nights' + room.id + '" name="nights">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '</div>';


            roomCard += '<div class="row">';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default">';
            roomCard += '<label>Check In</label>';
            roomCard += '<input rid="' + room.id + '" id="checkIn' + room.id + '" value="' + room.check_in+ '" name="check_in" class="form-control dateMask">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default">';
            roomCard += '<label>Check Out</label>';
            roomCard += '<input rid="' + room.id + '" id="checkOut' + room.id + '" value="' + room.check_out+ '"  name="check_out" class="form-control dateMask">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '<div class="col-md-3">';
            roomCard += '<div class="form-group form-group-default">';
            roomCard += '<label>Valor</label>';
            roomCard += '<input rid="' + room.id + '" id="price' + room.id +'" value="' + room.price+ '"  name="price" type="text" data-a-dec="," data-a-sep="." class="autonumeric form-control">';
            roomCard += '</div>';
            roomCard += '</div>';

            roomCard += '</div>';

            roomCard += '<div class="row">';
            roomCard += '<div class="col-md-9">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>Membros</label>';
            roomCard += '<input class="tagsinput custom-tag-input" type="text" value="hello World, quotes, inspiration" style="display: none;">';
            roomCard += '<div class="bootstrap-tagsinput" id="members' + room.id + '">';

            roomCard += members;

            roomCard += '<input size="1" type="text">';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '<div class="row">';
            roomCard += '<div class="col-md-9">';
            roomCard += '<div class="form-group form-group-default ">';
            roomCard += '<label>OBS</label>';

            roomCard += '<textarea rid="' + room.id + '" class="form-control"   id="roomNotes' + room.id + '" name="notes" rows="5">';
            roomCard +=  room.room_notes;
            roomCard += '</textarea>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</div>';
            roomCard += '</br>';




            return roomCard;

        }

        function drawPackageCards(package) {



            var packagecard = '';
            var tauchen = '';


            if (package.tuachen_pax !== ''){

                tauchen += ' Tauchen Pax: ' + package.tuachen_pax;
                tauchen += ' Tauchen Qnt: ' + package.tuachen_qty;

            }

            packagecard += '<span class="tag label label-danger">' + package.name + ' Days: ' + package.days + ' Estado: ' + package.state + ' Client: ' + package.client + tauchen + ' Price: ' + package.price;
            packagecard += '<span data-role="remove" id="' + package.id + '" ></span>';
            packagecard += '</span>';

            return packagecard;


        }

        function editBooking(id) {

            console.log( id );


            var form = $('#editBooking');
            $(document).ready(function () {
                $('.initSelect2').select2();


            });

            $.get("bookings/" + id, function (data) {
                var result = ( JSON.parse(data)  );
                console.log(result);

                var changeForm = $("#changeBooking");

                // result.booking["0"].arrival_flight

                if (result.booking["0"] ) {

                    $('#editBooking').toggle();
                    tom.val(result.booking["0"].tom);
                    booking.val(result.booking["0"].booking_number);
                    bookingNumber.attr('bookId', result.booking["0"].booking_id);
                    bookingNumber.text(result.booking["0"].booking_number);
                    bookingdate.val(result.booking["0"].booking_date);
                    // state.val( result.booking["0"].state ).trigger('change');
                    $('#state').val(result.booking["0"].state).trigger('change');
                    arrivalDate.val(result.booking["0"].arrival_date);
                    arrivalFlight.val(result.booking["0"].arrival_flight);
                    time.val(result.booking["0"].arrival_time);
                    day.val(result.booking["0"].day);
                    flight.attr('rid', result.booking["0"].flightid);
                    departureDate.val(result.booking["0"].departure_date);
                    departureFlight.val(result.booking["0"].departure_flight);
                    departureTime.val(result.booking["0"].departure_time);
                    destiny.val(result.booking["0"].destiny);


                    if(result.booking["0"].departure_date !== null ){
                        var now = new Date(result.booking["0"].departure_date);
                        var dayVal = now.getDayName();
                        day.val(dayVal);

                    }

                    hotel.val(result.booking["0"].hotel);
                    essen.val(result.booking["0"].essen);
                    var transferVal = result.booking["0"].transfer;

                    if (transferVal == '' || !transferVal || transferVal == null){
                        transferVal = 'Não Tem'
                    }

                    $('#transfer').val(transferVal).trigger('change');
                    //[array]
                    roomType.val();
                    // // nights.val();
                    //   quartoRegime.val();   //(roomcode)
                    //  members.val();
                    //   roomNotes.val();

                    if(result.car["0"]){

                        rentacarOption.val(result.car["0"].rentacar_id);
                        carNotes.val(result.car["0"].notes);
                        carPrice.val(result.car["0"].price);
                        $('#typeAuto').val(result.car["0"].type).trigger('change');
                    }




                    var mapBookRooms = $.map(result.rooms, function (room) {
                        return {id: room.id, text: room.room_type};
                    });

                    $.each(result.clients, function (index, client) {
                        $('#clientContainer').append(drawClientCard(client, mapBookRooms));
                        var gender = $('#gender' + client.id);
                        var room = $('#room' + client.id)
                        gender.select2();
                        room.select2({
                            data: mapBookRooms
                        });
                        room.val(client.room).trigger('change');
                        gender.val(client.gender).trigger('change');
                    });

                    $.each(result.rooms, function (index, room) {
                        $('#roomContainer').append(drawRoomsCards(room));
                    });

                    $.each(result.packages, function (index, package) {
                        if (package.name !== null) {
                            $('#packagesContainer').append(drawPackageCards(package));
                        }
                    });

                    state.val(result.booking["0"].state).trigger('change');


                    //  $('#bookingEditModal').modal();


                    $("#c_room").select2({
                        data: mapBookRooms
                    });

                    var mapBookClients = $.map(result.clients, function (client) {
                        return {id: client.id, text: client.name};
                    });


                    $("#p_clients").select2({
                        data: mapBookClients
                    });


                    //change funtion
                    {{--$('#editBooking input ,#editBooking select,#editBooking textarea').on('change', function() {--}}



                    {{--var  book = $('#bookingNumber').attr('bookid');--}}
                    {{--var  fieldValue = $(this).val();--}}
                    {{--var  fieldName = $(this).attr('name');--}}
                    {{--var  fieldContainer = $(this).closest('.form-group-attached').attr('container');--}}
                    {{--var  rid = $(this).attr('rid');--}}
                    {{--var  fid = flight.attr('rid');--}}
                    {{--var label = $(this).parent().find('label').text();--}}

                    {{--var url = "{{ route('bookings.update', ['id' => ':id']) }}";--}}
                    {{--url = url.replace(':id', book);--}}

                    {{--$.ajax({--}}
                    {{--url: url,--}}
                    {{--dataType: 'json',--}}
                    {{--type: 'PUT',--}}
                    {{--contentType: 'application/json',--}}
                    {{--data:--}}
                    {{--JSON.stringify( {--}}
                    {{--"_token": "{{csrf_token()}}",--}}
                    {{--"book" : book,--}}
                    {{--"fieldValue": fieldValue,--}}
                    {{--"fieldName": fieldName,--}}
                    {{--"fieldContainer": fieldContainer,--}}
                    {{--"rid": rid,--}}
                    {{--"label": label,--}}
                    {{--"fid":fid--}}


                    {{--} ),--}}
                    {{--complete: function(xhr, textStatus) {--}}

                    {{--if(xhr.status == '200' || xhr.status == 200){--}}

                    {{--$('#bookingEditModal').pgNotification({--}}
                    {{--style: 'flip',--}}
                    {{--message: xhr.responseText,--}}
                    {{--position: 'top-right',--}}
                    {{--timeout: 4500,--}}
                    {{--type: 'Success'--}}
                    {{--}).show();--}}

                    {{--}else{--}}

                    {{--$('#bookingEditModal').pgNotification({--}}
                    {{--style: 'flip',--}}
                    {{--message: xhr.responseText,--}}
                    {{--position: 'top-right',--}}
                    {{--timeout: 3500,--}}
                    {{--type: 'Danger'--}}
                    {{--}).show();--}}

                    {{--}--}}
                    {{--}--}}
                    {{--});--}}




                    {{--});--}}
                }

                if( result.cbooking["0"]){
                    changeForm.toggle();
                    console.log(result.cbooking["0"]);
                    $('#historytom').val(result.cbooking["0"].tom);
                    $('#historynumber').val(result.cbooking["0"].invoice);
                    $('#historydate').val(result.cbooking["0"].booked);
                    $('#historystate').val(result.cbooking["0"].state).trigger('change');
                    $('#historyClient').val(result.cbooking["0"].name);
                    $('#historyAge').val(result.cbooking["0"].age);

                    $('#historyarrivalDate').val(result.cbooking["0"].season);
                    $('#historyarrivalFlight').val(result.cbooking["0"].flight_number);
                    $('#historytime').val(result.cbooking["0"].arrival_time);
                    $('#historyarrivalLocation').val(result.cbooking["0"].arrival_location);

                    $('#historydepartureDate').val(result.cbooking["0"].departure_date);
                    $('#historydepartureFlight').val(result.cbooking["0"].connected_load);
                    $('#historydepartureTime').val(result.cbooking["0"].departure_time);
                    $('#historyDestiny').val(result.cbooking["0"].departure_location);

                    $('#historyHotel').val(result.cbooking["0"].hotel_name);
                    $('#historyHotelCode').val(result.cbooking["0"].hotel);
                    $('#historyRoom').val(result.cbooking["0"].room_name);
                    $('#historyRoomCode').val(result.cbooking["0"].room);
                    $('#historyValue').val(result.cbooking["0"].price);
                    $('#historyCheckIn').val(result.cbooking["0"].check_in);
                    $('#historyCheckOut').val(result.cbooking["0"].check_out);
                    $('#historyNights').val(result.cbooking["0"].days);
                    $('#historyDay').val(result.cbooking["0"].day);
                    $('#historyRoomsQty').val(result.cbooking["0"].number_of_rooms);
                    $('#historyNotes').val(result.cbooking["0"].notes);

                }


                $('#bookingEditModal').modal();

                $( "#bookingEditModal" ).on('shown.bs.modal', function(){
                    $("textarea").each(function(textarea) {
                        $(this).height( $(this)[0].scrollHeight );
                    });
                });

                $('#bookingEditModal').on("hidden.bs.modal", function () {


                    $('#clientContainer').empty();
                    $('#roomContainer').empty();
                    $('#packagesContainer').empty();
                    changeForm.trigger('reset');
                    changeForm.hide();
                    $('#editBooking').hide();
                    $('#changeBooking').trigger('reset');


                })
            });


        }

        {{--function destroyBooking(id,bookingNumber,state) {--}}

            {{--$('#modalBooking').text(bookingNumber);--}}

            {{--$('#destroyBookingModal').modal().one('click', '#destroyBookingButton', function(e) {--}}


                {{--var state = $("ul.nav-tabs li a.active span").text();--}}

                {{--$.ajax({--}}
                    {{--type: 'DELETE',--}}
                    {{--data: {--}}
                        {{--_token :'{{ csrf_token() }}',--}}
                        {{--state:'state'--}}
                    {{--},--}}
                    {{--url: "{!! url('bookings' ) !!}" + "/" + id,--}}
                    {{--success: function (data) {--}}



                        {{--$('.page-container').pgNotification({--}}
                            {{--style: 'flip',--}}
                            {{--message: 'Reserva ' + bookingNumber + ' excluída com sucesso.',--}}
                            {{--position: 'top-right',--}}
                            {{--timeout: 4500,--}}
                            {{--type: 'Success'--}}
                        {{--}).show();--}}

                        {{--switch(state) {--}}
                            {{--case 'Novas':--}}
                                {{--table1.ajax.reload();--}}
                                {{--break;--}}
                            {{--case 'Alterações':--}}
                                {{--table2.ajax.reload();--}}
                                {{--break;--}}
                            {{--case 'Cancelamentos':--}}
                                {{--table3.ajax.reload();--}}
                                {{--break;--}}
                        {{--}--}}



                    {{--}--}}
                {{--});--}}

            {{--});--}}

        {{--}--}}


        function destroyBooking(id,bookingNumber) {

            $('#modalBooking').text(bookingNumber);

            $('#destroyBookingModal').modal().one('click', '#destroyBookingButton', function(e) {

                var state = $("ul.nav-tabs li a.active span").text();



                $.ajax({
                    type: 'DELETE',
                    data: { _token :'{{ csrf_token() }}'},
                    url: "{!! url('bookings' ) !!}" + "/" + id,
                    success: function (data) {



                        $('.page-container').pgNotification({
                            style: 'flip',
                            message: 'Reserva ' + bookingNumber + ' excluída com sucesso.',
                            position: 'top-right',
                            timeout: 4500,
                            type: 'Success'
                        }).show();

                        switch(state) {
                            case 'Atuais':
                                table1.ajax.reload();
                                break;
                            case 'Antigas':
                                table2.ajax.reload();
                                break;
                            case 'Cancelamentos':
                                table3.ajax.reload();
                                break;
                        }



                    }
                });

            });

        }

        {{--function validateBooking(id,bookingNumber,state) {--}}

            {{--$('#validateBooking').text(bookingNumber);--}}

            {{--$('#validateBookingModal').modal().one('click', '#validarBookingButton', function(e) {--}}




                {{--$.ajax({--}}
                    {{--type: 'PUT',--}}
                    {{--url: "{!! url('bookings' ) !!}" + "/" + id,--}}
                    {{--data: {--}}
                        {{--_token :'{{ csrf_token() }}',--}}
                        {{--bookId : id--}}
                    {{--},--}}

                    {{--success: function (data) {--}}



                        {{--$('.page-container').pgNotification({--}}
                            {{--style: 'flip',--}}
                            {{--message: 'Reserva ' + bookingNumber + ' validada com sucesso.',--}}
                            {{--position: 'top-right',--}}
                            {{--timeout: 4500,--}}
                            {{--type: 'Success'--}}
                        {{--}).show();--}}

                        {{--switch(state) {--}}
                            {{--case 'BU':--}}
                                {{--table1.ajax.reload();--}}
                                {{--break;--}}
                            {{--case 'UU':--}}
                                {{--table2.ajax.reload();--}}
                                {{--break;--}}
                            {{--case 'US':--}}
                                {{--table2.ajax.reload();--}}
                                {{--break;--}}
                            {{--case 'ST':--}}
                                {{--table3.ajax.reload();--}}
                                {{--break;--}}
                        {{--}--}}

                    {{--}--}}
                {{--});--}}

            {{--});--}}

        {{--}--}}

        {{--function validateImport() {--}}

            {{--$('#validateImportModal').modal().one('click', '#validateImportButton', function(e) {--}}




                {{--$.ajax({--}}
                    {{--type: 'PUT',--}}
                    {{--data: { _token :'{{ csrf_token() }}'},--}}
                    {{--url: "{!! url('validateImport' ) !!}",--}}
                    {{--complete: function (data) {--}}


                        {{--window.location = './home';--}}


                    {{--}--}}
                {{--});--}}

            {{--});--}}

        {{--}--}}

        {{--function cancelImport() {--}}

            {{--$('#cancelImportModal').modal().one('click', '#cancelImportButton', function(e) {--}}




                {{--$.ajax({--}}
                    {{--type: 'DELETE',--}}
                    {{--data: { _token :'{{ csrf_token() }}'},--}}
                    {{--url: "{!! url('cancelImport' ) !!}",--}}
                    {{--complete: function (data) {--}}

                        {{--window.location = './home';--}}


                    {{--}--}}
                {{--});--}}


            {{--});--}}



        {{--}--}}

        function updateBooking(){

            var changedFields = $('.changed');

            $.each(changedFields, function( index, field ) {
                console.log($('bookingNumber').attr('bookid'));
                console.log($(this).val());
                console.log($(this).attr('name'));
            });

        }

        function createClient(){

            var form = $('#clientModalForm');
            var modalForm = $('#bookingEditModal');

            if(form.valid()){

                $.ajax({
                    url: '{{ route('bookings.store') }}',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data:
                        JSON.stringify( {
                            "_token": "{{csrf_token()}}",
                            "bookId" : $('#bookingNumber').attr('bookid'),
                            "roomId": $('#c_room').val(),
                            "gender": $('#c_gender').val(),
                            "age": $('#c_age').val(),
                            "name": $('#c_name').val(),
                            "container": "Cliente"
                        } ),
                    complete: function(xhr, textStatus) {

                        if(xhr.status == '200' || xhr.status == 200){

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                            $('#addClientModal').modal('toggle')

                        }else{

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });



            }


        }
        function createRoom(){

            var form = $('#roomModalForm');
            var modalForm = $('#bookingEditModal');

            if(form.valid()){

                $.ajax({
                    url: '{{ route('bookings.store') }}',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data:
                        JSON.stringify( {
                            "_token": "{{csrf_token()}}",
                            "bookId" : $('#bookingNumber').attr('bookid'),
                            "roomType": $('#r_room').val(),
                            "roomCode": $('#r_code').val(),
                            "nights": $('#r_nights').val(),
                            "checkOut": $('#r_checkout').val(),
                            "checkIn": $('#r_checkin').val(),
                            "hotelId": $('#r_hotel').val(),
                            "essen": $('#r_essen').val(),
                            "price": $('#r_price').val(),
                            "notes": $('#r_notes').val(),
                            "container": "Quarto"



                        } ),
                    complete: function(xhr, textStatus) {

                        if(xhr.status == '200' || xhr.status == 200){

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                            $('#addRoomModal').modal('toggle')



                        }else{

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });



            }

        }
        function createPackage() {

            var form = $('#packageModalForm');
            var modalForm = $('#bookingEditModal');

            if(form.valid()){

                $.ajax({
                    url: '{{ route('bookings.store') }}',
                    dataType: 'json',
                    type: 'POST',
                    contentType: 'application/json',
                    data:
                        JSON.stringify( {
                            "_token": "{{csrf_token()}}",
                            "bookId" : $('#bookingNumber').attr('bookid'),
                            "package": $('#p_package').val(),
                            "state": $('#p_state').val(),
                            "days": $('#p_days').val(),
                            "hotelId": $('#p_hotel').val(),
                            "price": $('#p_price').val(),
                            "tpax": $('#p_pax').val(),
                            "tqty": $('#p_tqty').val(),
                            "clientId": $('#p_clients').val(),
                            "notes": $('#p_notes').val(),
                            "container": "Pacote"



                        } ),
                    complete: function(xhr, textStatus) {

                        if(xhr.status == '200' || xhr.status == 200){

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 4500,
                                type: 'Success'
                            }).show();

                            $('#addPackageModal').modal('toggle')

                        }else{

                            modalForm.pgNotification({
                                style: 'flip',
                                message: xhr.responseText,
                                position: 'top-right',
                                timeout: 3500,
                                type: 'Danger'
                            }).show();

                        }
                    }
                });



            }

        }

        /*

            //file input stuff
            $('#sumit-import').click(function(){
                if(form.valid()){

                    form.validate();

                    if(form.valid()){
                        $('.pace').removeClass('pace-inactive')
                    }
                }
            });
            */




    </script>

@endsection