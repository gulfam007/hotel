@extends('layouts.app')

@section('content')
    <style>
        .input-file-container {
            position: relative;
            width: 225px;
        }
        .js .input-file-trigger {
            display: block;
            padding: 14px 45px;
            background: #39D2B4;
            color: #fff;
            font-size: 1em;
            transition: all .4s;
            cursor: pointer;
        }
        .js .input-file {
            position: absolute;
            top: 0; left: 0;
            width: 225px;
            opacity: 0;
            padding: 14px 0;
            cursor: pointer;
        }
        .js .input-file:hover + .input-file-trigger,
        .js .input-file:focus + .input-file-trigger,
        .js .input-file-trigger:hover,
        .js .input-file-trigger:focus {
            background: #34495E;
            color: #39D2B4;
        }

        .file-return {
            margin: 0;
        }
        .file-return:not(:empty) {
            margin: 1em 0;
        }
        .js .file-return {
            font-style: italic;
            font-size: .9em;
            font-weight: bold;
        }
        .js .file-return:not(:empty):before {
            content: "Ficheiro selecionado: ";
            font-style: normal;
            font-weight: normal;
        }
        </style>

    <div class=" container    container-fixed-lg">
    <div class="card card-transparent">
        <div class="card-header  ">

            </div>
        </div>
        <div class="m-0 row card-block">

            <div class="col-lg-12 sm-no-padding">
                <div class="card card-transparent">
                    <div class="card-block no-padding">
                        <div id="card-advance" class="card card-default">
                            <div class="card-header  ">
                                <div class="card-title">
                                    Importação de Reservas

                                </div>

                            </div>
                            <div class="card-block">
                                <h3>
                                    <span class="semi-bold">Nova</span> Importação</h3>

                                <form id="uploadFile" action="{{ route('importFile') }}" role="form" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="row">

                                        <div class="col-md-3">

                                            <div class="form-group form-group-default form-group-default-select2 required">
                                                <label class="">Formato</label>
                                                <select id="format" class="full-width" data-placeholder="Select Country" required>
                                                    @foreach($importConfigDropdownOptions as $option)
                                                        <option value="{{ $option->id }}">
                                                            {{ $option->format }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>


                                        </div>

                                        <div class="col-md-6">

                                            <div class="input-file-container">


                                                <label tabindex="0" for="my-file" class="btn btn-danger triggerx">Selecione...</label>
                                                <input type="file" class="input-file" id="fileimport" name="fileimport"   required >
                                            </div>
                                            <div class="pace pace-inactive">
                                                    <div class="pace-progress-inner"></div>
                                                </div>

                                            <p class="file-return"></p>
                                        </div>

                                    </div>
                                    <br>

                                    <div class="clearfix pull-right">
                                        <button class="btn btn-danger" id="sumit-import" type="submit">Enviar</button>
                                    </div>
                                </form>

                                <br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>




    @include('partial.scripts')
    <script>
        var form = $('#uploadFile')
      $(document).ready(function() {

          $('#format').select2()
      });



        //file input stuff
        document.querySelector("html").classList.add('js');

        var fileInput  = document.querySelector( ".input-file" ),
            button     = document.querySelector( ".triggerx" ),
            the_return = document.querySelector(".file-return");

        button.addEventListener( "keydown", function( event ) {
            if ( event.keyCode == 13 || event.keyCode == 32 ) {
                fileInput.focus();
            }
        });

        button.addEventListener( "click", function( event ) {
            fileInput.focus();
            return false;
        });
        fileInput.addEventListener( "change", function( event ) {
            the_return.innerHTML = this.value;
        });
        $('#sumit-import').click(function(){
            if(form.valid()){

                form.validate();

                if(form.valid()){
                    $('.pace').removeClass('pace-inactive')
                }
            }
        });
    </script>
@endsection

