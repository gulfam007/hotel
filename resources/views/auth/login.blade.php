



<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>TravelOne - Incoming Reservations</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="{{ asset ('pages/ico/60.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset ('pages/ico/76.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset ('pages/ico/120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset ('pages/ico/152.png') }}">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="{{ asset ('plugins/pace/pace-theme-flash.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset ('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset ('plugins/font-awesome/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset ('plugins/jquery-scrollbar/jquery.scrollbar.css') }}" rel="stylesheet" type="text/css" media="screen" />

    <link href="{{ asset ('plugins/switchery/css/switchery.min.css') }}" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
</head>
<body class="fixed-header ">
<div class="login-wrapper ">
    <!-- START Login Background Pic Wrapper-->
    <div class="bg-pic">
        <!-- START Background Pic-->
        <img src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">
        <!-- END Background Pic-->
        <!-- START Background Caption-->
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
            <h2 class="semi-bold text-white">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis nisi eget quam vehiccidunt nunc id, molestie quam.</h2>
            <p class="small">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis nisi eget quam vehit nunc id, molestie quam.
            </p>
        </div>
        <!-- END Background Caption-->
    </div>
    <!-- END Login Background Pic Wrapper-->
    <!-- START Login Right Container-->
    <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
            <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo_2x.png" width="78" height="22">
            <p class="p-t-35">Aceder a Gestão de Reservas</p>
            <!-- START Login Form -->
            <form id="form-login" class="p-t-15" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label>E-mail</label>
                    <div class="controls">
                        <input type="text" name="email" placeholder="E-mail do Utilizador" class="form-control" required>
                    </div>
                </div>
                <!-- END Form Control-->
                <!-- START Form Control-->
                <div class="form-group form-group-default">
                    <label>Senha</label>
                    <div class="controls">
                        <input type="password" class="form-control" name="password" placeholder="Credenciais" required>
                    </div>
                </div>
                <!-- START Form Control-->
                <div class="row">
                    <div class="col-md-6 no-padding sm-p-l-10">
                        <div class="checkbox ">
                            <input type="checkbox" value="1" id="checkbox1" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="checkbox1">Mantenha-me logado</label>
                        </div>
                    </div>

                </div>
                <!-- END Form Control-->
                <button class="btn btn-danger btn-cons m-t-10" type="submit">Login</button>
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Esqueceu a senha?
                </a>
            </form>
            <!--END Login Form-->
            <div class="pull-bottom sm-pull-bottom">
                <div class="m-b-30 p-r-80 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
                    <div class="col-sm-3 col-md-2 no-padding">
                        <img alt="" class="m-t-5" data-src="assets/img/demo/pages_icon.png" data-src-retina="assets/img/demo/pages_icon_2x.png" height="60" src="assets/img/demo/pages_icon.png" width="60">
                    </div>
                    <div class="col-sm-9 no-padding m-t-10">
                        <p>
                            <small>
                                Lorem ipsum dolor sit am, tincidunt nunc id, molestie quam.
                                orem ipsum dol <a href="#" class="text-info">Lorem</a> ou <a href="#"
                                                                                             class="text-info">orem ipsum dol</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Login Right Container-->
</div>

<!-- BEGIN VENDOR JS -->
<script src="{{ asset ('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery/jquery-1.11.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/modernizr.custom.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/tether/js/tether.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery/jquery-easy.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-unveil/jquery.unveil.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-ios-list/jquery.ioslist.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-actual/jquery.actual.min.js') }}"></script>
<script src="{{ asset ('plugins/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>

<script type="text/javascript" src="{{ asset ('plugins/classie/classie.js') }}"></script>
<script src="{{ asset ('plugins/switchery/js/switchery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('plugins/jquery-validation/js/localization/messages_pt_PT.min.js') }}" type="text/javascript"></script>
<!-- END VENDOR JS -->
<script src="{{ asset ('pages/js/pages.min.js') }}"></script>
<script>
    $(function()
    {
        $('#form-login').validate()
    })
</script>
</body>
</html>