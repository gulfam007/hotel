@extends('layouts.app')

@section('content')

    <div class="register-container full-height sm-p-t-30">
        <div class="d-flex justify-content-center flex-column full-height ">

            <h3>
                <span class="semi-bold">Resetar</span> Senha</h3>
            <p></p>

            <form  class="p-t-15" role="form" method="POST" action="{{ route('password.request') }}" novalidate="novalidate">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            <label>Email</label>
                            <input type="email" id="email" name="email" value="{{ $email or old('email') }}"  autofocus  class="form-control" required="" aria-required="true">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            <label>Senha</label>
                            <input type="password" id="password" name="password"  class="form-control" required="" aria-required="true">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            <label>Confirmar Senha</label>
                            <input type="password" id="password-confirm"  name="password_confirmation"  class="form-control" required="" aria-required="true">
                        </div>
                    </div>
                </div>


                <button class="btn btn-danger btn-cons m-t-10" type="submit">Resetar Senha</button>
            </form>
        </div>
    </div>


@endsection
