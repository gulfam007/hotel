@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>
                    <span class="semi-bold">
                        Resetar
                    </span> Senha
                    </h3>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                        <form  class="p-t-15" role="form" method="POST" action="{{ route('password.email') }}" novalidate="novalidate">
                            {{ csrf_field() }}



                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="email" id="email" name="email" value="{{ old('email') }}"  autofocus  class="form-control" required="" aria-required="true">
                                    </div>
                                </div>
                            </div>




                            <button class="btn btn-danger btn-cons m-t-10" type="submit">Enviar Link de Reset de Senha</button>
                        </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
