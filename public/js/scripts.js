(function($) {

    'use strict';

    $(document).ready(function() {
        // Initializes search overlay plugin.
        // Replace onSearchSubmit() and onKeyEnter() with
        // your logic to perform a search and display results
        $(".list-view-wrapper").scrollbar();



    });


    $('.panel-collapse label').on('click', function(e){
        e.stopPropagation();
    })

    // auto init for parallax sets window as scrollElement.
    // set .page-container as scrollElement for horizontal layouts.
    $('.jumbotron').parallax({
      scrollElement: '.page-container'
    })

    $('.page-container').on('scroll', function() {
        $('.jumbotron').parallax('animate');
    });

    //https://github.com/colebemis/feather
    //Feather ICONS
    //Used in sidebar icons
    if(feather){
        feather.replace({
            'width':16,
            'height':16
        });
    }

})(window.jQuery);
