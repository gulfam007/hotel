<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('login');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/importarReservas', 'ImportBookingsController@index')->name('importarReservas');

Route::post('/importFile', 'ImportBookingsController@importingFile')->name('importFile');

Route::get('/minhaImportacao', 'ImportBookingsController@myImportIndex')->name('minhaImportacao');

Route::post('/minhaImportacaoData', 'ImportBookingsController@listMyImport' )->name('minhaImportacaoData');

Route::post('/listData', 'ListController@listBookings' )->name('listData');

Route::delete('/cancelImport', 'ImportBookingsController@cancelImport' );

Route::put('/validateImport', 'ImportBookingsController@validateImport' );

Route::resource('/bookings', 'BookingsController');
Route::resource('hotel', 'HotelController');
Route::post('/hotelSearch','HotelController@hotelSearch')->name('hotelSearch');

Route::get('/lista', 'ListController@index')->name('lista');

Route::get('/reset', 'ImportBookingsController@resetDatabase')->name('reset');

Route::post('/fetchhistory','BookingsController@fetchHitstory')->name('fetchhistory');

Route::get('/exportImportData',
    [
        'as' => 'exportImport',
        'uses' => 'ImportBookingsController@excelExport'
    ]);

Route::get('/exportImportDataXML',
    [
        'as' => 'exportImportXML',
        'uses' => 'ImportBookingsController@xmlExport'
    ]);

Route::post('/removeClient', 'BookingsController@removeClient')->name('removeClient');

Route::post('/removeRoom', 'BookingsController@removeRoom')->name('removeRoom');