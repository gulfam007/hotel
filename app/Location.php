<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    public $fillable=['name','transfer_price'];
    public $table = 'locations';
    public function hotel(){
        return $this->belongsToMany(Hotel::class);
    }
}
