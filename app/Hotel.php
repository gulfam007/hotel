<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public $fillable=['code','name','email','guide_id','location_id','transfer_price_mod','active'];
    public $timestamps = false;
    public $table='hotels';
    public function guides()
    {
        return $this->hasOne(Guide::class);
    }
    public function locate()
    {
        return $this->hasOne(Location::class);
    }

}
