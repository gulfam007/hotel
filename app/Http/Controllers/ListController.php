<?php

namespace App\Http\Controllers;

use App\Entity\Bookings;
use App\Entity\Hotels;
use Illuminate\Http\Request;
use App\Entity\Cars;
use App\Entity\Packages;
use App\Entity\Imports;
class ListController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }


    public function index(){

        $rentacarOptions = Cars::rentacarOptions();

       $hotelOptions = Hotels::hotelOptions();
       $packageOptions = Packages::packageOptions();
        $tomOptions = Bookings::tomOptions();
        $new = Bookings::bookingStateCount('BU', 'list');
        $canceled = Bookings::bookingStateCount('ST', 'list');
        $changed = Bookings::bookingStateCount('UN', 'list');
        $findImport = Imports::findImport();

        return view('reservas.lista',compact('rentacarOptions','hotelOptions','packageOptions','new','changed','canceled','tomOptions','findImport'));
    }

    public  function listBookings(Request $request){


        echo Bookings::listImportedBookings($request,Bookings::fetchBookings($request->state));

    }
}
