<?php

namespace App\Http\Controllers;
use App\Entity\Bookings;
use App\Entity\Cars;
use App\Entity\Clients;
use App\Entity\Flights;
use App\Entity\Imports;
use App\Entity\Packages;
use App\Entity\Rooms;
use App\Entity\Transfers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Validator;


class BookingsController extends Controller

{

    public function __construct(){

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {



    }

    public function removeClient(Request $request)
    {
        Clients::removeClient($request->id);
        return response('ok' ,200);

    }

    public function removeRoom(Request $request)
    {
        Rooms::removeRoom($request->id);
        return response('ok' ,200);

    }

    public function store(Request $request)
    {


        switch ($request->container) {

            case 'Cliente':

                $flightID = Flights::checkFlight($request->bookId);

                $clientInfo = array(
                    'bookId' => $request->bookId,
                    'flightId' => $flightID?$flightID->id:'',
                    'roomId' => $request?$request->roomId:null,
                    'name' => $request->name,
                    'gender' => $request->gender,
                    'age' => $request->age

                );

                $client = new Clients();
                $client->newClient($clientInfo);

                break;

            case 'Quarto':




                $weekdayNo = date('w', strtotime($request->checkIn));
                $weekdaysFirstLetters = array("D", "S","T","QT","QN","SX","SB");


                $checkIn = Carbon::createFromFormat('d/m/Y', $request->checkIn);
                $checkOut =  Carbon::createFromFormat('d/m/Y', $request->checkOut);



                $roomInfo = array(

                    'bookId' => $request->bookId,
                    'hotelId' => $request->hotelId,
                    'hotelConfirm' => null,
                    'checkIn' =>  $checkIn->toDateString(),
                    'checkOut' =>  $checkOut->toDateString(),
                    'nights' => $request->nights,
                    'roomType' => $request->roomType,
                    'roomCode' => $request->roomCode,
                    'checkInDay' => $weekdaysFirstLetters[$weekdayNo],
                    'notes' => $request->notes,
                    'price' => $request->price,
                    'essen' =>  $request->essen,
                    'catering' => null
                );

                $room = new Rooms();
                $newRoomId = $room->newRoom($roomInfo);
                $room->newClientRoom(null,$newRoomId,$request->bookId);

                break;

            case 'Pacote':

                $package = new Packages();

                $checkIn = Carbon::createFromFormat('d/m/Y', $request->checkin);
                $checkOut =  Carbon::createFromFormat('d/m/Y', $request->checkout);

                $package->newPackage($request->bookId,$request->package,$request->clientId,$request->tpax,$request->tqty,$request->price,$request->notes,$request->days,$request->state,$request->hotelId,$checkIn->toDateString(),$checkOut->toDateString());

                break;

        }

        return response($request->container . ' adicionado com sucesso.', 200)
            ->header('Content-Type', 'application/json');


    }

    /**
     * Display the specified resource.
     *
     * @param  book id
     * @return \Illuminate\Http\Response
     */
    public function show($bookingID)
    {

        $history = Bookings::findBookingbyBookingHistorybyId($bookingID);
        //$bookNUmber = $history?$history->invoice:Bookings::findBookingbyBookingbyId($bookingID);
        $bookId = Bookings::findBookingbyBookingNumber($history?$history->invoice:'') ;

        echo Bookings::fetchBooking($bookId?$bookId->id:$bookingID,$history?$history->id:'');

    }

    public function fetchHitstory(Request $request){
      //  Log::info( $request->bookId );
       echo Bookings::fetchHistory($request->bookId);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bookings  $bookings
     * @return \Illuminate\Http\Response
     */
    public function edit(Bookings $bookings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bookings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {


        if(isset($request->form)){



            foreach($request->form['booking'][0] as $fieldName=>$fieldValue) {



                if($fieldName == 'transfer'){

                    $checkTransfer = Transfers::checkingTransfer($request->bookId);
                    $transfer = new Transfers();

                    if( empty($checkTransfer) || !$checkTransfer ){



                        $transferInfo = array(

                            'bookId' => $request->bookId,
                            'type' => $fieldValue
                        );
                        $transfer->newTransfer($transferInfo);

                    }else{

                        $transfer->updateImportedTransfer($checkTransfer->id,$fieldValue);
                    }


                }else{



                    Bookings::updateBooking($request->bookId, $fieldName, self::is_date($fieldValue)?self::convertDate('d/m/Y', $fieldValue):$fieldValue);

                }


            }

            foreach($request->form['clients'][0] as $fieldName=>$fieldValue) {





                $client = explode("_", $fieldName);



                if($client[0] == 'room'){
                    $client_Room = explode("_", $fieldName);
                    Clients::updateClient($client[1], 'room_id', $fieldValue);
                    Clients::updateClientRoomEdit($client_Room[1],$client_Room[2],$fieldValue);

                }else{
                    Clients::updateClient($client[1], $client[0], $fieldValue);
                }


            }

            foreach($request->form['flights'][0] as $fieldName=>$fieldValue) {


                Flights::updateFlights($request->flightId, $fieldName, self::is_date($fieldValue)?self::convertDate('d/m/Y', $fieldValue):$fieldValue );
            }



            $rcol = collect($request->form['rooms']);

            $index = 0;

            while($index < $rcol->count())
            {
                $roomsCollection = collect($request->form['rooms'][$index]);

                //clock($roomsCollection->get('room_id'));
                $roomsCollectionSorted = $roomsCollection->toArray() ;
                ksort($roomsCollectionSorted);

                foreach ($roomsCollectionSorted as $fieldName=>$fieldValue) {



                    if($fieldName == 'aroom_id' ){

                        $room_id = $fieldValue;

                    }



                    if($fieldName !== 'aroom_id' ){

                        Rooms::updateRoom($room_id, $fieldName, self::is_date($fieldValue)?self::convertDate('d/m/Y', $fieldValue):$fieldValue );

                    }

                }

                $index++;
            }







            foreach($request->form['cars'][0] as $fieldName=>$fieldValue) {



                    if ( !empty(Cars::checkCar($request->bookId)) ) {

                        $rentaCarId = Cars::checkCar($request->bookId)->id;

                        if( $fieldName == 'rentacar_id' && $fieldValue == 'null'){

                            Cars::removeCar($rentaCarId);
                            break;

                        }else{
                            Cars::updateCar($request->bookId, $fieldName, $fieldValue);
                        }


                    } else {

                        $carsInfo = array(

                            'bookId' => $request->bookId,
                            'rentacar_id' => $request->form['cars'][0]['rentacar_id'],
                            'type' => $request->form['cars'][0]['type'],
                            'initDate' => null,
                            'endDate' => null,
                            'notes' => $request->form['cars'][0]['notes'],
                            'price' => $request->form['cars'][0]['price']
                        );

                        $car = new Cars();


                        $carsInfo['rentacar_id'] !== 'null' ?$car->newCar($carsInfo):'';

                        break;

                    }




            }


            return response('Reserva ' . $request->bookNUmber . ' alterada com sucesso.', 200)
                ->header('Content-Type', 'application/json');
        }  elseif(isset($request->package)){

            Packages::removePackage($request->package_id);
            return response('Reserva ' . $request->bookNUmber . ' alterada com sucesso.', 200)
                ->header('Content-Type', 'application/json');

        }
        else{
            $okToValidate = Imports::checkValidImport($request->bookId);



            if(empty($okToValidate)){
                Bookings::validateBooking($request->bookId,$request->state);
                return response('ok ', 200)
                    ->header('Content-Type', 'text/plain');
            }else{

                     return response('Reserva sem serviço RentaCar Atribuído, Transfer a Vefificar ou Hotel Vazio.', 400)
                    ->header('Content-Type', 'text/plain');
            }

        }






    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  book id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bookingID,Request $request)
    {
       Bookings::destroyBooking($bookingID,$request->state);

        return response($request->view, 200)
            ->header('Content-Type', 'text/plain');
    }

    static function convertDate($oldFormat, $date)
    {


        $dateobj = Carbon::createFromFormat($oldFormat, $date);

        return $dateobj->toDateString();


    }

    static function is_date($date)
    {

        return preg_match("^\\d{2}/\\d{2}/\\d{4}^", $date);
    }
}
