<?php

namespace App\Http\Controllers;

use App\Entity\Cars;
use App\Entity\Imports;
use App\Entity\Hotels;
use App\Entity\Packages;
use App\Entity\Rooms;
use App\Entity\Transfers;
use App\Import;
use App\Entity\Bookings;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;


class ImportBookingsController extends Controller


{

    public function __construct(){

        $this->middleware('auth');
    }


    public function index(){

        $importConfigDropdownOptions = Import::importConfigOptions();


        return view('reservas.importarReservas',compact('importConfigDropdownOptions'));
    }


    public function importingFile(Request $request){

        $import = new Import();


        try
        {
            $file = request()->file('fileimport');
        }
        catch (Illuminate\Filesystem\FileNotFoundException $exception)
        {
            die("O Ficheiro não foi carregado");
        }

        $importing = $import->ImportBooks( $file );



        return redirect()->route('minhaImportacao');



    }


    public function myImportIndex(){


        $rentacarOptions = Cars::rentacarOptions();
        $hotelOptions = Hotels::hotelOptions();
        $packageOptions = Packages::packageOptions();
        $tomOptions = Bookings::tomOptions();

        $new = Bookings::bookingStateCount('BU', 'import');
        $canceled = Bookings::bookingStateCount('ST', 'import');
        $changed = Bookings::bookingStateCount('UN', 'import');

        $rentacarExists = Bookings::findRentACarWarn();

        $noHotelExists = Bookings::findNoHotelWarn();

        $compareExists = Bookings::findBookingsToCompare();

        $transferWarn = Transfers::findTransferWarn();
        $findImport = Imports::findImport();

        return view('reservas.minhaImportacao',compact('rentacarOptions','hotelOptions','packageOptions','new','changed','canceled','compareExists','rentacarExists','transferWarn','tomOptions','findImport','noHotelExists' ));

    }


    public  function listMyImport(Request $request){

        echo Bookings::listImportedBookings($request,Bookings::importedBookings($request->state, $request->view));

    }

    public function cancelImport(){

        Imports::cancelImport();

       // return redirect()->route('home');


    }


    public function validateImport(){

        $okToValidate = Imports::checkValidImport(null);

        if(empty($okToValidate)){
            Imports::validateImport();
            return response('ok', 200)
                ->header('Content-Type', 'text/plain');
        }else{

            return response('Existem Reservas sem serviço RentaCar Atribuído, Transfer a Verificar ou Hotel Vazio.', 400)
                ->header('Content-Type', 'text/plain');
        }




    }

    public function resetDatabase(){
        Imports::resetDb();
        return redirect()->route('home');
    }

    public function excelExport(Request $request){

        $state = $request->state;

        $view = $request->view;

        $exportQuery = Bookings::exportBookings($state,$view);


    Excel::create('exportacao_' . Carbon::now() . '_' . $state , function($excel) use ($exportQuery) {


            $excel->setTitle('Exportação de Reservas' );
            $excel->setCreator('Laravel')->setCompany('TravelOne');
            $excel->setDescription('Exportação de Resevas');


            $excel->sheet('página1', function($sheet) use ($exportQuery) {
                $sheet->fromArray($exportQuery, null, 'A1', false, $exportQuery);
            });

        })->download('xlsx');




    }

    public function xmlExport(Request $request){

        $state = $request->state;

        $view = $request->view;



      return  $exportQuery = Bookings::exportBookingsXML($state,$view);


        $response = Response::make($exportQuery, 200);

        $response->header('Content-Type: text/xml','Content-Disposition: attachment; filename="exportacao_' . Carbon::now() . '_' . $state .'.xml"');






    }
}
