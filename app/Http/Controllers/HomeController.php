<?php

namespace App\Http\Controllers;

use App\Entity\Imports;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\PHP;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $findImport = Imports::findImport();
        return view('home')->with(compact('findImport'));
    }
}
