<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guide = DB::table('guides')->pluck('name','id');
        $location=DB::table('locations')->pluck('name','id');
        $hotels = Hotel::orderBy('id','desc')->get();
        return view('reservas.hotel.index', compact('hotels','guide','location'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guide = DB::table('guides')->pluck('name','id');
        $location=DB::table('locations')->pluck('name','id');
        return view('reservas.hotel.create',compact('guide','location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Hotel::create($request -> all());
        return redirect()->route('hotel.index')->with('success-message', "You are successfully add record!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = Hotel::findOrFail($id);
        $guide = DB::table('guides')->pluck('name','id');
        $location=DB::table('locations')->pluck('name','id');
        return view('reservas.hotel.edit', compact('hotel','guide','location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Hotel::findOrFail($id);

        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'email' => 'required',
            'transfer_price_mod' => 'required',
            'active' => 'required'
        ]);

        $input = $request->all();

        $task->fill($input)->save();

//        Session::flash ( 'success-message', "You are successfully updated record!" );

//        return redirect()->back();

        return redirect()->route('hotel.index')->with('success-message', "You are successfully updated record!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_array($id))
        {
            Hotel::destroy($id);
        }
        else
        {
            Hotel::findOrFail($id)->delete();
        }
        return redirect()->route('hotel.index')->with('fail-message', "You are successfully delete record!");
    }
    public function hotelSearch(Request $request){
        $hotels = Hotel::where('name','like','%'.$request->name.'%')->get();
        return view('reservas.hotel.index',compact('hotels'));
    }
}
