<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
    public $fillable=['name','abr','vocation_id','active'];
    public $table='guides';
    public $primaryKey='guide_id';


    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }

}
