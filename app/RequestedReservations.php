<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestedReservations extends Model
{
    private $validState = false;
    private $validationMessages = array();


    /* Import Table Fields */
    public $invoice;
    public $salutati;
    public $nameFirstName;
    public $age;
    public $hotel;
    public $hotelName;
    public $room;
    public $roomName;
    public $cate;
    public $catering;
    public $checkIn;
    public $checkOut;
    public $days;
    public $opt;
    public $optionDate;
    public $noRooms;
    public $members;
    public $flightNo;
    public $departureDate;
    public $departureTime;
    public $arrivalTime;
    public $departureLocation;
    public $arrivalLocation;
    public $connectedLoad;
    public $season;
    public $changed;
    public $booked;
    public $field27;
    public $field28;
    public $field29;
    public $field30;
    public $field31;
    public $field32;


    private function validate()
    {
        $validState = true;
        if($this->invoice == "") {
            $validState = false;
            array_push($validationMessages, "Invoice is empty");
        }
        if($this->salutati == "") {
            $validState = false;
            array_push($validationMessages, "Invoice is empty");
        }
        if($this->nameFirstName == "") {
            $validState = false;
            array_push($validationMessages, "Invoice is empty");
        }

        return array("isValid" => $validState, "validationMessages" => $validationMessages);
    }
}
