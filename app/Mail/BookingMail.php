<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
class BookingMail extends Mailable
{
    use Queueable, SerializesModels;

    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $booking)
    {
        $this->booking = $booking;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $season = self::getSeason();
        $subject = 'SLR- Cat. ' . $season;

        $this->subject($subject);
        return $this->view('emails.bookingMail');
    }



    static function getSeason()
    {

        $curDate = strtotime(date('d-m-Y'));
        $curYear = date('Y');
        $summerStart = strtotime(date('01-05-' . $curYear));
        $summerEnd = strtotime(date('31-10-' . $curYear));

        if ($curDate >= $summerStart && $curDate <= $summerEnd) {
            return "VERÃO";
        } else {
            return "INVERNO";
        }

    }
}
