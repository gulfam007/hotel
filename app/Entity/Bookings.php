<?php

namespace App\Entity;

use App\Import;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Entity\Imports;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\BookingMail;
use Illuminate\Support\Facades\Log;
use SoapBox\Formatter\Formatter;

class Bookings extends Model
{

    public function newBoking($bookinfo)
    {

        $bookingsTable = DB::table('bookings');

        $import = new Imports();

        return $bookingsTable->insertGetId(
            [
                'import_id' => $import::findImport()->importId,
                //'code' => $bookinfo['code'],
                'operator_id' => $import::findImport()->operatorId,
                'book_number' => $bookinfo['bookingNumber'],
                'locked' => 1,
                'state' => $bookinfo['state'],
                'book_date' => $bookinfo['bookDate'],
                'active' => 1,
                'last_modified' => Carbon::now(),
                'tms_id' => $bookinfo['tmsId']
//                '2htl' => $bookinfo['2HTL']

            ]
        );

    }

    static function addHistory($bookinfo)
    {

        $bookingsHistoryTable = DB::table('bookings_history');

        $import = new Imports();

        $bookingsHistoryTable->insert(
            [

                'book_id' => $bookinfo['book_id'],
                'import_id' => $import::findImport()->importId,
                'invoice' => $bookinfo['invoice'],
                'day' => $bookinfo['day'],
                'tms_id' => $bookinfo['tms_id'],
                'salutati' => $bookinfo['salutati'],
                'name' => $bookinfo['name'],
                'opt' => $bookinfo['opt'],
                'age' => $bookinfo['age'],
                'hotel' => $bookinfo['hotel'],
                'hotel_name' => $bookinfo['hotel_name'],
                'room' => $bookinfo['room'],
                'room_name' => $bookinfo['room_name'],
                'cate' => $bookinfo['cate'],
                'catering' => $bookinfo['catering'],
                'check_in' => $bookinfo['check_in'],
                'check_out' => $bookinfo['check_out'],
                'days' => $bookinfo['days'],
                'option_date' => $bookinfo['option_date'],
                'number_of_rooms' => $bookinfo['number_of_rooms'],
                'members' => $bookinfo['members'],
                'flight_number' => $bookinfo['flight_number'],
                'departure_date' => $bookinfo['departure_date'],
                'departure_time' => $bookinfo['departure_time'],
                'arrival_time' => $bookinfo['arrival_time'],
                'departure_location' => $bookinfo['departure_location'],
                'arrival_location' => $bookinfo['arrival_location'],
                'connected_load' => $bookinfo['connected_load'],
                'season' => $bookinfo['season'],
                'changed' => $bookinfo['changed'],
                'booked' => $bookinfo['booked'],
                'notes' => $bookinfo['notes'],
                'state' => $bookinfo['state'],
                'price' => $bookinfo['price'],
                'nr_a' => $bookinfo['nr_a'],
                'nr_c' => $bookinfo['nr_c'],

            ]
        );


//        $bookingInfo = array(
//
//            'book_id' => $book_id,
//            'invoice' => $stainvoicete,
//            'salutati' => $salutati,
//            'name' => $name,
//            'first_name' => $first_name,
//            'age' => $age,
//            'hotel' => $hotel,
//            'hotel_name' => $hotel_name,
//            'room' => $room,
//            'room_name' => $room_name,
//            'cate' => $cate,
//            'catering' => $catering,
//            'check_in' => $check_in,
//            'check_out' => $check_out,
//            'days' => $days,
//            'option_date' => $option_date,
//            'number_of_rooms' => $number_of_rooms,
//            'members' => $members,
//            'flight_number' => $flight_number,
//            'daparture_date' => $daparture_date,
//            'departure_time' => $departure_time,
//            'arrival_time' => $arrival_time,
//            'departure_location' => $departure_location,
//            'arrival_location' => $arrival_location,
//            'connected_load' => $connected_load,
//            'season' => $season,
//            'changed' => $changed,
//            'booked' => $booked
//        );

    }

    static function tomOptions()
    {
        $tms = DB::table('tmsrecords')
            ->select(
                DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS text"),
                'id')
            ->get();

        return $tms;
    }

    static function fetchHistory($bookId)
    {


        $bookingsQuery = DB::table('bookings_history')
            ->where('book_id', '=', $bookId)
            ->get();

        $data = array();

        array_push($data, $bookingsQuery);


        $json_data = array(

            "data" => $bookingsQuery
        );

        return json_encode($bookingsQuery);


    }

    static function findBookingbyBookingNumber($bookingNumber)
    {

        $bookingsTable = DB::table('bookings');
        return $bookingsTable
            ->where('book_number', '=', $bookingNumber)
            ->select('id', 'book_number')
            ->first();

    }

    static function findBookingbyBookingNumberAndStatus($bookingNumber, $state)
    {

        $bookingsTable = DB::table('bookings');
        return $bookingsTable
            ->where('book_number', '=', $bookingNumber)
            ->where('state', '=', $state)
            ->select('id', 'book_number')
            ->first();

    }

    static function findBookingbyBookingbyId($bookingId)
    {

        $bookingsTable = DB::table('bookings');
        return $bookingsTable->where('id', '=', $bookingId)
            ->select('id', 'book_number')
            ->first();

    }

    static function findBookingbyBookingHistorybyId($bookingId)
    {

        $bookingsTable = DB::table('bookings_history');
        return $bookingsTable->where('id', '=', $bookingId)
            ->select('id', 'invoice')
            ->first();

    }

    static function findValidBookings($bookingNumber)

    {

        $bookingsTable = DB::table('bookings');
        return $bookingsTable
            ->where('book_number', '=', $bookingNumber)
            ->where('locked', '=', '0')
            ->select('id', 'book_number')
            ->first();

    }

    static function findBookingsToCompare()
    {

        $import = new Imports();

        $first = DB::table('bookings')
            ->select('book_number')
            ->where('locked', '=', 0);

        return $unionQ = DB::table('bookings_history')
            ->select('invoice')
            ->where('import_id', '=', $import::findImport()->importId)
            ->where('locked', '=', 0)
            ->union($first)
            ->get();

    }

    static function findRentACarWarn()
    {


        $import = new Imports();

        $response = false;

        $rentacarTable = DB::table('cars')
            ->select('rentacar_id')
            ->leftJoin('bookings', 'bookings.id', '=', 'cars.book_id')
            ->where('bookings.import_id', '=', $import::findImport()->importId)
            ->get();

        foreach ($rentacarTable as $r) {

            if (!$r->rentacar_id) {

                $response = true;
                break;
            }

        }

        return $response;

    }

    static function findNoHotelWarn()
    {


        $import = new Imports();

        $response = false;

        $bookingsTable = DB::table('bookings')
            ->select('bookings.book_number')
            ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
            ->leftJoin('hotels', 'hotels.id', '=', 'rooms.hotel_id')
            ->where('bookings.import_id', '=', $import::findImport()->importId)
            ->where('hotels.id', '=', 190)
            ->get();


        if ($bookingsTable->count() > 0) {

            $response = true;

        }


        return $response;

    }

    static function findRentaCarBooking($bookingNumber, $state)
    {
        $bookID = self::findBookingbyBookingNumberAndStatus($bookingNumber, $state);
        $rentacarTable = DB::table('cars');
        return $rentacarTable
            ->where('book_id', '=', $bookID ? $bookID->id : '')
            ->where('rentacar_id', '=', null)
            ->select('id', 'rentacar_id')
            ->first();

    }

    static function findNoHotelBooking($bookingNumber, $state)
    {
        $bookID = self::findBookingbyBookingNumberAndStatus($bookingNumber, $state);

        return $bookingsTable = DB::table('bookings')
            ->select('bookings.book_number')
            ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
            ->leftJoin('hotels', 'hotels.id', '=', 'rooms.hotel_id')
            ->where('bookings.id', '=', $bookID ? $bookID->id : '')
            ->where('hotels.id', '=', 190)
            ->first();

    }

    static function findTransferBooking($bookingNumber, $state)
    {
        $bookID = self::findBookingbyBookingNumberAndStatus($bookingNumber, $state);
        return $bookingsTable = DB::table('bookings')
            ->select('bookings.book_number')
            ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
            ->where(function ($query) use ($bookID) {
                $query
                    ->where('bookings.id', '=', $bookID ? $bookID->id : '');
            })->where(function ($query) use ($bookID) {
                $query->orWhere('transfers.type', '=', 'Verificar')
                    ->orWhere('transfers.type', '=', null);
            })
            ->first();

    }


    static function bookingStateCount($state, $view)
    {

        if ($view == 'import') {

            if ($state !== 'BU') {

                $bookingsTable = DB::table('bookings')
                    ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                    ->where([
                        ['imports.user_id', '=', Auth::id()],
                        ['bookings.state', '=', $state],
                    ])
                    ->orWhere('bookings.state', '=', $state == 'UN' ? 'US' : '')
                    ->count();

            } else {

                $bookingsTable = DB::table('bookings')
                    ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                    ->where([
                        ['imports.user_id', '=', Auth::id()],
                        ['bookings.state', '=', $state],
                    ])
                    ->count();

            }

        } else {

            if ($state == 'UN') {


                $bookingsTable = DB::table('bookings')
                    ->where([
                        ['bookings.locked', '=', '0'],
                        ['bookings.state', '=', 'UN'],
                    ])
                    ->orWhere([
                        ['bookings.locked', '=', '0'],
                        ['bookings.state', '=', 'US']
                    ])
                    ->count();

            } else {

                $bookingsTable = DB::table('bookings')
                    ->where([
                        ['bookings.locked', '=', '0'],
                        ['bookings.state', '=', $state],
                    ])
                    ->count();
            }


        }

        return $bookingsTable;


    }

    static function importedBookings($state, $view)
    {

        // Log::info( ' State ' . $state .   PHP_EOL);
        if ($state !== 'BU') {

            $bookingsTable = DB::table('bookings')
                ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                ->where('imports.user_id', '=', Auth::id())
                ->where('bookings.locked', '=', '1')
                ->where(function ($q) use ($state) {
                    $q->where('bookings.state', $state)
                        ->orWhere('bookings.state', $state == 'UN' ? 'US' : '');
                })
                ->select(
                    DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                    DB::raw("bookings.book_number AS BOOKING"),
                    DB::raw("bookings.id AS ID"),
                    DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                    DB::raw("rooms.day AS DAY"),
                    DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                    DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),
                    DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                    DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                    DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                    DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),
                    DB::raw("bookings.essen as ESSEN"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(hotels.name) SEPARATOR ',') AS HOTEL"),
                    DB::raw("guides.name as GUIDE"),
                    DB::raw("rooms.room_type as ROOMTYPE"),
                    DB::raw("rooms.room_code as ROOMCODE"),
                    DB::raw("rooms.nights as ROOMNIGHTS"),
                    DB::raw("bookings.state as STATE"),
                    DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                    DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN"),
                    DB::raw("(SELECT  count(*) FROM clients WHERE book_id = bookings.id ) as MEMBERS"),
                    DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMS")
                )
                ->groupBy('bookings.id');

        } else {

            $bookingsTable = DB::table('bookings')
                ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                ->where([
                    ['imports.user_id', '=', Auth::id()],
                    ['bookings.locked', '=', '1'],
                    ['bookings.state', '=', $state],
                ])
                ->select(
                    DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                    DB::raw("bookings.book_number AS BOOKING"),
                    DB::raw("bookings.id AS ID"),
                    DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                    DB::raw("rooms.day AS DAY"),
                    DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                    DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),
                    DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                    DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                    DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                    DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),
                    DB::raw("bookings.essen as ESSEN"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(hotels.name) SEPARATOR ',') AS HOTEL"),
                    DB::raw("guides.name as GUIDE"),
                    DB::raw("rooms.room_type as ROOMTYPE"),
                    DB::raw("rooms.room_code as ROOMCODE"),
                    DB::raw("rooms.nights as ROOMNIGHTS"),
                    DB::raw("bookings.state as STATE"),
                    DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                    DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN"),
                    DB::raw("(SELECT  count(*) FROM clients WHERE book_id = bookings.id ) as MEMBERS"),
                    DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMS")
                )
                ->groupBy('bookings.book_number');
        }


        return $bookingsTable;

    }

    static function fetchBookings($state)
    {


        if ($state !== 'BU') {

            $bookingsTable = DB::table('bookings')
                ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                ->where('bookings.locked', '=', '0')
                ->where(function ($q) use ($state) {
                    $q->where('bookings.state', $state)
                        ->orWhere('bookings.state', $state == 'UN' ? 'US' : '');
                })
                ->select(
                    DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                    DB::raw("bookings.book_number AS BOOKING"),
                    DB::raw("bookings.id AS ID"),
                    DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                    DB::raw("rooms.day AS DAY"),
                    DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                    DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),
                    DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                    DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                    DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                    DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),
                    DB::raw("bookings.essen as ESSEN"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(hotels.name) SEPARATOR ',') AS HOTEL"),
                    DB::raw("guides.name as GUIDE"),
                    DB::raw("rooms.room_type as ROOMTYPE"),
                    DB::raw("rooms.room_code as ROOMCODE"),
                    DB::raw("rooms.nights as ROOMNIGHTS"),
                    DB::raw("bookings.state as STATE"),
                    DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                    DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN"),
                    DB::raw("(SELECT  count(*) FROM clients WHERE book_id = bookings.id ) as MEMBERS"),
                    DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMS")
                )
                ->groupBy('bookings.id');

        } else {

            $bookingsTable = DB::table('bookings')
                ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                ->where([
                    ['bookings.locked', '=', '0'],
                    ['bookings.state', '=', $state],
                ])
                ->select(
                    DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                    DB::raw("bookings.book_number AS BOOKING"),
                    DB::raw("bookings.id AS ID"),
                    DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                    DB::raw("rooms.day AS DAY"),
                    DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                    DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),
                    DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                    DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                    DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                    DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),
                    DB::raw("bookings.essen as ESSEN"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(hotels.name) SEPARATOR ',') AS HOTEL"),
                    DB::raw("guides.name as GUIDE"),
                    DB::raw("rooms.room_type as ROOMTYPE"),
                    DB::raw("rooms.room_code as ROOMCODE"),
                    DB::raw("rooms.nights as ROOMNIGHTS"),
                    DB::raw("bookings.state as STATE"),
                    DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                    DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN"),
                    DB::raw("(SELECT  count(*) FROM clients WHERE book_id = bookings.id ) as MEMBERS"),
                    DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMS")
                )
                ->groupBy('bookings.book_number');
        }


        return $bookingsTable;

    }

    static function listImportedBookings($request, $bookingsQuery)
    {
        $columns = array(
            0 => 'tmsrecords.NR_TOM',
            1 => 'bookings.book_number',
            2 => 'bookings.state',
            3 => 'rooms.check_in',
            4 => 'DAY',
            5 => 'flights.arrival_date',
            6 => 'flights.arrival_flight',
            7 => 'flights.arrival_time',
            8 => 'flights.departure_location',
            9 => 'flights.departure_flight',
            10 => 'flights.departure_date',
            11 => 'flights.departure_time',
            12 => 'flights.arrival_location',
            13 => 'clients.name',
            14 => 'rooms.essen',
            15 => 'hotels.name',
            16 => 'GUIDE',
            17 => 'rooms.room_type',
            18 => 'rooms.room_code',
            19 => 'rooms.nights',
            20 => 'rooms.check_out',
            21 => 'members',
            22 => 'rooms',


        );


        $totalData = $bookingsQuery->get()->count();

        $totalFiltered = $totalData;


        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit !== '-1') {

                $query = $bookingsQuery
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $query = $bookingsQuery
                    ->orderBy($order, $dir)
                    ->get();
            }
        } else {
            $search = $request->input('search.value');

            if ($limit !== '-1') {


                $query = $bookingsQuery
                    ->where(function ($q) use ($search) {

                        return $q
                            ->where('tmsrecords.NR_TOM', 'LIKE', "%{$search}%")
                            ->orWhere('tmsrecords.SERIE', 'LIKE', "%{$search}%")
                            ->orWhere('bookings.book_number', 'LIKE', "%{$search}%")
                            ->orWhere('bookings.book_date', 'LIKE', "%{$search}%")
                            ->orWhere('rooms.day', 'LIKE', "%{$search}%")
                            ->orWhere('flights.arrival_flight', 'LIKE', "%{$search}%")
                            ->orWhere('flights.arrival_date', 'LIKE', "%{$search}%")
                            ->orWhere('flights.arrival_time', 'LIKE', "%{$search}%")
                            ->orWhere('flights.arrival_location', 'LIKE', "%{$search}%")
                            ->orWhere('flights.departure_flight', 'LIKE', "%{$search}%")
                            ->orWhere('flights.departure_date', 'LIKE', "%{$search}%")
                            ->orWhere('flights.departure_time', 'LIKE', "%{$search}%")
                            ->orWhere('flights.departure_location', 'LIKE', "%{$search}%")
                            ->orWhere('clients.name', 'LIKE', "%{$search}%")
                            ->orWhere('bookings.essen', 'LIKE', "%{$search}%")
                            ->orWhere('hotels.name', 'LIKE', "%{$search}%")
                            ->orWhere('hotels.code', 'LIKE', "%{$search}%")
                            ->orWhere('guides.name', 'LIKE', "%{$search}%")
                            ->orWhere('rooms.room_type', 'LIKE', "%{$search}%")
                            ->orWhere('rooms.room_code', 'LIKE', "%{$search}%")
                            ->orWhere('rooms.nights', 'LIKE', "%{$search}%")
                            ->orWhere('rooms.check_out', 'LIKE', "%{$search}%")
                            ->orWhere('rooms.check_in', 'LIKE', "%{$search}%");
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();

            } else {


                $query = $bookingsQuery
                    ->orWhere('tmsrecords.NR_TOM', 'LIKE', "%{$search}%")
                    ->orWhere('tmsrecords.SERIE', 'LIKE', "%{$search}%")
                    ->orWhere('bookings.book_number', 'LIKE', "%{$search}%")
                    ->orWhere('bookings.book_date', 'LIKE', "%{$search}%")
                    ->orWhere('rooms.day', 'LIKE', "%{$search}%")
                    ->orWhere('flights.arrival_flight', 'LIKE', "%{$search}%")
                    ->orWhere('flights.arrival_date', 'LIKE', "%{$search}%")
                    ->orWhere('flights.arrival_time', 'LIKE', "%{$search}%")
                    ->orWhere('flights.arrival_location', 'LIKE', "%{$search}%")
                    ->orWhere('flights.departure_flight', 'LIKE', "%{$search}%")
                    ->orWhere('flights.departure_date', 'LIKE', "%{$search}%")
                    ->orWhere('flights.departure_time', 'LIKE', "%{$search}%")
                    ->orWhere('flights.departure_location', 'LIKE', "%{$search}%")
                    ->orWhere('clients.name', 'LIKE', "%{$search}%")
                    ->orWhere('bookings.essen', 'LIKE', "%{$search}%")
                    ->orWhere('hotels.name', 'LIKE', "%{$search}%")
                    ->orWhere('hotels.code', 'LIKE', "%{$search}%")
                    ->orWhere('guides.name', 'LIKE', "%{$search}%")
                    ->orWhere('rooms.room_type', 'LIKE', "%{$search}%")
                    ->orWhere('rooms.room_code', 'LIKE', "%{$search}%")
                    ->orWhere('rooms.nights', 'LIKE', "%{$search}%")
                    ->orWhere('rooms.check_out', 'LIKE', "%{$search}%")
                    ->orWhere('rooms.check_in', 'LIKE', "%{$search}%")
                    ->orderBy($order, $dir)
                    ->get();


            }


            $sub = DB::table('bookings')
                ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                ->where([
                    ['imports.user_id', '=', Auth::id()],
                    ['bookings.locked', '=', '1'],
                    ['bookings.state', '=', $request->state],
                ])
                ->select(
                    DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                    DB::raw("bookings.book_number AS BOOKING"),
                    DB::raw("bookings.id AS ID"),
                    DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                    DB::raw("rooms.day AS DAY"),
                    DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                    DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                    DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                    DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y')  AS DEPARTUREDATE"),
                    DB::raw("flights.departure_time AS DEPARTURETIME"),
                    DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                    DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),
                    DB::raw("bookings.essen as ESSEN"),
                    DB::raw("hotels.name as HOTEL"),
                    DB::raw("guides.name as GUIDE"),
                    DB::raw("rooms.room_type as ROOMTYPE"),
                    DB::raw("rooms.room_code as ROOMCODE"),
                    DB::raw("rooms.nights as ROOMNIGHTS"),
                    DB::raw("bookings.state as STATE"),
                    DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                    DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN")
                )
                ->groupBy('bookings.book_number')
                ->where(function ($q) use ($search) {

                    return $q
                        ->orWhere('tmsrecords.NR_TOM', 'LIKE', "%{$search}%")
                        ->orWhere('tmsrecords.SERIE', 'LIKE', "%{$search}%")
                        ->orWhere('bookings.book_number', 'LIKE', "%{$search}%")
                        ->orWhere('bookings.book_date', 'LIKE', "%{$search}%")
                        ->orWhere('rooms.day', 'LIKE', "%{$search}%")
                        ->orWhere('flights.arrival_flight', 'LIKE', "%{$search}%")
                        ->orWhere('flights.arrival_date', 'LIKE', "%{$search}%")
                        ->orWhere('flights.arrival_time', 'LIKE', "%{$search}%")
                        ->orWhere('flights.arrival_location', 'LIKE', "%{$search}%")
                        ->orWhere('flights.departure_flight', 'LIKE', "%{$search}%")
                        ->orWhere('flights.departure_date', 'LIKE', "%{$search}%")
                        ->orWhere('flights.departure_time', 'LIKE', "%{$search}%")
                        ->orWhere('flights.departure_location', 'LIKE', "%{$search}%")
                        ->orWhere('clients.name', 'LIKE', "%{$search}%")
                        ->orWhere('bookings.essen', 'LIKE', "%{$search}%")
                        ->orWhere('hotels.name', 'LIKE', "%{$search}%")
                        ->orWhere('hotels.code', 'LIKE', "%{$search}%")
                        ->orWhere('guides.name', 'LIKE', "%{$search}%")
                        ->orWhere('rooms.room_type', 'LIKE', "%{$search}%")
                        ->orWhere('rooms.room_code', 'LIKE', "%{$search}%")
                        ->orWhere('rooms.nights', 'LIKE', "%{$search}%")
                        ->orWhere('rooms.check_out', 'LIKE', "%{$search}%")
                        ->orWhere('rooms.check_in', 'LIKE', "%{$search}%");
                });

            $totalFiltered = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                ->mergeBindings($sub)
                ->count();


        }
        $scolumn = $columns;
        foreach ($scolumn as $key => $value) {
            $columnSeachValue = $request->input('columns.' . $key . '.search.value');

//            if (preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $columnSeachValue) !== 0){
//                $columnSeachValue = trim(date("Y-d-m", strtotime($columnSeachValue) ));
//            }
            if (!empty($columnSeachValue)) {


                $query = $bookingsQuery
                    ->where(function ($q) use ($columnSeachValue, $value) {
                        return $q
                            ->where("{$value}", 'LIKE', "%{$columnSeachValue}%");

                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();

                $totalFiltered = $query->count();

                //Log::info( ' VAlor ' . $columnSeachValue .   PHP_EOL);
            };
        }


        $data = array();
        if (!empty($bookingsQuery)) {
            foreach ($query as $post) {
                // $show =  route('posts.show',$post->ID);
                // $edit =  route('posts.edit',$post->ID);

                $nestedData['Tom'] = $post->TOM;
                $nestedData['Booking'] = $post->BOOKING;
                $nestedData['State'] = $post->STATE;
//                $nestedData['OPERATORID'] = isset($post->OPERATORID)?$post->OPERATORID:"";
//                $nestedData['OPERATOR'] = isset($post->OPERATOR)?$post->OPERATOR:"";
                $nestedData['Date'] = $post->BOOKINGDATE ? $post->BOOKINGDATE : '';
                $nestedData['Day'] = $post->DAY;
                $nestedData['ArrivalFligh'] = $post->ARRIVALFLIGHT;
                $nestedData['ArrivalDate'] = $post->ARRIVALDATE ? $post->ARRIVALDATE : '';
                $nestedData['ArrivalTime'] = $post->ARRIVALTIME;
                $nestedData['ArrivalLocation'] = $post->ARRIVALLOCATION;
                $nestedData['DepartureFlight'] = $post->DEPARTUREFLIGHT;
                $nestedData['DepartureDate'] = $post->DEPARTUREDATE ? $post->DEPARTUREDATE : '';
                $nestedData['DepartureTime'] = $post->DEPARTURETIME;
                $nestedData['DepartureLocation'] = $post->DEPARTURELOCATION;
                $nestedData['Names'] = isset($post->NAMES) ? $post->NAMES : '';
                $nestedData['Essen'] = isset($post->ESSEN) ? $post->ESSEN : '';
                $nestedData['Hotel'] = $post->HOTEL;
                $nestedData['Guide'] = isset($post->GUIDE) ? $post->GUIDE : '';
                $nestedData['RoomType'] = $post->ROOMTYPE;
                $nestedData['RoomCode'] = $post->ROOMCODE;
                $nestedData['Nights'] = $post->ROOMNIGHTS;
                //  $nestedData['checkOut'] = date('j/m/Y',strtotime($post->CHECKOUT));
                $nestedData['checkOut'] = $post->CHECKOUT ? $post->CHECKOUT : '';
                $nestedData['checkIn'] = $post->CHECKIN ? $post->CHECKIN : '';
                $nestedData['ID'] = $post->ID;
                $nestedData['EXISTS'] = self::findValidBookings($post->BOOKING) ? self::findBookingbyBookingNumber($post->BOOKING)->book_number : '';
                $nestedData['RENTACAREXISTS'] = self::findRentaCarBooking($post->BOOKING, $post->STATE) ? self::findRentaCarBooking($post->BOOKING, $post->STATE)->rentacar_id : '';
                $nestedData['NOHOTELEXISTS'] = self::findNoHotelBooking($post->BOOKING, $post->STATE) ? self::findNoHotelBooking($post->BOOKING, $post->STATE)->book_number : '';
                $nestedData['TRANSFEREXISTS'] = self::findTransferBooking($post->BOOKING, $post->STATE) ? self::findTransferBooking($post->BOOKING, $post->STATE)->book_number : '';
                $nestedData['MEMBERS'] = $post->MEMBERS;
                $nestedData['ROOMS'] = $post->ROOMS;


                //$nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                //                           &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);

    }

    public function updateImportedBookings($bookinfo)
    {

        $bookingsTable = DB::table('bookings');

        $bookingsTable
            ->where('id', $bookinfo['id'])
            ->update(
                [

                    'state' => $bookinfo['state'],
                    'book_date' => $bookinfo['bookDate'],
                    'last_modified' => Carbon::now(),
                    'tms_id' => $bookinfo['tmsId'],
//                    '2htl' => $bookinfo['2HTL']
                ]
            );

    }

    static function fetchBooking($bookingID, $historyId)
    {


        $booking = DB::table('bookings')
            ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
            ->leftJoin('flights', 'flights.book_id', '=', 'bookings.id')
            ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
            ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->leftJoin('cars', 'cars.book_id', '=', 'bookings.id')
            ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
            ->where('bookings.id', '=', $bookingID)
            ->select(
                'bookings.tms_id',
                DB::raw("bookings.book_number AS booking_number"),
                DB::raw("bookings.id AS booking_id"),
                DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS booking_date"),
                DB::raw("flights.arrival_flight AS arrival_flight"),
                DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS arrival_date"),
                DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS arrival_time"),
                DB::raw("flights.arrival_location AS arrival_location"),
                DB::raw("flights.departure_flight AS departure_flight"),
                DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS departure_date"),
                DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS departure_time"),
                DB::raw("flights.departure_location AS destiny"),
                DB::raw("flights.id AS flightid"),
                DB::raw("bookings.state as state"),
                DB::raw("cars.id as car_id"),
                DB::raw("cars.category as car_type"),
                DB::raw("cars.rentacar_id as rentacar_id"),
                DB::raw("cars.notes as car_notes"),
                DB::raw("transfers.type as transfer")
            )
            ->groupBy('bookings.book_number')
            ->get();

        $clients = DB::table('clients')
            ->where('clients.book_id', '=', $bookingID)
            ->leftJoin('client_room', 'client_room.client_id', '=', 'clients.id')
            ->select(
                DB::raw("clients.name as name"),
                DB::raw("clients.age as age"),
                DB::raw("clients.gender as gender"),
                DB::raw("COALESCE(clients.room_id,client_room.room_id) as room"),
                DB::raw("clients.id as id")
            )
            ->get();

        $rooms = DB::table('rooms')
            ->leftJoin('client_room', 'client_room.room_id', '=', 'rooms.id')
            ->leftJoin('clients', 'clients.room_id', '=', 'rooms.id')
            ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->select(
                DB::raw("rooms.id as id"),
                DB::raw("rooms.room_type as room_type"),
                DB::raw("rooms.essen as essen"),
                DB::raw("rooms.catering as catering"),
                DB::raw("hotels.name as hotel"),
                DB::raw("hotels.id as hotel_id"),
                DB::raw("DATE_FORMAT(rooms.check_in,'%d/%m/%Y') as check_in"),
                DB::raw("rooms.day AS day"),
                DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as check_out"),
                DB::raw("rooms.nights as nights"),
                DB::raw("rooms.room_code as room_code"),
                DB::raw("rooms.price as price"),
                DB::raw("rooms.notes as room_notes"),
                //  DB::raw("(SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') from clients where id =  client_room.client_id) as members")
                DB::raw("COALESCE(concat(clients.name,'/',(SELECT clients.name from clients where id = client_room.client_id)) ,(SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ')
                        FROM clients
                        WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ')
                        FROM client_room
                        LEFT JOIN clients on clients.id = client_room.client_id
                        WHERE client_room.room_id = rooms.id)) as members")
            )
            ->where('rooms.book_id', '=', $bookingID)
            ->groupBy('rooms.id')
            ->orderBy('rooms.check_in', 'asc')
            ->get();


        $packages = DB::table('bookings')
            ->leftJoin('packages', 'packages.book_id', '=', 'bookings.id')
            ->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
            ->leftJoin('clients as cl', 'packages.client_id', '=', 'cl.id')
            ->where('bookings.id', '=', $bookingID)
            ->select(
                DB::raw("packages.id as id"),
                DB::raw("packages_type.name as name"),
                DB::raw("cl.name as client"),
                DB::raw("packages.days as days"),
                DB::raw("packages.tuachen_nr_pax as pack_pax"),
                DB::raw("packages.tuachen_qty as pack_qty"),
                DB::raw("packages.state as state"),
                DB::raw("packages.price as price"),
                DB::raw("DATE_FORMAT(packages.check_in,'%d/%m/%Y') as checkIn"),
                DB::raw("DATE_FORMAT(packages.check_out,'%d/%m/%Y') as checkOut")
            )
            ->get();

        $car = DB::table('cars')
            ->where('book_id', '=', $bookingID)
            ->select(
                DB::raw("rentacar_id"),
                DB::raw("type"),
                DB::raw("notes"),
                DB::raw("price")

            )
            ->get();


        $cbooking = DB::table('bookings_history')
            ->select(
                DB::raw("bookings_history.*"),
                DB::raw("DATE_FORMAT(bookings_history.check_in,'%d/%m/%Y') AS check_in"),
                DB::raw("DATE_FORMAT(bookings_history.check_out,'%d/%m/%Y') AS check_out"),
                DB::raw("DATE_FORMAT(bookings_history.departure_date,'%d/%m/%Y') AS departure_date"),
                DB::raw("TIME_FORMAT(bookings_history.departure_time, '%H:%i') AS departure_time"),
                DB::raw("DATE_FORMAT(bookings_history.season,'%d/%m/%Y') AS season"),
                DB::raw("TIME_FORMAT(bookings_history.arrival_time, '%H:%i') AS arrival_time"),
                DB::raw("DATE_FORMAT(bookings_history.booked,'%d/%m/%Y') AS booked")

            )
            ->where('id', '=', $historyId)
            ->get();


        $json_data = array(
            "booking" => $booking,
            "clients" => $clients,
            "rooms" => $rooms,
            "packages" => $packages,
            "car" => $car,
            "cbooking" => $cbooking

        );


        return json_encode($json_data);


    }

    static function destroyBooking($bookingID, $state)
    {

        if ($state !== 'Novas') {

            DB::table('packages')
                ->where('book_id', '=', $bookingID)
                ->delete();
            DB::table('clients')
                ->where('book_id', '=', $bookingID)
                ->delete();
            DB::table('flights')
                ->where('book_id', '=', $bookingID)
                ->delete();
            DB::table('rooms')
                ->where('book_id', '=', $bookingID)
                ->delete();
            DB::table('transfers')
                ->where('book_id', '=', $bookingID)
                ->delete();
            DB::table('cars')
                ->where('book_id', '=', $bookingID)
                ->delete();
            DB::table('bookings')
                ->where('id', '=', $bookingID)
                ->delete();

        } else {

            DB::table('bookings_history')
                ->where('id', '=', $bookingID)
                ->delete();
        }


    }

    static function sendBookingEmail($bookingID)
    {

        $packages = DB::table('packages')
            ->where('book_id', '=', $bookingID)
            ->select('hotel_id as packageHotelId')
            ->first();

        $booking = DB::table('bookings')
            ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
            ->leftJoin('flights', 'flights.book_id', '=', 'bookings.id')
            ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
            ->select(
                DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS tom"),
                DB::raw("bookings.book_number AS booking_number"),
                DB::raw("bookings.id AS booking_id"),
                DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS booking_date"),
                DB::raw("flights.arrival_flight AS arrival_flight"),
                DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS arrival_date"),
                DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS arrival_time"),
                DB::raw("flights.arrival_location AS arrival_location"),
                DB::raw("flights.departure_flight AS departure_flight"),
                DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS departure_date"),
                DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS departure_time"),
                DB::raw("flights.departure_location AS destiny"),
                DB::raw("flights.id AS flightid"),
                DB::raw("bookings.state as state"),
                DB::raw("transfers.type as transfer")
            )
            ->where('bookings.id', '=', $bookingID)
            ->groupBy('bookings.book_number')
            ->get();


        $roomsData = DB::table('rooms')
        
          		->leftJoin('hotels', 'hotels.id','=','rooms.hotel_id')
		->leftJoin('clients','clients.room_id', '=', 'rooms.id')
		->leftJoin('packages', 'packages.client_id', '=', 'clients.id')
		->leftJoin('packages_type', 'packages_type.id', '=', 'packages.type_id')
        /*    ->leftJoin('client_room', 'rooms.id', '=', 'client_room.room_id')
            ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->leftjoin('clients', 'clients.book_id', '=', 'client_room.book_id')
            //->leftJoin('packages', 'packages.hotel_id', '=', DB::raw($packages?$packages->packageHotelId:'rooms.hotel_id'))
            ->leftJoin('packages', 'packages.hotel_id', '=', DB::raw($packages?$packages->packageHotelId:'NULL'))
			->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')*/
            ->select(
                DB::raw("rooms.id as id"),
                DB::raw("rooms.room_type as room_type"),
                DB::raw("rooms.essen as essen"),
                DB::raw("hotels.name as hotel"),
                //DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                //DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
        
                DB::raw("count_adults(rooms.id) as adults"),
                        DB::raw("count_kids(rooms.id) as kids"),
                DB::raw("DATE_FORMAT(rooms.check_in,'%d/%m/%Y') AS check_in"),
                DB::raw("rooms.day AS day"),
                DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') AS check_out"),
                DB::raw("rooms.nights as nights"),
                DB::raw("rooms.room_code as room_code"),
                DB::raw("rooms.price as price"),
                DB::raw("rooms.notes as room_notes"),
                DB::raw("GROUP_CONCAT(packages_type.name SEPARATOR ', ') as packages"),
                DB::raw("GROUP_CONCAT(CONCAT(clients.age) SEPARATOR ' , ') AS ages"),
                DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.room_id = rooms.id)) AS members")
            )
            ->where('rooms.book_id', '=', $bookingID)
            ->groupBy('rooms.id')
            ->get();


        if (!$roomsData->isEmpty()) {


            if ($roomsData->count() > 1) {


                foreach ($roomsData->groupBy('hotel') as $hotel) {

                    $data = array(
                        "booking" => $booking,
                        "room" => $hotel,


                    );


                    Mail::to('janete.dominguez@travelone.pt')
                        ->cc('michaeljdsilva@gmail.com')
                        ->send(new BookingMail($data));

                }


            } else {

                $data = array(
                    "booking" => $booking,
                    "room" => $roomsData,


                );


                Mail::to('janete.dominguez@travelone.pt')
                    ->cc('michaeljdsilva@gmail.com')
                    ->send(new BookingMail($data));

            }


        } else {// if no room, only package


            $packageData = DB::table('packages')
                ->leftJoin('hotels', 'packages.hotel_id', '=', 'hotels.id')
                ->leftjoin('clients', 'clients.id', '=', 'packages.client_id')
                ->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
                ->select(
                    DB::raw("' - ' as id"),
                    DB::raw("' - ' as room_type"),
                    DB::raw("' - ' as essen"),
                    DB::raw("hotels.name as hotel"),
                    DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                    DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
                    DB::raw("DATE_FORMAT(packages.check_in,'%d/%m/%Y') AS check_in"),
                    DB::raw("' - ' AS day"),
                    DB::raw("DATE_FORMAT(packages.check_out,'%d/%m/%Y') AS check_out"),
                    DB::raw("' - ' as nights"),
                    DB::raw("' - ' as room_code"),
                    DB::raw("' - ' as price"),
                    DB::raw("packages.notes as room_notes"),
                    DB::raw("GROUP_CONCAT(packages_type.name SEPARATOR ', ') as packages"),
                    DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.id = packages.client_id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.client_id = packages.client_id)) AS members")
                )
                ->where('packages.book_id', '=', $bookingID)
                ->groupBy('packages.id')
                ->get();

            if (!$packageData->isEmpty()) {

                if ($packageData->count() > 1) {


                    foreach ($packageData->groupBy('hotel') as $hotel) {

                        $data = array(
                            "booking" => $booking,
                            "room" => $hotel,


                        );


                        Mail::to('janete.dominguez@travelone.pt')
                            ->cc('michaeljdsilva@gmail.com')
                            ->send(new BookingMail($data));

                    }


                } else {

                    $data = array(
                        "booking" => $booking,
                        "room" => $packageData,
                    );


                    Mail::to('janete.dominguez@travelone.pt')
                        ->cc('michaeljdsilva@gmail.com')
                        ->send(new BookingMail($data));

                }

            }

        }

        $carsData = DB::table('cars')
            ->leftjoin('rentacars', 'rentacars.id', '=', 'cars.rentacar_id')
            ->select(
                DB::raw("cars.*"),
                DB::raw("rentacars.name as rentacar")
            )
            ->where('book_id', '=', $bookingID)
            ->first();


        if (!empty($carsData)) {

            $data2 = array(
                "booking" => $booking,
                "car" => $carsData,


            );


            Mail::to('janete.dominguez@travelone.pt')
                ->cc('michaeljdsilva@gmail.com')
                ->send(new BookingMail($data2));
        }

    }

    static function validateBooking($bookingID)
    {

//        if ($state == 'BU') {

        self::sendBookingEmail($bookingID);

        DB::table('bookings')
            ->where('id', '=', $bookingID)
            ->update(
                ['locked' => 0]
            );


        $import = new Imports();


        $bookingIds = DB::table('bookings')
            ->where([
                ['import_id', '=', $import::findImport()->importId,],
                ['locked', '=', '0']
            ])
            ->count();

        if ($bookingIds == 0) {

            DB::table('imports')
                ->where('id', '=', $import::findImport()->importId)
                ->update(
                    ['active' => 0]
                );

        }

//        } else {
//            self::sendBookingEmail($bookingID, $state);
//
//        }


    }

    static function updateBooking($bookId, $field, $value)
    {

        if ($value) {

            DB::table('bookings')
                ->where('id', '=', $bookId)
                ->update(
                    [$field => $value]
                );

        }

    }

    static function exportBookings($state, $view, $locked)
    {

        if ($view == 'import') {

            if ($state == 'Novas') {

                $bookingsTable = DB::table('bookings')
                    ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                    ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                    ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                    ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                    ->leftJoin('clients', 'clients.room_id', '=', 'rooms.id')
                    ->leftJoin('client_room', 'rooms.id', '=', 'client_room.room_id')
                    ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                    ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                    //->leftJoin('packages', 'packages.book_id', '=', 'bookings.id') // pacote por booking
                    //->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
                    ->leftJoin('packages', 'packages.client_id', '=', 'clients.id') //pacote por pessoa e não por booking
                    ->leftJoin('packages_type', 'packages_type.id', '=', 'packages.type_id') //pacote por pessoa e não por booking
                    ->leftJoin('cars', 'cars.book_id', '=', 'bookings.id')
                    ->leftJoin('rentacars', 'cars.rentacar_id', '=', 'rentacars.id')
                    ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
                    ->where([
                        ['imports.user_id', '=', Auth::id()],
                        ['bookings.locked', '=', $locked],
                        ['bookings.state', '=', 'BU'],
                    ])
                    ->select(


                        DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                        DB::raw("hotels.name as HOTEL"),
                        DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN"),
                        DB::raw("rooms.day AS DAY"),
                        DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                        DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),
                        
                        //DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                //DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
        
                DB::raw("count_adults(rooms.id) as ADULTS"),
                        DB::raw("count_kids(rooms.id) as KIDS"),
                        DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                        DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                        DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                        DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.room_id = rooms.id)) AS NAMES"),
                        DB::raw("rooms.essen as ESSEN"),
                        DB::raw("guides.abr as GUIDE"),
                        DB::raw("bookings.book_number AS BOOKING"),
                        DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                        DB::raw("bookings.state as STATE"),
                        DB::raw("rooms.room_type as ROOMTYPE"),
                        DB::raw("rooms.room_code as ROOMCODE"),
                        DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMSQTY"),
                        DB::raw("rooms.nights as ROOMNIGHTS"),
                        DB::raw("rooms.notes as ROOMNOTES"),
                        DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                        DB::raw("cars.type as CARTYPE"),

                        DB::raw(" ' ' as WANDER"),
                        DB::raw(" ' ' as NUM_WANDER"),


                        DB::raw("bookings.barco_bloqueio AS BARCOBLOQUEIO"),

                        DB::raw(" ' ' as EXTRAS"),
                        DB::raw(" ' ' as 2HTL"),
                        DB::raw(" ' ' as WELLNESS"),

                        DB::raw("IFNULL(transfers.type,'Não Tem') as TRANSFER"),

                        DB::raw(" ' ' as GENUSS"),

                        DB::raw("cars.notes as CARNOTES"),

                        DB::raw(" rentacars.name as RENT_A_CAR"),

                        DB::raw("rentacars.name as RENTACAR"),

                        DB::raw(" ' ' as PRIVILEGE"),

                        DB::raw("rooms.price as ROOMPRICE"),

                        DB::raw(" ' ' as TAUCHEN"),

                        DB::raw("packages.tuachen_nr_pax as PACKPAX"),
                        DB::raw("packages.tuachen_qty as PACKQTY"),

                        DB::raw(" ' ' as Hochsaisonzuschlag"),

                        DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                        DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                        DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                        DB::raw("GROUP_CONCAT( CONCAT(clients.age) SEPARATOR ' , ') AS AGE"),

                        //DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),

                        DB::raw("hotels.code as HOTELCODE"),
                        DB::raw("rooms.catering as CATERING"),
                        DB::raw("packages_type.name as PACKAGENAME"),
                        DB::raw("packages.days as PACKAGEDAYS"),
                        DB::raw("packages.state as PACKAGESTATE"),
                        DB::raw("packages.price as PACKAGEPRICE"),
                        DB::raw("packages.notes as PACKAGENOTES"),
                        DB::raw("cars.price as CARPRICE")


                    )
                    ->groupBy('rooms.id')
                    ->get()
                    ->map(function ($item, $key) {
                        return (array)$item;
                    })
                    ->all();

            } else {

                $bookingsTable = DB::table('bookings')
                    ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                    ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                    ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                    ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                    ->leftJoin('clients', 'clients.room_id', '=', 'rooms.id')
                    ->leftJoin('client_room', 'rooms.id', '=', 'client_room.room_id')
                    ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                    ->leftJoin('guides', 'hotels.guide_id', '=', 'guides.id')
                    ->leftJoin('packages', 'packages.book_id', '=', 'bookings.id')
                    ->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
                    ->leftJoin('cars', 'cars.book_id', '=', 'bookings.id')
                    ->leftJoin('rentacars', 'cars.rentacar_id', '=', 'rentacars.id')
                    ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
                    ->where('imports.user_id', '=', Auth::id())
                    ->where('bookings.locked', '=', $locked)
                    ->where(function ($q) use ($state) {
                        $q->where('bookings.state', $state == 'Alterações' ? 'UN' : 'ST')
                            ->orWhere('bookings.state', $state == 'Alterações' ? 'US' : '');
                    })
                    ->select(


                        DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                        DB::raw("hotels.name as HOTEL"),
                        DB::raw("DATE_FORMAT(rooms.check_IN,'%d/%m/%Y') as CHECKIN"),
                        DB::raw("rooms.day AS DAY"),
                        DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                        DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),
                        //DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                //DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
        
                DB::raw("count_adults(rooms.id) as ADULTS"),
                        DB::raw("count_kids(rooms.id) as KIDS"),
                        DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                        DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                        DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                        DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.room_id = rooms.id)) AS NAMES"),
                        DB::raw("rooms.essen as ESSEN"),
                        DB::raw("guides.abr as GUIDE"),
                        DB::raw("bookings.book_number AS BOOKING"),
                        DB::raw("flights.departure_location AS DEPARTURELOCATION"),
                        DB::raw("bookings.state as STATE"),
                        DB::raw("rooms.room_type as ROOMTYPE"),
                        DB::raw("rooms.room_code as ROOMCODE"),
                        DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMSQTY"),
                        DB::raw("rooms.nights as ROOMNIGHTS"),
                        DB::raw("rooms.notes as ROOMNOTES"),
                        DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                        DB::raw("cars.type as CARTYPE"),

                        DB::raw(" ' ' as WANDER"),
                        DB::raw(" ' ' as NUM_WANDER"),


                        DB::raw("bookings.barco_bloqueio AS BARCOBLOQUEIO"),

                        DB::raw(" ' ' as EXTRAS"),
                        DB::raw(" ' ' as 2HTL"),
                        DB::raw(" ' ' as WELLNESS"),

                        DB::raw("IFNULL(transfers.type,'Não Tem') as TRANSFER"),

                        DB::raw(" ' ' as GENUSS"),

                        DB::raw("cars.notes as CARNOTES"),

                        DB::raw(" ' ' as RENT_A_CAR"),

                        DB::raw("rentacars.name as RENTACAR"),

                        DB::raw(" ' ' as PRIVILEGE"),

                        DB::raw("rooms.price as ROOMPRICE"),

                        DB::raw(" ' ' as TAUCHEN"),

                        DB::raw("packages.tuachen_nr_pax as TAUCHENPAX"),
                        DB::raw("packages.tuachen_qty as TAUCHENQTY"),

                        DB::raw(" ' ' as Hochsaisonzuschlag"),

                        DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                        DB::raw("flights.arrival_location AS ARRIVALLOCATION"),
                        DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                        DB::raw("GROUP_CONCAT( CONCAT(clients.age) SEPARATOR ' , ') AS AGE"),

                        //DB::raw("GROUP_CONCAT(DISTINCT CONCAT(clients.gender,' ',clients.name) SEPARATOR ' / ') AS NAMES"),

                        DB::raw("hotels.code as HOTELCODE"),
                        DB::raw("rooms.catering as CATERING"),
                        DB::raw("packages_type.name as PACKAGENAME"),
                        DB::raw("packages.days as PACKAGEDAYS"),
                        DB::raw("packages.state as PACKAGESTATE"),
                        DB::raw("packages.price as PACKAGEPRICE"),
                        DB::raw("packages.notes as PACKAGENOTES"),
                        DB::raw("cars.price as CARPRICE")


                    )
                    ->groupBy('rooms.id')
                    ->get()
                    ->map(function ($item, $key) {
                        return (array)$item;
                    })
                    ->all();


            }


        }

        return $bookingsTable;


    }

    static function exportBookingsXML($state, $view, $locked)
    {

        if ($view == 'import') {

            if ($state == 'Novas') {


                $bookingsCol = collect();

                $bookings = DB::table('bookings')
                    ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                    ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
                    ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                    ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                    ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                    ->where([
                        ['imports.user_id', '=', Auth::id()],
                        ['bookings.locked', '=', $locked],
                        ['bookings.state', '=', 'BU'],
                    ])
                    ->select(
//                        DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                        DB::raw("bookings.book_number AS BOOKING"),
                        DB::raw("bookings.id AS BOOKID"),
                        DB::raw("bookings.state as STATE"),
                        DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                        DB::raw("IFNULL(transfers.type,'Não Tem') as TRANSFER"),
                        DB::raw("(SELECT  count(*) FROM clients WHERE book_id = bookings.id and age < 18 and age <> 0) as KIDS"),
                        DB::raw("(SELECT count(*) FROM clients WHERE (book_id = bookings.id) and (age > 18 or age = 0) ) as ADULTS"),
                        DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMSQTY")
                    )
                    ->groupBy('bookings.book_number')
                    ->get();

                foreach ($bookings as $book) {

//                    $bookID = self::findBookingbyBookingNumber($book->BOOKID);

                    $clients = DB::table('bookings')
                        ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                        ->where('bookings.id', '=', $book->BOOKID)
                        ->select(
                            DB::raw("clients.name as NAME"),
                            DB::raw("clients.age as AGE"),
                            DB::raw("clients.gender as GENDER")
                        )
                        ->get();


                    $flights = DB::table('bookings')
                        ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                        ->where('bookings.id', '=', $book->BOOKID)
                        ->select(
                            DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                            DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                            DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),

                            DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                            DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                            DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                            DB::raw("flights.departure_location AS DEPARTURELOCATION"))
                        ->get();


                    $rooms = DB::table('rooms')
                        ->leftJoin('client_room', 'client_room.book_id', '=', DB::raw($book->BOOKID))
                        ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                        ->select(
                            DB::raw("rooms.room_type as ROOMTYPE"),
                            DB::raw("rooms.essen as ESSEN"),
                            DB::raw("rooms.catering as CATERING"),
                            DB::raw("hotels.name as HOTEL"),
                            DB::raw("hotels.code as HOTELCODE"),
                            DB::raw("DATE_FORMAT(rooms.check_in,'%d/%m/%Y') as CHECKIN"),
                            DB::raw("rooms.day AS DAY"),
                            DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                            DB::raw("rooms.nights as NIGHTS"),
                            DB::raw("rooms.room_code as ROOMCODE"),
                            DB::raw("rooms.price as PRICE"),
                            DB::raw("rooms.notes as ROOMNOTES"),
                            //  DB::raw("(SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') from clients where id =  client_room.client_id) as members")
                            DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ')
                        FROM clients
                        WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ')
                        FROM client_room
                        LEFT JOIN clients on clients.id = client_room.client_id
                        WHERE client_room.room_id = rooms.id)) AS MEMBERS")
                        )
                        ->where('rooms.book_id', '=', $book->BOOKID)
                        ->groupBy('rooms.id')
                        ->get();


                    $packages = DB::table('bookings')
                        ->leftJoin('packages', 'packages.book_id', '=', 'bookings.id')
                        ->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
                        ->leftJoin('clients as cl', 'packages.client_id', '=', 'cl.id')
                        ->leftJoin('hotels', 'packages.hotel_id', '=', 'hotels.id')
                        ->where('bookings.id', '=', $book->BOOKID)
                        ->select(
                            DB::raw("packages_type.name as PACKAGE"),
                            DB::raw("cl.name as CLIENT"),
                            DB::raw("packages.days as DAYS"),
                            DB::raw("packages.tuachen_nr_pax as pack_pax"),
                            DB::raw("packages.tuachen_qty as pack_qty"),
                            DB::raw("packages.state as STATE"),
                            DB::raw("packages.price as PRICE"),
                            DB::raw("hotels.name as HOTEL"),
                            DB::raw("packages.notes as NOTES"),
                            DB::raw("DATE_FORMAT(packages.check_in,'%d/%m/%Y') as CHECKIN"),
                            DB::raw("DATE_FORMAT(packages.check_out,'%d/%m/%Y') as CHECKOUT")
                        )
                        ->get();

                    $car = DB::table('cars')
                        ->where('book_id', '=', $book->BOOKID)
                        ->leftJoin('rentacars', 'cars.rentacar_id', '=', 'rentacars.id')
                        ->select(
                            DB::raw("rentacars.name as RENTACAR"),
                            DB::raw("cars.type as TYPE"),
                            DB::raw("cars.notes as NOTES"),
                            DB::raw("cars.price as PRICE")

                        )
                        ->get();


                    $bookArray = array(
                        "book" => $book,
                        "clients" => $clients,
                        "flights" => $flights,
                        "rooms" => $rooms,
                        "packages" => $packages,
                        "car" => $car,

                    );
//                    $book->BOOKING
                    $bookingsCol->push($bookArray);

                }


            } else {


                $bookingsCol = collect();

                $bookings = DB::table('bookings')
                    ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
                    ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
                    ->leftJoin('imports', 'bookings.import_id', '=', 'imports.id')
                    ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                    ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
                    ->where('imports.user_id', '=', Auth::id())
                    ->where('bookings.locked', '=', $locked)
                    ->where(function ($q) use ($state) {
                        $q->where('bookings.state', $state == 'Alterações' ? 'UN' : 'ST')
                            ->orWhere('bookings.state', $state == 'Alterações' ? 'US' : '');
                    })
                    ->select(
//                        DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS TOM"),
                        DB::raw("bookings.book_number AS BOOKING"),
                        DB::raw("bookings.id AS BOOKID"),
                        DB::raw("bookings.state as STATE"),
                        DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS BOOKINGDATE"),
                        DB::raw("IFNULL(transfers.type,'Não Tem') as TRANSFER"),
                        DB::raw("(SELECT  count(*) FROM clients WHERE book_id = bookings.id and age < 18 and age <> 0) as KIDS"),
                        DB::raw("(SELECT count(*) FROM clients WHERE (book_id = bookings.id) and (age > 18 or age = 0) ) as ADULTS"),
                        DB::raw("(SELECT  count(*) FROM rooms WHERE book_id = bookings.id ) as ROOMSQTY")
                    )
                    ->groupBy('bookings.id')
                    ->get();

                foreach ($bookings as $book) {

//                    $bookID = self::findBookingbyBookingNumber($book->BOOKID);

                    $clients = DB::table('bookings')
                        ->leftJoin('clients', 'clients.book_id', '=', 'bookings.id')
                        ->where('bookings.id', '=', $book->BOOKID)
                        ->select(
                            DB::raw("clients.name as NAME"),
                            DB::raw("clients.age as AGE"),
                            DB::raw("clients.gender as GENDER")
                        )
                        ->get();


                    $flights = DB::table('bookings')
                        ->rightJoin('flights', 'flights.book_id', '=', 'bookings.id')
                        ->where('bookings.id', '=', $book->BOOKID)
                        ->select(
                            DB::raw("flights.arrival_flight AS ARRIVALFLIGHT"),
                            DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS ARRIVALDATE"),
                            DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS ARRIVALTIME"),

                            DB::raw("flights.departure_flight AS DEPARTUREFLIGHT"),
                            DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS DEPARTUREDATE"),
                            DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS DEPARTURETIME"),
                            DB::raw("flights.departure_location AS DEPARTURELOCATION"))
                        ->get();


                    $rooms = DB::table('rooms')
                        ->leftJoin('client_room', 'client_room.book_id', '=', DB::raw($book->BOOKID))
                        ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
                        ->select(
                            DB::raw("rooms.room_type as ROOMTYPE"),
                            DB::raw("rooms.essen as ESSEN"),
                            DB::raw("rooms.catering as CATERING"),
                            DB::raw("hotels.name as HOTEL"),
                            DB::raw("hotels.code as HOTELCODE"),
                            DB::raw("DATE_FORMAT(rooms.check_in,'%d/%m/%Y') as CHECKIN"),
                            DB::raw("rooms.day AS DAY"),
                            DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as CHECKOUT"),
                            DB::raw("rooms.nights as NIGHTS"),
                            DB::raw("rooms.room_code as ROOMCODE"),
                            DB::raw("rooms.price as PRICE"),
                            DB::raw("rooms.notes as ROOMNOTES"),
                            //  DB::raw("(SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') from clients where id =  client_room.client_id) as members")
                            DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ')
                        FROM clients
                        WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ')
                        FROM client_room
                        LEFT JOIN clients on clients.id = client_room.client_id
                        WHERE client_room.room_id = rooms.id)) AS MEMBERS")
                        )
                        ->where('rooms.book_id', '=', $book->BOOKID)
                        ->groupBy('rooms.id')
                        ->get();


                    $packages = DB::table('bookings')
                        ->leftJoin('packages', 'packages.book_id', '=', 'bookings.id')
                        ->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
                        ->leftJoin('clients as cl', 'packages.client_id', '=', 'cl.id')
                        ->leftJoin('hotels', 'packages.hotel_id', '=', 'hotels.id')
                        ->where('bookings.id', '=', $book->BOOKID)
                        ->select(
                            DB::raw("packages_type.name as PACKAGE"),
                            DB::raw("cl.name as CLIENT"),
                            DB::raw("packages.days as DAYS"),
                            DB::raw("packages.tuachen_nr_pax as PACKPAX"),
                            DB::raw("packages.tuachen_qty as PACKQTY"),
                            DB::raw("packages.state as STATE"),
                            DB::raw("packages.price as PRICE"),
                            DB::raw("hotels.name as HOTEL"),
                            DB::raw("packages.notes as NOTES"),
                            DB::raw("DATE_FORMAT(packages.check_in,'%d/%m/%Y') as CHECKIN"),
                            DB::raw("DATE_FORMAT(packages.check_out,'%d/%m/%Y') as CHECKOUT")
                        )
                        ->get();

                    $car = DB::table('cars')
                        ->where('book_id', '=', $book->BOOKID)
                        ->leftJoin('rentacars', 'cars.rentacar_id', '=', 'rentacars.id')
                        ->select(
                            DB::raw("rentacars.name as RENTACAR"),
                            DB::raw("cars.type as TYPE"),
                            DB::raw("cars.notes as NOTES"),
                            DB::raw("cars.price as PRICE")

                        )
                        ->get();


                    $bookArray = array(
                        "book" => $book,
                        "clients" => $clients,
                        "flights" => $flights,
                        "rooms" => $rooms,
                        "packages" => $packages,
                        "car" => $car,

                    );
//                    $book->BOOKING
                    $bookingsCol->push($bookArray);

                }


            }


        }


        $formatter = Formatter::make(json_encode($bookingsCol), Formatter::JSON);
        $xmlFile = $formatter->toXml();

        $filename = 'exportacaoXML_' . Carbon::now() . '_' . $state . '.xml';
        return response()->xmlattachment($xmlFile, $filename);


    }


    static function emailRoomsData($bookingID)
    {
        $booking = DB::table('bookings')
            ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
            ->leftJoin('flights', 'flights.book_id', '=', 'bookings.id')
            ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
            ->select(

                DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS tom"),
                DB::raw("bookings.book_number AS booking_number"),
                DB::raw("bookings.id AS booking_id"),
                DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS booking_date"),
                DB::raw("flights.arrival_flight AS arrival_flight"),
                DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS arrival_date"),
                DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS arrival_time"),
                DB::raw("flights.arrival_location AS arrival_location"),
                DB::raw("flights.departure_flight AS departure_flight"),
                DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS departure_date"),
                DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS departure_time"),
                DB::raw("flights.departure_location AS destiny"),
                DB::raw("flights.id AS flightid"),
                DB::raw("bookings.state as state"),
                DB::raw("transfers.type as transfer")
            )
            ->where('bookings.id', '=', $bookingID)
            ->groupBy('bookings.book_number')
            ->get();


        $roomsData = DB::table('rooms')
            ->leftJoin('hotels', 'hotels.id','=','rooms.hotel_id')
		->leftJoin('clients','clients.room_id', '=', 'rooms.id')
		->leftJoin('packages', 'packages.client_id', '=', 'clients.id')
		->leftJoin('packages_type', 'packages_type.id', '=', 'packages.type_id')
        /*    ->leftJoin('client_room', 'rooms.id', '=', 'client_room.room_id')
            ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->leftjoin('clients', 'clients.book_id', '=', 'client_room.book_id')
            //->leftJoin('packages', 'packages.hotel_id', '=', DB::raw($packages?$packages->packageHotelId:'rooms.hotel_id'))
            ->leftJoin('packages', 'packages.hotel_id', '=', DB::raw($packages?$packages->packageHotelId:'NULL'))
			->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')*/
            ->select(
                DB::raw("rooms.id as id"),
                DB::raw("rooms.room_type as room_type"),
                DB::raw("rooms.essen as essen"),
              //DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                //DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
        
                DB::raw("count_adults(rooms.id) as adults"),
                        DB::raw("count_kids(rooms.id) as kids"),
                DB::raw("DATE_FORMAT(rooms.check_in,'%d/%m/%Y') as check_in"),
                DB::raw("rooms.day AS day"),
                DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as check_out"),
                DB::raw("rooms.nights as nights"),
                DB::raw("rooms.room_code as room_code"),
                DB::raw("rooms.price as price"),
                DB::raw("rooms.notes as room_notes"),
                DB::raw("GROUP_CONCAT(packages_type.name SEPARATOR ', ') as packages"),
                  DB::raw("GROUP_CONCAT(CONCAT(clients.age) SEPARATOR ' , ') AS ages"),
                DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.room_id = rooms.id)) AS members")
            )
            ->where('rooms.book_id', '=', $bookingID)
            ->groupBy('rooms.id')
            ->get();

        if ($roomsData->count() > 1) {


            $roomsColl = collect();

            foreach ($roomsData->groupBy('hotel') as $hotel) {

                $data = array(
                    "booking" => $booking,
                    "room" => $hotel,


                );

                return $roomsColl . push($data);


            }


        } else {


            return $data = array(
                "booking" => $booking,
                "room" => $roomsData,


            );

        }


    }

    static function emailRoomsData2($bookingID, $roomId)
    {

        $packages = DB::table('packages')
            ->where('book_id', '=', $bookingID)
            ->select('hotel_id as packageHotelId')
            ->first();

        $booking = DB::table('bookings')
            ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
            ->leftJoin('flights', 'flights.book_id', '=', 'bookings.id')
            ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
            ->select(

                DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS tom"),
                DB::raw("bookings.book_number AS booking_number"),
                DB::raw("bookings.id AS booking_id"),
                DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS booking_date"),
                DB::raw("flights.arrival_flight AS arrival_flight"),
                DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS arrival_date"),
                DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS arrival_time"),
                DB::raw("flights.arrival_location AS arrival_location"),
                DB::raw("flights.departure_flight AS departure_flight"),
                DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS departure_date"),
                DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS departure_time"),
                DB::raw("flights.departure_location AS destiny"),
                DB::raw("flights.id AS flightid"),
                DB::raw("bookings.state as state"),
                DB::raw("transfers.type as transfer")
            )
            ->where('bookings.id', '=', $bookingID)
            ->groupBy('bookings.book_number')
            ->get();


        $roomsData = DB::table('rooms')
          ->leftJoin('hotels', 'hotels.id','=','rooms.hotel_id')
		->leftJoin('clients','clients.room_id', '=', 'rooms.id')
		->leftJoin('packages', 'packages.client_id', '=', 'clients.id')
		->leftJoin('packages_type', 'packages_type.id', '=', 'packages.type_id')
        /*    ->leftJoin('client_room', 'rooms.id', '=', 'client_room.room_id')
            ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->leftjoin('clients', 'clients.book_id', '=', 'client_room.book_id')
            //->leftJoin('packages', 'packages.hotel_id', '=', DB::raw($packages?$packages->packageHotelId:'rooms.hotel_id'))
            ->leftJoin('packages', 'packages.hotel_id', '=', DB::raw($packages?$packages->packageHotelId:'NULL'))
			->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')*/
            ->select(
                DB::raw("rooms.id as id"),
                DB::raw("rooms.room_type as room_type"),
                DB::raw("rooms.essen as essen"),
                DB::raw("hotels.name as hotel"),
                //DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                //DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
        
                DB::raw("count_adults(rooms.id) as adults"),
                DB::raw("count_kids(rooms.id) as kids"),
                DB::raw("DATE_FORMAT(rooms.check_in,'%d/%m/%Y') as check_in"),
                DB::raw("rooms.day AS day"),
                DB::raw("DATE_FORMAT(rooms.check_out,'%d/%m/%Y') as check_out"),
                DB::raw("rooms.nights as nights"),
                DB::raw("rooms.room_code as room_code"),
                DB::raw("rooms.price as price"),
                DB::raw("rooms.notes as room_notes"),
                DB::raw("GROUP_CONCAT(packages_type.name SEPARATOR ', ') as packages"),
                  DB::raw("GROUP_CONCAT( CONCAT(clients.age) SEPARATOR ' , ') AS ages"),
                DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.room_id = rooms.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.room_id = rooms.id)) AS members")
            )
            ->where('rooms.id', '=', $roomId)
            ->groupBy('rooms.id')
            ->get();

        if (!$roomsData->isEmpty()) {
            if ($roomsData->count() > 1) {


                foreach ($roomsData->groupBy('hotel') as $hotel) {

                    return $data = array(
                        "booking" => $booking,
                        "room" => $hotel,


                    );


//                Mail::to('clayson.capo@gmail.com')
//                    ->cc('michaeljdsilva@gmail.com')

//                Mail::to('michaeljdsilva@gmail.com')
//                    ->send(new BookingMail($data));

                }


            } else {

                return $data = array(
                    "booking" => $booking,
                    "room" => $roomsData,


                );


            }
        } else {

            $packageData = DB::table('packages')
                ->leftJoin('hotels', 'packages.hotel_id', '=', 'hotels.id')
                ->leftjoin('clients', 'clients.id', '=', 'packages.client_id')
                ->leftJoin('packages_type', 'packages.type_id', '=', 'packages_type.id')
                ->select(
                    DB::raw("' - ' as id"),
                    DB::raw("' - ' as room_type"),
                    DB::raw("' - ' as essen"),
                    DB::raw("hotels.name as hotel"),
                    DB::raw("(SELECT  count(*) FROM clients WHERE book_id = " . $bookingID . " and age < 18 and age <> 0) as kids"),
                    DB::raw("(SELECT count(*) FROM clients WHERE (book_id = " . $bookingID . ") and (age > 18 or age = 0) ) as adults"),
                    DB::raw("DATE_FORMAT(packages.check_in,'%d/%m/%Y') AS check_in"),
                    DB::raw("' - ' AS day"),
                    DB::raw("DATE_FORMAT(packages.check_out,'%d/%m/%Y') AS check_out"),
                    DB::raw("' - ' as nights"),
                    DB::raw("' - ' as room_code"),
                    DB::raw("' - ' as price"),
                    DB::raw("packages.notes as room_notes"),
                    DB::raw("GROUP_CONCAT(packages_type.name SEPARATOR ', ') as packages"),
                    DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.id = packages.client_id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.client_id = packages.client_id)) AS members")
                )
                ->where('packages.book_id', '=', $bookingID)
                ->groupBy('packages.id')
                ->get();

            if (!$packageData->isEmpty()) {

                if ($packageData->count() > 1) {


                    foreach ($packageData->groupBy('hotel') as $hotel) {

                        return $data = array(
                            "booking" => $booking,
                            "room" => $hotel,


                        );


                    }


                } else {

                    return $data = array(
                        "booking" => $booking,
                        "room" => $packageData,
                    );


                }

            }

        }


    }

    static function emailCarsdata($bookingID)
    {

        $booking = DB::table('bookings')
            ->leftJoin('tmsrecords', 'bookings.tms_id', '=', 'tmsrecords.ID')
            ->leftJoin('flights', 'flights.book_id', '=', 'bookings.id')
            ->leftJoin('transfers', 'transfers.book_id', '=', 'bookings.id')
            ->select(

                DB::raw("CONCAT('TMS ',tmsrecords.NR_TOM, '/',tmsrecords.SERIE) AS tom"),
                DB::raw("bookings.book_number AS booking_number"),
                DB::raw("bookings.id AS booking_id"),
                DB::raw("DATE_FORMAT(bookings.book_date,'%d/%m/%Y') AS booking_date"),
                DB::raw("flights.arrival_flight AS arrival_flight"),
                DB::raw("DATE_FORMAT(flights.arrival_date,'%d/%m/%Y') AS arrival_date"),
                DB::raw("TIME_FORMAT(flights.arrival_time, '%H:%i') AS arrival_time"),
                DB::raw("flights.arrival_location AS arrival_location"),
                DB::raw("flights.departure_flight AS departure_flight"),
                DB::raw("DATE_FORMAT(flights.departure_date,'%d/%m/%Y') AS departure_date"),
                DB::raw("TIME_FORMAT(flights.departure_time, '%H:%i') AS departure_time"),
                DB::raw("flights.departure_location AS destiny"),
                DB::raw("flights.id AS flightid"),
                DB::raw("bookings.state as state"),
                DB::raw("IFNULL((SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM clients WHERE clients.book_id = bookings.id), (SELECT GROUP_CONCAT(clients.name SEPARATOR '/ ') FROM client_room LEFT JOIN clients on clients.id = client_room.client_id WHERE client_room.book_id = bookings.id)) AS members"),
                DB::raw("transfers.type as transfer")
            )
            ->where('bookings.id', '=', $bookingID)
            ->groupBy('bookings.book_number')
            ->get();

        $carsData = DB::table('cars')
            ->leftjoin('rentacars', 'rentacars.id', '=', 'cars.rentacar_id')
            ->select(
                DB::raw("cars.*"),
                DB::raw("rentacars.name as rentacar")
            )
            ->where('book_id', '=', $bookingID)
            ->get();

        return $data = array(
            "booking" => $booking,
            "car" => $carsData,


        );

    }


}
