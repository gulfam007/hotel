<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Mail\BookingMail;



class Imports extends Model
{


    public function newImport($configId)
    {

        $importsTable = DB::table('imports');
        $importsTable->insert(
            [
                'user_id' => Auth::id(),
                'creation_date' => Carbon::today(),
                'active' => 1,
                'last_modified' => Carbon::now(),
                'config_id' => $configId

            ]
        );

    }

    static function checkValidImport($bookID)
    {
        $bookingsTable = DB::table('bookings');

        $carsTable = DB::table('cars');

        if($bookID){

            $carsTable
                ->select('type')
                ->where('cars.book_id', '=', $bookID)
                ->first();

            if($carsTable->count() == 0){

                return $bookingsTable
                    ->leftJoin('transfers','transfers.book_id','=','bookings.id')
                    ->leftJoin('rooms','rooms.book_id','=','bookings.id')
                    ->leftJoin('hotels','hotels.id','=','rooms.hotel_id')
                    ->where(function($query) use ($bookID) {
                        $query
                            ->where('bookings.id','=',$bookID);
                    })->where(function($query) use ($bookID) {
                        $query->orWhere('transfers.type','=','Verificar')
                            ->orWhere('hotels.id','=',190)
                            ->orWhere('transfers.type', '=', null);
                    })
                    ->select('bookings.book_number as bookNumber')
                    ->first();
            }else{

                return $bookingsTable
                    ->leftJoin('cars','cars.book_id','=','bookings.id')
                    ->leftJoin('transfers','transfers.book_id','=','bookings.id')
                    ->leftJoin('rooms','rooms.book_id','=','bookings.id')
                    ->leftJoin('hotels','hotels.id','=','rooms.hotel_id')
                    ->where(function($query) use ($bookID) {
                        $query
                            ->where('bookings.id','=',$bookID);
                    })->where(function($query) use ($bookID) {
                        $query
                            ->orWhere('transfers.type','=','Verificar')
                            ->orWhere('transfers.type', '=', null)
                            ->orWhere('hotels.id','=',190)
                            ->orWhere('cars.rentacar_id','=',null);
                    })
                    ->select('bookings.book_number as bookNumber')
                    ->first();
            }

//


        }else{

            $importId = self::findImport()->importId;

            $bookingsTable2 = DB::table('bookings');

            $bookingsTable2
                ->select('cars.id as carId')
                ->join('cars','cars.book_id','=','bookings.id')
                ->where('bookings.import_id', '=',$importId)
                ->first();



            if($bookingsTable2->count() == 0){

                return $bookingsTable
                    ->leftJoin('transfers','transfers.book_id','=','bookings.id')
                    ->leftJoin('rooms','rooms.book_id','=','bookings.id')
                    ->leftJoin('hotels','hotels.id','=','rooms.hotel_id')
                    ->where('bookings.import_id', '=',$importId)
                    ->where('transfers.type', '=', 'Verificar')
                    ->orWhere('hotels.id', '=', 190)
                    ->select('bookings.book_number as bookNumber')
                    ->first();

            }else{

                return $bookingsTable
                    ->join('cars','cars.book_id','=','bookings.id')
                    ->leftJoin('transfers','transfers.book_id','=','bookings.id')
                    ->leftJoin('rooms','rooms.book_id','=','bookings.id')
                    ->leftJoin('hotels','hotels.id','=','rooms.hotel_id')
                    ->where('bookings.import_id', '=',$importId)
                    ->where('cars.rentacar_id', '=', null)
                    ->orWhere('transfers.type', '=', 'Verificar')
                    ->orWhere('hotels.id', '=', 190)
                    ->select('bookings.book_number as bookNumber')
                    ->first();
            }



        }



    }

    static function findImport()
    {

      return  $importsTable = DB::table('imports')
                ->leftJoin('import_config', 'import_config.id', '=', 'imports.config_id')
                ->where('imports.user_id', '=', Auth::id())
                ->where('imports.active', '=', '1')
                ->select('imports.id as importId', 'import_config.operator_id as operatorId')
                ->first();


    }

    static function cancelImport()
    {

        $importId = self::findImport()->importId;

        $bookingIds = DB::table('bookings')
            ->where('import_id', '=', $importId)
            ->select('id')
            ->get();

        foreach ($bookingIds as $bookID) {

            DB::table('packages')
                ->where('book_id', '=', $bookID->id)
                ->delete();
            DB::table('clients')
                ->where('book_id', '=', $bookID->id)
                ->delete();
            DB::table('flights')
                ->where('book_id', '=', $bookID->id)
                ->delete();
            DB::table('rooms')
                ->where('book_id', '=', $bookID->id)
                ->delete();
            DB::table('transfers')
                ->where('book_id', '=', $bookID->id)
                ->delete();
            DB::table('cars')
                ->where('book_id', '=', $bookID->id)
                ->delete();
            DB::table('bookings')
                ->where('id', '=', $bookID->id)
                ->delete();
        }


        DB::table('bookings_history')
            ->where('import_id', '=', $importId)
            ->select('id')
            ->delete();

        DB::table('imports')
            ->where('id', '=', $importId)
            ->delete();


    }

    static function validateImport()
    {

        $collection1 = collect();
        $collection2 = collect();
        $collection3 = collect();
        $collection4 = collect();

        //TODO MAKE IT FASTER!!!

        $importId = self::findImport()->importId;


        $roomsData = DB::table('bookings')
            ->leftJoin('rooms', 'rooms.book_id', '=', 'bookings.id')
            ->leftJoin('hotels', 'rooms.hotel_id', '=', 'hotels.id')
            ->where('import_id', '=', $importId)
            //->orderBy('hotels.hotelId')
            ->orderBy('rooms.check_in')
            ->select('hotels.id as hotelId', 'bookings.id as bookId', 'rooms.check_in as checkin', 'rooms.id as roomID','hotels.id as hotelId', 'hotels.name as hotelName')
            ->get();


        $carsData = DB::table('bookings')
            ->join('cars', 'cars.book_id', '=', 'bookings.id')
            ->leftJoin('rentacars', 'cars.rentacar_id', '=', 'rentacars.id')
            ->where('import_id', '=', $importId)
            ->groupBy('rentacars.id')
            ->orderBy('rentacars.id')
            ->orderBy('cars.init_date', 'ASC')
            ->select('rentacars.id as rentacarID', DB::raw('group_concat(bookings.id) as bookId'))
            ->get();

        $collection1->push($roomsData->groupBy('hotelName'));
        //$collection1->push($roomsData->groupBy('hotelId'));
        $collection3->push($carsData->groupBy('rentacarID'));


        foreach ($collection1[0] as $index => $roomData){

            foreach ($roomData as $rooms){

                $collection2->push(Bookings::emailRoomsData2($rooms->bookId, $rooms->roomID));
            }

            //todo add Hotels::findHotelEmail($index)
//          Mail::to(Hotels::findHotelEmail($index))
          Mail::to('janete.dominguez@travelone.pt')
                ->cc('michaeljdsilva@gmail.com')
                ->send(new BookingMail($collection2->all()));
            $collection2 = collect();

        }

        foreach ($collection3[0] as $index => $carData){



            foreach ($carData as $cars){

                $collection4->push(Bookings::emailCarsdata($cars->bookId));
            }

            //todo add findRentaCarEmail($carData->rentacarID)
               // Mail::to(Cars::findRentaCarEmail($index))
          Mail::to('janete.dominguez@travelone.pt')
                ->cc('michaeljdsilva@gmail.com')
                ->send(new BookingMail($collection4->all()));
            $collection4 = collect();

        }


        DB::table('bookings')
            ->where([
                ['import_id', '=', $importId],
                ['locked', '=', 1]
            ])
            ->update(
                ['locked' => 0]
            );


        DB::table('imports')
            ->where('id', '=', $importId)
            ->update(
                ['active' => 0]
            );






    }

    static function resetDb()
    {


        DB::table('cars')
            ->delete();
        DB::table('packages')
            ->delete();
        DB::table('clients')
            ->delete();
        DB::table('flights')
            ->delete();
        DB::table('rooms')
            ->delete();
        DB::table('transfers')
            ->delete();
        DB::table('bookings_history')
            ->delete();
        DB::table('bookings')
            ->delete();
        DB::table('imports')
            ->delete();


    }

}
