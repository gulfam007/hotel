<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TMS extends Model
{
    static function findTMSId($date){
        $TMSTable = DB::table('tmsrecords');
        return $TMSTable->where('DATA', '=', $date)
            ->select('ID')
            ->first();
    }
}
