<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Flights extends Model
{
    public function newFlight($flightInfo){



    $flightsTable = DB::table('flights');
    $flightsTable->insertGetId(
        [

            'book_id' => $flightInfo['bookId'],
            'arrival_flight' => $flightInfo['arrivalFlight'],
            'arrival_date' => $flightInfo['arrivalDate'],
            'arrival_time' => $flightInfo['arrivalTime'],
            'arrival_location' => $flightInfo['arrivalLocation'],
            'departure_flight' => $flightInfo['departureFlight'],
            'departure_date' => $flightInfo['departureDate'],
            'departure_time' => $flightInfo['departureTime'],
            'departure_location' => $flightInfo['departureLocation'],
            'active' => 1

        ]
    );

return $flightsTable;


    }

    static function checkFlight($bookID){

        $flightsTable = DB::table('flights');
        return $flightsTable->where('book_id', '=', $bookID)
            ->select('*')
            ->first();

    }

    static function updateFlights($flightId,$field,$value){

        DB::table('flights')
            ->where('id', '=', $flightId)
            ->update(
                [$field => $value]
            );

    }

    static function fixFlightDeparture($flightInfo,$flightId){

        DB::table('flights')
            ->where('id', '=', $flightId)
            ->update([
                'departure_location' => $flightInfo['departureLocation'],
                'departure_flight' => $flightInfo['departureFlight'],
                'departure_date' => $flightInfo['departureDate'],
                'departure_time' => $flightInfo['departureTime'],

            ]);

    }
    static function fixFlightArrival($flightInfo,$flightId){

        DB::table('flights')
            ->where('id', '=', $flightId)
            ->update([
                'arrival_location' => $flightInfo['arrivalLocation'],
                'arrival_flight' => $flightInfo['arrivalFlight'],
                'arrival_date' => $flightInfo['arrivalDate'],
                'arrival_time' => $flightInfo['arrivalTime'],

            ]);

    }
}
