<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Rooms extends Model
{

    public function newRoom ($roomInfo){


        $roomsTable = DB::table('rooms')->insertGetId(
            [
                'book_id' => $roomInfo['bookId'],
                'hotel_id' => $roomInfo['hotelId'],
                //'hotel_confirm' => $roomInfo['state'],,
                'check_in' => $roomInfo['checkIn'],
                'check_out' => $roomInfo['checkOut'],
                'room_code' => $roomInfo['roomCode'],
                'room_type' => $roomInfo['roomType'],
                'nights' => $roomInfo['nights'],
                'day' => $roomInfo['checkInDay'],
                'notes' => $roomInfo['notes'],
                'price' => $roomInfo['price'],
                'essen' => $roomInfo['essen'],
                'catering' => $roomInfo['catering'],
                'member' => $roomInfo['member'],
                'active' => 1

            ]
        );



        return $roomsTable;
    }

    public function newClientRoom ($clientId,$roomId,$bookId){

        $clientRoomTable = DB::table('client_room');



        $clientRoomTable->insert(
            [
                'client_id' => $clientId,
                'room_id' => $roomId,
                'book_id' => $bookId


            ]
        );

    }

    static function checkRoom($bookID){

        $roomsTable = DB::table('rooms');
        return $roomsTable
            ->where('book_id', '=', $bookID)
            ->leftJoin('hotels', 'hotels.id', '=', 'rooms.hotel_id')
            ->select('rooms.id as id','rooms.check_in as check_in','hotels.code as hotel','rooms.member','rooms.room_type as roomName', 'rooms.notes as roomNotes')
            ->first();

    }

    static function checkRoomMember($bookID,$member){

        $roomsTable = DB::table('rooms');
        return $roomsTable
            ->where('book_id', '=', $bookID)
            ->where('member', '=', $member)
            ->select('*')
            ->first();

    }

    static function checkClientRoom($bookId){

        $clientRoomTable = DB::table('client_room');
        return $clientRoomTable
            ->where('book_id', '=', $bookId)
            ->select('id','client_id','room_id')
            ->get();

    }

    static function updateRoom($roomId,$field,$value){

        DB::table('rooms')
            ->where('id', '=', $roomId)
            ->update(
                [$field => $value]
            );

    }

    static function checkRoomTransfer($bookId){

        $roomTable = DB::table('rooms');
        return $roomTable
            ->where('book_id', '=', $bookId)
            ->where('notes', 'LIKE', '%Transfer ab/bis%')
            ->get();
        }

    static function getRoomsBookId($bookID){

        $roomsTable = DB::table('rooms');
        return $roomsTable
            ->where('book_id', '=', $bookID)
            ->select('id','room_type','check_in')
            ->get();

    }
    static function getRoomsBookIdRoomName($bookID,$roomName){

        $roomsTable = DB::table('rooms');
        return $roomsTable
            ->where('book_id', '=', $bookID)
            ->where('room_type', '=', $roomName)
            ->select('id','room_type')
            ->orderBy('created_at', 'desc')
            ->first();

    }
    static function checkRoomNameBookCheckInHotel($bookID,$roomName,$checkIn,$hotelId){

        $roomsTable = DB::table('rooms');
        return $roomsTable
            ->where('book_id', '=', $bookID)
            ->where('room_type', '=', $roomName)
            ->where('B724194', '=', $checkIn)
            ->where('hotel_id', '=', $hotelId)
            ->select('id')
            ->first();

    }

    static function removeRoom($roomId){

        DB::table('clients')
            ->where('room_id', '=', $roomId)
            ->update(['room_id' => null]);


        DB::table('rooms')
            ->where('id', '=', $roomId)
            ->delete();

        DB::table('client_room')
            ->where('room_id', '=', $roomId)
            ->delete();
    }



}
