<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transfers extends Model
{
    public function newTransfer($transfersInfo){



        $transfersTable = DB::table('transfers');
        $transfersTable->insert(
            [

                'book_id' => $transfersInfo['bookId'],
                'type' => $transfersInfo['type'],
                'active' => 1

            ]
        );


    }

    static function checkTransfer($type,$bookId){

        $transfersTable = DB::table('transfers');
        return $transfersTable
            ->where([
                ['type', '=', $type],
                ['book_id', '=', $bookId],
            ])

            ->select('id','type')
            ->first();

    }

    static function checkingTransfer($bookId){

        $transfersTable = DB::table('transfers');
        return $transfersTable
            ->where('book_id', '=', $bookId)
            ->select('id','type')
            ->first();

    }

    public function updateImportedTransfer($transferId,$transferType){



        $bookingsTable =  DB::table('transfers');

        $bookingsTable
            ->where('id',$transferId)
            ->update(
                [
                    'type' => $transferType
                ]
            );

    }


    static function findTransferWarn(){



        $import = new Imports();
        $response = false;

        $transfersTable = DB::table('transfers')
            ->select('type')
            ->leftJoin('bookings', 'bookings.id', '=', 'transfers.book_id')
            ->where('bookings.import_id', '=', $import::findImport()->importId)
            ->get();

        foreach($transfersTable as $r){

            if ($r->type == 'Verificar'){

                $response = true;
                break;
            }

        }

        return $response;

    }
}
