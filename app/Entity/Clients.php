<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Clients extends Model
{
    public function newClient($clientInfo){




       return DB::table('clients')->insertGetId(
            [

                'book_id' => $clientInfo['bookId'],
                'flight_id' => $clientInfo['flightId'],
                'room_id' => $clientInfo['roomId'],
                'name' => $clientInfo['name'],
                'gender' => $clientInfo['gender'],
                'age' => $clientInfo['age'],
                'active' => 1

            ]
        );


    }

    static function checkClient($bookID,$clientName){

        $clientsTable = DB::table('clients');
        return $clientsTable
            ->where('book_id', '=', $bookID)
            ->where('name', '=', $clientName)
            ->select('id')
            ->first();

    }

    static function checkClients($bookID){

        $clientsTable = DB::table('clients');
        return $clientsTable
            ->where([
                ['book_id', '=', $bookID]
            ])
            ->select('id','room_id','name')
            ->get();

    }

    static function updateClientRoom($clientId,$bookId){

        $roomId = DB::table('rooms')
            ->where([
            ['book_id', '=', $bookId]
            ])
            ->select('id')
            ->first();



       return DB::table('clients')
            ->where('id', '=', $clientId)
            ->update(
                ['room_id' => $roomId ? $roomId->id : NULL]
            );

    }

    static function addClientRoom($clientId,$bookId){


        return DB::table('client_room')
            ->where('book_id', '=', $bookId)
            ->update(
                ['client_id' => $clientId]
            );

    }

    static function hasPackage($bookID,$clientId){

        $packagesTable = DB::table('packages');
        return $packagesTable
            ->where([
                ['book_id', '=', $bookID],
                ['client_id', '=', $clientId],
            ])
            ->select('id')
            ->first();

    }

    static function updateClient($clientId,$field,$value){

        DB::table('clients')
            ->where('id', '=', $clientId)
            ->update(
                [$field => $value]
            );

    }

    static function updateClientRoomEdit($clientId,$roomId,$value){

        DB::table('client_room')
            ->where([
                ['room_id', '=', $roomId],
                ['client_id', '=', $clientId],
            ])
            ->update(
                ['room_id' => $value]
            );

    }

    static function checkClientRoom($roomId,$clientId){

        $clientsTable = DB::table('clients');
        return $clientsTable
            ->where([
                ['room_id', '=', $roomId],
                ['id', '=', $clientId],
            ])
            ->select('id')
            ->first();

    }

    static function removeClient($clientId){

        DB::table('client_room')
            ->where('client_id', '=', $clientId)
            ->delete();

        DB::table('packages')
            ->where('client_id', '=', $clientId)
            ->delete();

        DB::table('clients')
            ->where('id', '=', $clientId)
            ->delete();


    }


}
