<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Hotels extends Model

{

    public function findHotel($hotelCode){

        $hotelsTable = DB::table('hotels');



     /*if(substr($hotelCode, 3, 1) == 8) {

            $hotelCode = substr_replace($hotelCode, "0", 3, 1);

        }else */

            if(strlen($hotelCode) > 6) {

            $hotelCode = substr($hotelCode, 0, strlen($hotelCode)-1);

        }


       return  $hotelsTable->where('code', '=', trim($hotelCode))
                    ->select('id','name','code')
                    ->first();

    }

    public function newHotel($hotelInfo){

        $location = new Locations();

        $hotelsTable = DB::table('hotels');
        $hotelsTable->insert(
            [

                'location_id' => $location::findLocation($hotelInfo->local),
                'name' => $hotelInfo->name,
                'code' => $hotelInfo->code,
                'email' => $hotelInfo->email,
                'transfer_price_mod' => $hotelInfo->trasnferPrice,
                'active' => 1

            ]
        );


    }

    static function hotelOptions(){
        $hotelsTable = DB::table('hotels')
            ->select('id','name as text')
            ->get();

        return $hotelsTable;
    }

    static function findHotelEmail($hotelID){

        $hotelsTable = DB::table('hotels')
        ->where('id', '=', trim($hotelID))
            ->select('email')
            ->first();

        return $hotelsTable->email;

    }




}
