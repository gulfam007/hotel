<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Locations extends Model
{
    static function findLocation($locationName){

        $locationsTable = DB::table('locations');

        return $locationsTable->where('name', '=',  $locationName)
            ->select('id as locationId', 'name as locatioName')
            ->first();


    }
}
