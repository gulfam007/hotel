<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cars extends Model
{
    public function newCar($carsInfo){



        $carsTable = DB::table('cars');
        $carsTable->insert(
            [

                'book_id' => $carsInfo['bookId'],
                'rentacar_id' => $carsInfo['rentacar_id'],
                'type' => $carsInfo['type'],
                'init_date' => $carsInfo['initDate'],
                'end_date' => $carsInfo['endDate'],
                'notes' => $carsInfo['notes'],
                'price' => $carsInfo['price'],
                'active' => 1

            ]
        );


    }

    static function checkCar($bookID){

        $carsTable = DB::table('cars');
        return $carsTable->where('book_id', '=', $bookID)
            ->select('id')
            ->first();

    }

    static function rentacarOptions(){
       $rentacarsTables = DB::table('rentacars')
        ->select('id','name as text')
           ->get();

       return $rentacarsTables;
    }

    static function updateCar($bookId,$field,$value){

        DB::table('cars')
            ->where('book_id', '=', $bookId)
            ->update(
                [$field => $value]
            );

    }

    static function findRentaCarEmail($rentacarID){

        $rentacarTable = DB::table('rentacars')
            ->where('id', '=', trim($rentacarID))
            ->select('email')
            ->first();

        return $rentacarTable->email;

    }

    static function removeCar($carId){
        DB::table('cars')
            ->where('id', '=', $carId)
            ->delete();
    }

}
