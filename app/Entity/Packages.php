<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Packages extends Model
{
    public function newPackage($bookId,$typeId,$clientId, $tuachenNrPax,$tauchenQty,$price,$notes,$days,$state,$hotelId,$checkin,$checkout)
    {


        $packagesTable = DB::table('packages');
        $packagesTable->insert(
            [

                'book_id' => $bookId,
                'type_id' => $typeId,
                'client_id' => $clientId,
                'tuachen_nr_pax' => isset($tuachenNrPax)?$tuachenNrPax:'',
                'tuachen_qty' => isset($tauchenQty)?$tauchenQty:'',
                'price' => isset($price)?$price:'',
                'notes' => isset($notes)?$notes:'',
                'days' => isset($days)?$days:'',
                'state' => isset($state)?$state:'',
                'hotel_id' => isset($hotelId)?$hotelId:'',
                'check_in' => isset($checkin)?$checkin:'',
                'check_out' => isset($checkout)?$checkout:'',
                'active' => 1

            ]
        );


    }

    static function getPackageTypeId($packageName)
    {
        $packagesTypeTable = DB::table('packages_type');
        $packagesTypeTable->where('name', '=', $packageName)
            ->select('id')
            ->first();

    }

    static function getPackageByBookId($bookId)
    {
        $packagesTable = DB::table('packages');
        $packagesTable->where('book_id', '=', $bookId)
            ->select('id')
            ->first();

    }

    static function packageOptions()
    {
        $packageTypeTable = DB::table('packages_type')
            ->select('id', 'name as text')
            ->get();

        return $packageTypeTable;
    }

    static function checkPackageTransfer($bookId){

        $packagesTable = DB::table('packages');
        return $packagesTable
            ->where('book_id', '=', $bookId)
            ->where('notes', 'LIKE', '%Transfer ab/bis%')
            ->get();
    }

    static function removePackage($packageId){
        DB::table('packages')
            ->where('id', '=', $packageId)
            ->delete();
    }
    static function updatePackage($packageId,$price){

        DB::table('packages')
            ->where('id', '=', $packageId)
            ->update(
                ['price' => $price]
            );

    }


}
