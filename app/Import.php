<?php

namespace App;

use App\Entity\Cars;
use App\Entity\Clients;
use App\Entity\Flights;
use App\Entity\Packages;
use App\Entity\Rooms;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\RequestedReservation;
use DateTime;
use Jenssegers\Date\Date;
use App\Entity\Bookings;
use App\Entity\Hotels;
use App\Entity\Transfers;
use App\Entity\TMS;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Carbon;


use App\Entity\Imports;


class Import extends Model
{

    static function importConfigOptions()
    {

        return ($importConfigDropdownOptions = DB::table('import_config')->get());

    }

    public function ImportBooks($file)
    {

        if (!empty($file)) {


            $uploadedFile = Storage::disk('local')->putFile('imports', $file);

            $importedFile = Storage::disk('local')->get($uploadedFile);


            $this->rawText = $importedFile;
            // Search for the first reservation (One letter and 6 digits)
            // ˆ

            $index = $this->textReservationNumberPosition($importedFile);
            if ($index != -1) {


                $titleLine = substr($importedFile, 0, $index); // Title line removing last ; (semicolon)
                $this->rawBody = substr($importedFile, $index); // Remaining text constitutes the reservations body

                $this->reservationsPresent = true; // Mark reservations as present
                $this->headers = $this->singleRecordToArray($titleLine); // Store headers
                $this->requestedReservationsDynamicFields = $this->multipleRecordsToArray($this->rawBody); // Store dynamic fields requested reservations

                $this->headers = $this->addExtraHeaders($this->headers); // Add headers up to the maximum number of fields in records (match in tables)
                $this->resquestedReservations = array(); // Map requestedReservationsDynamicFields to requestedReservations

                $this->mapReservations($this->mapRequestedReservations()); // Map requestedReservations to reservations


            }


        }

    }

    //AUXILIARY FUNCTIONS

    private function textReservationNumberPosition($string)
    {

        $index = -1;


        //if(preg_match("/(F|A|E|B){1}\d{6}/", $string, $matches, PREG_OFFSET_CAPTURE)) {
        // /^[F|A|E|B][0-9]{6}$/
        // /[(F|A|E|B){1}]?\d{6}/
        //([A-Z]{3}|[0-9]{4})
        if (preg_match("/[(F|A|E|B|C){1}]?\d{6}[ ;]/", $string, $matches, PREG_OFFSET_CAPTURE)) {

            $index = $matches[0][1];

        }

        return $index;
    }

    function singleRecordToArray($semiColonDelimitedRecord)
    {

        $semiColonDelimitedRecord = str_replace("^/", "", $semiColonDelimitedRecord);
        //$removeBadChar = str_replace("ˆ", "", $semiColonDelimitedRecord); //added 10/01
        $lines = explode("\n", $semiColonDelimitedRecord); //added 10/01
        $lines = preg_replace("//", "", $lines);
        $recordArray = array();
        foreach ($lines as $line) {

            $lineArray = explode(";", $line);
            $lineArray = array_map('trim', $lineArray);
            if ($lineArray[count($lineArray) - 1] == "") {
                array_pop($lineArray);
            }

            foreach ($lineArray as $cell) {
                array_push($recordArray, $cell);
            }

        }

        return $recordArray;

    }

    private function multipleRecordsToArray($semiColonDelimitedRecords)
    {


        $records = array();
        $semiColonDelimitedRecord = str_replace("^/", "", $semiColonDelimitedRecords); // Remove line delimiters

        $rawFieldsArray = array();

        $lines = explode("\n", $semiColonDelimitedRecord);
        $lines = preg_replace("//", "", $lines);
        $recordArray = array();

        $fieldIsStatus = false; // Used to check if current field is an upload status (BU / UN / US / ST)

        $cellNo = 0;
        $commentFieldCumulator = "";


        foreach ($lines as $line) {

            $lineArray = explode(";", $line);

            $i = 0;
            $lineArrayLen = count($lineArray);


            foreach ($lineArray as $cell) {


                $cellNo++;
                $isBooking = $this->isBookingNr($cell);


                if ($cellNo > 1 && $isBooking) {
                    array_push($rawFieldsArray, $commentFieldCumulator);


                    $commentFieldCumulator = "";

                }

                if ($fieldIsStatus && !$isBooking) {


                    $commentFieldCumulator .= $cell;


                } else if ($isBooking || !$isBooking) {

                    if ($i < $lineArrayLen || trim($cell) != "") { // If last field in line isn't empty, insert field

                        array_push($rawFieldsArray, $cell);


                    }

                }

                if ($cellNo > 1 && $isBooking) {
                    $fieldIsStatus = false;
                } else if (!$fieldIsStatus) {
                    $fieldIsStatus = $this->isStatus($cell);
                }

                $i++;

            }

        }

        if ($commentFieldCumulator !== '') {
            array_push($rawFieldsArray, $commentFieldCumulator);
        }

        $recordId = 0;
        $records[0] = array();
        $firstReservationRecorded = false;
        $fieldCount = 0;
        $rawFieldsCount = 0;


        foreach ($rawFieldsArray as $field) {


            $fieldCount++;
            $reservationNoIndex = $this->textReservationNumberPosition($field);
            $salutatiIndex = $this->salutatiPosition($rawFieldsArray[$rawFieldsCount + 1]);
            $fieldHasLineBreak = strpos(str_replace(' ', '', $field), "\n");

            // If field value is reservation number (not the first), and next field is salutati (HERR, FRAU) change the record
            if ($this->isBookingNr($field) && $salutatiIndex != -1 && $firstReservationRecorded) {
                //$reservationNoIndex = $this->textReservationNumberLastPosition($field); // ADDED
                array_push($records[$recordId], substr($field, 0, $reservationNoIndex)); // Add to record array

                $recordId++;

                $fieldCount = 0; // Reset field counter
                $records[$recordId] = array();
                $field = substr($field, $reservationNoIndex);

            }

            //$rawFieldsCount = 0; // Reset total field counter

            // If field has a line break in the middle without a semicolon, insert two fields
            if ($fieldHasLineBreak > 0) {
                $fields = explode("\n", $field);
                $fieldValue = preg_match('/[a-zA-Z0-9_.-]/', trim($fields[0])) ? trim($fields[0]) : trim($fields[1]);
                array_push($records[$recordId], $fieldValue);

                $field = $fields[1];
                $fieldCount++;
            }

            array_push($records[$recordId], trim($field));
            $firstReservationRecorded = true;
        }


        return $records;

    }

    private function isBookingNr($field)
    {

        $field = trim($field);
        $matches = "";
        $index = -1;
        //if(preg_match("/(F|A|E|B){1}\d{6}/", $field, $matches, PREG_OFFSET_CAPTURE)){
        if (preg_match("/[(F|A|E|B|C){1}]?\d{6}/", $field, $matches, PREG_OFFSET_CAPTURE)) {
            $index = $matches[0][1];
        }
        return $index == 0;

    }

    private function isStatus($field)
    {

        $field = trim($field);
        $statuses = array("BU", "US", "UN", "ST");

        return in_array($field, $statuses);

    }

    private function addExtraHeaders($headers)
    {

        $headersCount = count($this->headers);
        $extraFields = $this->maxFieldCount - $headersCount;

        for ($i = 0; $i < $extraFields; $i++) {
            array_push($headers, "FIELD" . ($i + $headersCount));
        }

        return $headers;

    }

    public function mapRequestedReservations()
    {

        $requestedReservations = array();

        if (count($this->requestedReservationsDynamicFields) > 0) {

            foreach ($this->requestedReservationsDynamicFields as $dynamicReservation) {


                $requestedReservation = new RequestedReservations();

                $dynamicReservation = $this->fixDynamicLength($dynamicReservation);

                $requestedReservation->invoice = isset($dynamicReservation[0]) ? $dynamicReservation[0] : "";
                $requestedReservation->salutati = isset($dynamicReservation[1]) ? $dynamicReservation[1] : "";
                $requestedReservation->nameFirstName = isset($dynamicReservation[2]) ? $dynamicReservation[2] : "";
                $requestedReservation->age = isset($dynamicReservation[3]) ? $dynamicReservation[3] : "";
                $requestedReservation->hotel = isset($dynamicReservation[4]) ? $dynamicReservation[4] : "";
                $requestedReservation->hotelName = isset($dynamicReservation[5]) ? $dynamicReservation[5] : "";
                $requestedReservation->room = isset($dynamicReservation[6]) ? $dynamicReservation[6] : "";
                $requestedReservation->roomName = isset($dynamicReservation[7]) ? $dynamicReservation[7] : "";
                $requestedReservation->cate = isset($dynamicReservation[8]) ? $dynamicReservation[8] : "";
                $requestedReservation->catering = isset($dynamicReservation[9]) ? $dynamicReservation[9] : "";
                $requestedReservation->checkIn = isset($dynamicReservation[10]) ? $dynamicReservation[10] : "";
                $requestedReservation->checkOut = isset($dynamicReservation[11]) ? $dynamicReservation[11] : "";
                $requestedReservation->days = isset($dynamicReservation[12]) ? $dynamicReservation[12] : "";
                $requestedReservation->opt = isset($dynamicReservation[13]) ? $dynamicReservation[13] : "";
                $requestedReservation->optionDate = isset($dynamicReservation[14]) ? $dynamicReservation[14] : "";
                $requestedReservation->noRooms = isset($dynamicReservation[15]) ? $dynamicReservation[15] : "";
                $requestedReservation->members = isset($dynamicReservation[16]) ? $dynamicReservation[16] : "";
                $requestedReservation->flightNo = isset($dynamicReservation[17]) ? $dynamicReservation[17] : "";
                $requestedReservation->departureDate = isset($dynamicReservation[18]) ? $dynamicReservation[18] : "";
                $requestedReservation->departureTime = isset($dynamicReservation[19]) ? $dynamicReservation[19] : "";
                $requestedReservation->arrivalTime = isset($dynamicReservation[20]) ? $dynamicReservation[20] : "";
                $requestedReservation->departureLocation = isset($dynamicReservation[21]) ? $dynamicReservation[21] : "";
                $requestedReservation->arrivalLocation = isset($dynamicReservation[22]) ? $dynamicReservation[22] : "";
                $requestedReservation->connectedLoad = isset($dynamicReservation[24]) ? $dynamicReservation[24] : "";
                $requestedReservation->season = isset($dynamicReservation[25]) ? $dynamicReservation[25] : "";


                //  If field changed is a date instead of hour, the next four fields are dislocated in four indexes

                if (isset($dynamicReservation[26]) && $this->is_date($dynamicReservation[26])) {
                    $requestedReservation->field30 = isset($dynamicReservation[26]) ? $dynamicReservation[26] : "";
                    $requestedReservation->field31 = isset($dynamicReservation[27]) ? $dynamicReservation[27] : "";
                    $requestedReservation->field32 = isset($dynamicReservation[28]) ? $dynamicReservation[28] : "";
                    $requestedReservation->field33 = isset($dynamicReservation[29]) ? $dynamicReservation[29] : "";

                } else {

                    $requestedReservation->changed = isset($dynamicReservation[26]) ? $dynamicReservation[26] : "";
                    $requestedReservation->booked = isset($dynamicReservation[27]) ? $dynamicReservation[27] : "";;
                    $requestedReservation->field28 = isset($dynamicReservation[28]) ? $dynamicReservation[28] : "";
                    $requestedReservation->field29 = isset($dynamicReservation[29]) ? $dynamicReservation[29] : "";
                    $requestedReservation->field30 = isset($dynamicReservation[30]) ? $dynamicReservation[30] : "";
                    $requestedReservation->field31 = isset($dynamicReservation[31]) ? $dynamicReservation[31] : "";
                    $requestedReservation->field32 = isset($dynamicReservation[32]) ? $dynamicReservation[32] : "";
                    $requestedReservation->field33 = isset($dynamicReservation[33]) ? $dynamicReservation[33] : "";

                }

                // Fix Estado
                $estadosPossiveis = array("US", "UN", "BU", "ST");
                if (!in_array($requestedReservation->field32, $estadosPossiveis)) {
                    $estadoTemp = "";
                    for ($i = 26; $i < 34; $i++) {
                        $estadoTemp = isset($dynamicReservation[$i]) ? trim($dynamicReservation[$i]) : "";
                        if (in_array($estadoTemp, $estadosPossiveis)) {
                            $requestedReservation->field32 = $estadoTemp;
                            break;
                        }
                    }

                }

                array_push($requestedReservations, $requestedReservation);
            }


        }


        return $requestedReservations;

    }


    /* mapReservations - initialization from text input
     * $param inputText - The text body with the reservations
     * $returns
    */

    private function getWanderType($hotel)
    {


        $hotelCode = $hotel ? $hotel : 190;


        if ($hotelCode == 'FNC876' || $hotelCode == 'FNC888' || $hotelCode == 'FNC826' || $hotelCode == 'FNC813' || $hotelCode == 'FNC802' || $hotelCode == 'FNC801' || $hotelCode == 'FNC840' || $hotelCode == 'FNC896' || $hotelCode == 'FNC843' || $hotelCode == 'FNC836' || $hotelCode == 'FNC834' || $hotelCode == 'FNC844' || $hotelCode == 'FNC501') {

            return 3;

        } elseif ($hotelCode == 'FNC811' || $hotelCode == 'FNC851' || $hotelCode == 'FNC835' || $hotelCode == 'FNC854') {

            return 6;

        } elseif ($hotelCode == 'FNC815' || $hotelCode == 'FNC809') {

            return 7;

        } else {
            return 10;
        }


    }

    private function mapReservations($requestedReservations)
    {

        $this->reservations = array();


        if (count($requestedReservations) > 0) {

            //TODO: import type methods and Config

            //creates a new import record
            $import = new Imports();
            $import->newImport(1);

            $requestedReservationsAdded = array(); // Follow added booking
            $requestedReservationsProcessed = $requestedReservations;

            $reservationsProcessed = array();
            $newRecordRowId = 0;
            $rentacars = array();
            $packets = array();
            $dynamicPackets = array();
            $totrentcars = 0;
            $totpacket = 0;
            $totnotpacket = 0;
            $totreservation = 0;
            $totreservationok = 0;
            $txc = null;

            foreach ($requestedReservationsProcessed as $requestedReservation) {

                //$dynamicPacketInfo = $this->isDynamicPacket($requestedReservation);

                if ($requestedReservation->hotel == "FNC700") { // Only push record for display if it's not rent a car
                    $totrentcars++;

                    Log::info("Booking: " . $requestedReservation->invoice . " Rent-a-Car found!");

                    // Added a function to create cars once its found. Not Working as expected
                    //$this->createCarForBooking($requestedReservations, $requestedReservation);

                    // If record is Rent a Car, save it for updating matching record
                    array_push($rentacars, $requestedReservation);


                } else if ($this->isPacket($requestedReservation->roomName, $requestedReservation->field33)) { // Only push record if it's not a packet
                    $totnotpacket++;
                    // If record is Packet, save it for updating matching record
                    array_push($packets, $requestedReservation);

                } else if ((strlen($requestedReservation->field28) == 8) && (strlen($requestedReservation->field30) != 8) && ($this->isPacket($requestedReservation->roomName, $requestedReservation->field31))) {
                    // If record is Packet, save it for updating matching record
                    array_push($packets, $requestedReservation);


                } else {
                    $totreservation++;

                    if (preg_match("/[(F|A|E|B|C){1}]?\d{6}/", $requestedReservation->invoice, $matches, PREG_OFFSET_CAPTURE)) {//validate booking number

                        if (!in_array(array($requestedReservation->invoice, $requestedReservation->hotel, $requestedReservation->field32, $requestedReservation->roomName), $reservationsProcessed)) { // If booking + hotel hasn't been inserted
                            $totreservationok++;

                            $fncleerBookings = $this->getFNCLEERBookings($requestedReservationsProcessed);

                            // If there are duplicate bookings, merge them in one record
//                            $duplicateBookingIndexes = $this->getRequestedBookingIndexes($requestedReservationsProcessed, $requestedReservation->invoice, $requestedReservation->hotel, $requestedReservation->field32);
//
//                            if (count($duplicateBookingIndexes)) {
//                                $requestedReservation = $this->mergeBookings($requestedReservationsProcessed, $duplicateBookingIndexes);
//                            }
//                            array_push($reservationsProcessed, array($requestedReservation->invoice, $requestedReservation->hotel, $requestedReservation->field32, $requestedReservation->roomName)); // Mark booking no + hotel as processed

                            // Associated records
                            //$tmsrecord = $TmsrecordsModel->find('all', ['conditions' => ['Tmsrecords.DATA' => $this->convertDate('d.m.y', 'Y-m-d', $requestedReservation->checkIn)]])->first();


                            //BOOKINGS
                            $bookId = $this->createBookings($requestedReservationsProcessed, $requestedReservation);


                            //FLIGHTS
                            $flightId = $this->createFlights($requestedReservationsProcessed, $requestedReservation, $bookId);


                            //ROOOMS / CLIENTS

                            $ntxc = $this->createRooms($requestedReservationsProcessed, $requestedReservation, $bookId, $flightId ? $flightId : null, $txc ? $txc : null);
                            $txc = $ntxc;

                            //CARS
                            $this->createCar($requestedReservationsProcessed, $requestedReservation, $bookId);

                            //TRANSFERS
                            $this->createTransfers($requestedReservationsProcessed, $requestedReservation, $bookId, 'x');


                            //PACKAGES
                            $this->createPackages($requestedReservationsProcessed, $requestedReservation, $bookId);


                            array_push($requestedReservationsAdded, $requestedReservation);


                        }
                    }
                }
            }
        }
    }

    private function bookingInfo($book_number, $state, $book_date, $tms_id)
    {

        $bookingInfo = array(

            //$bookingInfo->code = $code;
            'bookingNumber' => $book_number,
            'state' => $state,
            'bookDate' => $book_date,
            'tmsId' => $tms_id
//            '2HTL' => $booking_htl
        );

        return $bookingInfo;


    }

    private function roomInfo($bookId, $hotelId, $hotelConfirm, $checkIn, $checkOut, $nights, $roomType, $roomCode, $checkIn_day, $notes, $price, $essen, $catering, $member)
    {

        $roomInfo = array(

            'bookId' => $bookId,
            'hotelId' => $hotelId,
            'hotelConfirm' => $hotelConfirm,
            'checkIn' => $checkIn,
            'checkOut' => $checkOut,
            'nights' => $nights,
            'roomType' => $roomType,
            'roomCode' => $roomCode,
            'checkInDay' => $checkIn_day,
            'notes' => $notes,
            'price' => $price,
            'essen' => $essen,
            'catering' => $catering,
            'member' => $member
        );

        return $roomInfo;


    }

    private function flightInfo($bookId, $arrivalFlight, $arrivalLocation, $arrivalDate, $arrivalTime, $departureFlight, $departureLocation, $departureDate, $departureTime)
    {
        if ($arrivalFlight == '') {

            $arrivalFlight = null;
            $arrivalLocation = null;
            $arrivalDate = null;
            $arrivalTime = null;

        }

        if ($departureFlight == '') {

            $departureFlight = null;
            $departureLocation = null;
            $departureDate = null;
            $departureTime = null;

        }


        $flightInfo = array(

            'bookId' => $bookId,

            'arrivalFlight' => $arrivalFlight,
            'arrivalLocation' => $arrivalLocation,
            'arrivalDate' => $arrivalDate,
            'arrivalTime' => $arrivalTime,

            'departureFlight' => $departureFlight,
            'departureLocation' => $departureLocation,
            'departureDate' => $departureDate,
            'departureTime' => $departureTime
        );


        return $flightInfo;

    }

    private function clientsInfo($bookId, $flightId, $name, $gender)
    {

        $clientsInfo = array(

            'bookId' => $bookId,
            'flightId' => $flightId,
            'name' => $name,
            'gender' => $gender
        );

    }

    private function transferInfo($bookId, $type)
    {

        $transferInfo = array(

            'bookId' => $bookId,
            'type' => $type
        );

        return $transferInfo;

    }

    private function carsInfo($bookid, $rentaCarId, $type, $initDate, $endDate, $notes, $carPrice)


    {

        $carsInfo = array(

            'bookId' => $bookid,
            'rentacar_id' => $rentaCarId == 0 ? null : $rentaCarId,
            'type' => $type,
            'initDate' => $initDate,
            'endDate' => $endDate,
            'notes' => $notes,
            'price' => $carPrice
        );


        return $carsInfo;


    }

    private function packageInfo($bookId, $hotelId, $transferPresent, $typeId, $pricePerPeople, $pricePerDay, $startDate, $endDate, $uniqueDate, $extra)
    {


        $packageInfo = array(

            'bookId' => $bookId,
            'hotelId' => $hotelId,
            'tranferPresent' => $transferPresent,
            'typeId' => $typeId,
            'pricePerPeople' => $pricePerPeople,
            'pricePerDay' => $pricePerDay,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'uniqueDate' => $uniqueDate,
            'extra' => $extra
        );


        return $packageInfo;

    }

    private function salutatiPosition($string)
    {

        $index = -1;

        if (preg_match("/HERR|FRAU/", $string, $matches, PREG_OFFSET_CAPTURE)) {

            $index = $matches[0][1];

        }

        return $index;
    }


    /* mapRequestedReservations - initialization from text input
    * $param inputText - The text body with the reservations
    * $returns
   */
    private function getRequestedBookingIndexes($bookings, $bookingNo, $hotelCode, $estado)
    {

        $indexes = array();
        $i = 0;
        foreach ($bookings as $requestedReservation) {

            if ($requestedReservation->invoice == $bookingNo && $requestedReservation->hotel == $hotelCode && $requestedReservation->field32 == $estado) {

                array_push($indexes, $i);


            }

            $i++;

        }

        return $indexes;
    }

    /* fixDynamicLength - If record has less fields than maximum number of fields on a record,
    *                    adds empty strings until field number is the same
    * $param dynamicReservation - Dynamic Fields Reservation
    * $returns Fixed length $dynamicReservation
   */
    private function fixDynamicLength($dynamicReservation)
    {

        $extraFieldsDiff = $this->maxFieldCount - count($dynamicReservation);

        for ($i = 0; $i < $extraFieldsDiff; $i++) {
            array_push($dynamicReservation, "");
        }

        return $dynamicReservation;

    }

    private function is_date($date)
    {

        return preg_match('/[0-9]{2}\.[0-9]{2}\.[0-9]{2}/', $date);
    }

    private function isDynamicPacket($requestedReservation)
    {

        $bookingDate = $requestedReservation->checkIn;
        $bookingDate = $this->changeSeparator(".", "-", $bookingDate);
        $bookingDate = $this->invertDate("-", $bookingDate);

        $pacotesModel = TableRegistry::get('Pacotes');
        $pacotesForReservation = $pacotesModel->find('all', ['conditions' => [
            'Pacotes.DATA_INICIO <=' => $bookingDate,
            'Pacotes.DATA_FIM >=' => $bookingDate]]);

        $packets = array();

        foreach ($pacotesForReservation as $pack) {

            if ($pack->tabelascampos_id_SEARCH_KEYWORD_FIELD == 19) {

                if (stripos($pack->SEARCH_KEYWORD_RESERVA, $requestedReservation->roomName) !== false) {

                    array_push($packets, $pack->tabelascampos_id_ASSOCIATED_PACKET);

                }

            }

        }


        return $packets;

    }

    private function getFNCLEERBookings($bookings)
    {

        $fncleerbookings = array();

        foreach ($bookings as $requestedReservation) {

            if ($requestedReservation->hotel == "FNCLEER") {

                array_push($fncleerbookings, $requestedReservation->invoice);

            }

        }

        return $fncleerbookings;
    }

    private function mergeBookings($bookings, $duplicateIndexes)
    {

        $mergedSalutati = "";
        $mergedNames = "";
        $mergedAges = array();
        $duplicateIndexesCount = count($duplicateIndexes);
        $age = "";
        for ($i = 0; $i < $duplicateIndexesCount; $i++) {

            //if ($bookings[$duplicateIndexes[$i]]->age != "") {

            array_push($mergedAges, $bookings[$duplicateIndexes[$i]]->age ? $bookings[$duplicateIndexes[$i]]->age : 0);


            // $age = $bookings[$duplicateIndexes[$i]]->age != "" ? $bookings[$duplicateIndexes[$i]]->age : $age;

            $mergedSalutati .= $bookings[$duplicateIndexes[$i]]->salutati; // Merge Salutati
            $separateSalutati = $this->getSeparateSalutati($bookings[$duplicateIndexes[$i]]->salutati);
            $separateNames = explode("/", $bookings[$duplicateIndexes[$i]]->nameFirstName); // Assuming / separator for names

            //if(count($separateSalutati) * 2 >  str_word_count($bookings[$duplicateIndexes[$i]]->nameFirstName)) {
            // If there is more than one person and names aren't separated (more salutati than separate distinguishable names)
            // TO DO, algorithm for same last name
            //}
            if (count($separateSalutati) != count($separateNames)) {

                $mergedNames .= $bookings[$duplicateIndexes[$i]]->nameFirstName;

            } else {
                // Salutati count and names count match
                $j = 0;
                foreach ($separateSalutati as $separateSal) {
                    $mergedNames .= $separateSal[0] . " " . trim($separateNames[$j] . '*' . $bookings[$duplicateIndexes[$i]]->members);
                    // Log::info(' merged members ' . trim($separateNames[$j])  . ' / ' . $bookings[$duplicateIndexes[$i]]->members . PHP_EOL);
                    $j++;
                    if ($j < count($separateSalutati)) {
                        $mergedNames .= "/";
                    }
                }
            }

            if ($i < ($duplicateIndexesCount - 1)) {
                $mergedNames .= "/";
            } // Names separator


        }

        $booking = $bookings[$duplicateIndexes[0]];
        $booking->salutati = $mergedSalutati;
        $booking->nameFirstName = " $mergedNames";
        $booking->age = $mergedAges;

        return $booking;
    }


    private function isPacket($roomName, $obs)
    {

        if (stripos("Privilege-Paket", $roomName) !== false ||
            stripos("Gennuss", $roomName) !== false ||
            stripos("Tauchpaket", $roomName) !== false) {
            return true;
        }

        return false;

    }

    private function convertDate($oldFormat, $newFormat, $date)
    {

        if ($date == "" || $date == 0 || preg_match("/[a-z]/i", $date)) {
            return "";
        }

        //$dateTime = new DateTime();
        $dateobj = Carbon::createFromFormat($oldFormat, $date);

        return $dateobj->toDateString();


    }

    /* getWeekdayLetter - Returns the first letter from the weekday name in portuguese
   * $param date - Given date
   * @return Weekday first letter
  */
    private function getWeekdayLetter($date)
    {


        $weekdayNo = date('w', strtotime($date));

        $weekdaysFirstLetters = array("D", "S", "T", "QT", "QN", "SX", "SB");

        return $weekdaysFirstLetters[$weekdayNo];

    }

    private function formatName($title, $names)
    {

        return $names;

    }

    private function calcularnumerodequartos($requestedReservationsProcessed, $requestedReservation)
    {
        $maxnum = 1;
        $pessoas = 0;
        foreach ($requestedReservationsProcessed as $rnow) {
            if (($rnow->invoice == $requestedReservation->invoice) && ($rnow->hotel == $requestedReservation->hotel) && ($rnow->roomName == $requestedReservation->roomName)) {
                if ($maxnum < $rnow->members) {
                    $maxnum = $rnow->members;
                }
                $pessoas++;
            }
        }
        return intval($pessoas / $maxnum);
    }

    private function createBookings($requestedReservationsProcessed, $requestedReservation)
    {

        //CREATE BOOkING

        $tms = new TMS();
        $tmsId = $requestedReservation->checkIn ? $tms::findTMSId($this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->checkIn)) : NULL;

        $booking_Number = $requestedReservation->invoice;
        $booking_state = $requestedReservation->field32;
        $booking_date = $requestedReservation->field30 ? $this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->field30) : NULL;

        $booking_tmsId = isset($tmsId) ? $tmsId->ID : NULL;
//        $booking_htl = $this->searchField($requestedReservation->hotel, "FNCLEER");


        if ((strlen($requestedReservation->field28) == 8) && (strlen($requestedReservation->field30) != 8)) {
            $booking_date = $requestedReservation->field28 ? $this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->field28) : NULL;  // DATA_RESERVA in Field30 from file

        }

        $booking = new Bookings();

//        if($booking_state == 'BU'){

        $bID = $booking::findBookingbyBookingNumberAndStatus(trim($booking_Number), trim($booking_state));
//        }


        if (empty($bID)) {


            $bookingInfo = $this->bookingInfo($booking_Number, $booking_state, $booking_date, $booking_tmsId);

            $bID = $booking->newBoking($bookingInfo);

            return $bID;

        } else {
            return $bID->id;
        }


    }

    private function createFlights($requestedReservationsProcessed, $requestedReservation, $bookId)
    {

        $arrivalFligh = $requestedReservation->flightNo ? $requestedReservation->flightNo : '';
        $arrivalLocation = $requestedReservation->arrivalLocation ? $requestedReservation->arrivalLocation : NULL;
        $arrivalTime = $requestedReservation->arrivalTime ? $requestedReservation->arrivalTime : NULL;
        $arrivalDate = $requestedReservation->departureDate ? $this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->departureDate) : NULL;


        $departureFlight = $requestedReservation->connectedLoad ? $requestedReservation->connectedLoad : '';
        $departureLocation = $requestedReservation->departureLocation ? $requestedReservation->departureLocation : $requestedReservation->field28;
        $departureDate = $requestedReservation->season ? $this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->season) : NULL;
        $departureTime = $requestedReservation->changed ? $requestedReservation->changed : NULL;


        $flightInfo = $this->flightInfo($bookId, $arrivalFligh, $arrivalLocation, $arrivalDate, $arrivalTime, $departureFlight, $departureLocation, $departureDate, $departureTime);

        $flight = new Flights();

        $flightId = Flights::checkFlight($bookId);


        if (empty($flightId)) {


            $flight->newFlight($flightInfo);

            $flightId = $flight::checkFlight($bookId);


        } else {


            if ($flightId->departure_date !== $requestedReservation->season && stripos($departureFlight, "FNC") === false && $departureFlight !== '') {

                $flightInfo = array(


                    'departureFlight' => $departureFlight,
                    'departureLocation' => $departureLocation,
                    'departureDate' => $departureDate,
                    'departureTime' => $departureTime
                );

                Flights::fixFlightDeparture($flightInfo, $flightId->id);


            }

            if (stripos($arrivalFligh, "FNC") === false && $arrivalFligh !== '') {

                $flightInfo = array(


                    'arrivalFlight' => $arrivalFligh,
                    'arrivalLocation' => $arrivalLocation,
                    'arrivalTime' => $arrivalTime,
                    'arrivalDate' => $arrivalDate

                );

                Flights::fixFlightArrival($flightInfo, $flightId->id);


            }


//            foreach ($requestedReservationsProcessed as $rnow) {
//
//
//                if ($rnow->invoice == $requestedReservation->invoice && !empty($flightId)) {
//
//                    if ($flightId->departure_flight !== $rnow->connectedLoad)
//
//
//
//
//                }
//            }

        }
        return $flightId;
    }

    private function createClients($requestedReservationsProcessed, $requestedReservation, $bookId)
    {

    }

    private function createRooms($requestedReservationsProcessed, $requestedReservation, $bookId, $flightId, $txc)
    {


        $book = $bookId;


        $room = new Rooms();
        $hotel = new Hotels();

        $client = new Clients();
        $client_name = utf8_encode($requestedReservation->nameFirstName);
        $client_age = empty($requestedReservation->age) ? 0 : $requestedReservation->age;
        $gender = substr($requestedReservation->salutati, 0, 1);
        $client_member = $requestedReservation->members;


//        if(count($requestedReservationsProcessed) == 1){
//            Log::info( $requestedReservationsProcessed );
//
//        }


        if ($requestedReservation->hotel !== 'FNC510' && $requestedReservation->hotel !== 'FNC511' && $requestedReservation->roomName !== 'Transfer ab/bis Flughafe' && $requestedReservation->roomName !== "Tribünenkarte Blumenfest" && ($requestedReservation->cate !== 'W') && empty($roomId) && ($requestedReservation->cate !== 'X')) {


//            foreach (explode("/", $requestedReservation->nameFirstName) as $Person) {


//                $ageIndex++;
            $rooms = Rooms::getRoomsBookIdRoomName($book, $requestedReservation->roomName);


            $clientId = $client::checkClient($bookId, $client_name);


            if ($client_member == '1' && empty($clientId)) {

                $xc = $this->addRoom($requestedReservation, $hotel, $bookId, '3');


            } elseif ($client_member == '1' && !empty($clientId)) {

                $xc = $this->addRoom($requestedReservation, $hotel, $bookId, '4');

                $room->newClientRoom($clientId->id, $xc, $bookId);


            } elseif ($client_member !== '1' && empty($clientId)) {

                $clientInfo = array(
                    'bookId' => $bookId,
                    'flightId' => $flightId ? $flightId->id : null,
                    'roomId' => $txc ? $txc : null,
                    'name' => $client_name,
                    'gender' => $gender,
                    'age' => $client_age

                );
                $clientId = $client->newClient($clientInfo);


            } elseif ($client_member !== '1' && !empty($clientId)) {
                $room->newClientRoom($clientId->id, $txc, $bookId);
            }


            if (empty($clientId)) {


                $clientInfo = array(
                    'bookId' => $bookId,
                    'flightId' => $flightId ? $flightId->id : null,
                    'roomId' => $xc ? $xc : null,
                    'name' => $client_name,
                    'gender' => $gender,
                    'age' => $client_age

                );

                $clientId = $client->newClient($clientInfo);


                //Clients::addClientRoom($newClientId,$bookId);


            }

//            }
        }


        return isset($xc) ? $xc : $txc;


    }

    private function createTransfers($requestedReservationsProcessed, $requestedReservation, $bookId, $oldType)
    {


        foreach ($requestedReservationsProcessed as $rnow) {

            if (($requestedReservation->invoice == $rnow->invoice) && ($requestedReservation->field32 == $rnow->field32)) {
                $fncleerBookings = $this->getFNCLEERBookings($requestedReservationsProcessed);
                $transferType = $this->getTransfer($rnow->field33, $rnow->flightNo, $rnow->connectedLoad, $rnow->hotelName, $rnow->hotel, $rnow->invoice, $fncleerBookings, $bookId);
                $chkTransfer = Transfers::checkingTransfer($bookId);

                $transfers = new Transfers();
                if (empty($chkTransfer)) {


                    $oldType = $transferType;


                    $transferInfo = $this->transferInfo($bookId, $transferType);
                    $transfers->newTransfer($transferInfo);


                } else {
                    $transfers->updateImportedTransfer($chkTransfer->id, $transferType);
                }


            }


        }


    }

    private function createPackages($requestedReservationsProcessed, $requestedReservation, $bookId)

    {


        foreach ($requestedReservationsProcessed as $rnow) {
            if ($requestedReservation->invoice == $rnow->invoice && $requestedReservation->field32 == $rnow->field32 && $requestedReservation->nameFirstName == $rnow->nameFirstName) {

                $package = new Packages();
                $clientId = NULL;
                $client = new Clients();

                // $client_name = $rnow->nameFirstName;
                $client_name = utf8_encode($rnow->nameFirstName);
                $client_age = empty($rnow->age) ? 0 : $rnow->age;
                $gender = substr($rnow->salutati, 0, 1);


                // Log::info(' BOOKING : ' . $rnow->invoice . ' HOTEL: ' . $rnow->hotel .   PHP_EOL);

                $acheck = $rnow->hotel ? $rnow->hotel[strlen($rnow->hotel) - 1] : '';
                $packageId = Clients::hasPackage($bookId, $clientId);
                // Log::info($rnow->roomName . ' // '. $rnow->field33 . ' acheck: ' . $acheck . ' pack: ' . $packageId.   PHP_EOL);
                if (empty($packageId) || ($acheck == "A") || ($acheck == "O")) {


                    $clientInfo = array(
                        'bookId' => $bookId,
                        'flightId' => null,
                        'roomId' => null,
                        'name' => $client_name,
                        'gender' => $gender,
                        'age' => $client_age

                    );


                    $clientId = Clients::checkClient($bookId, utf8_encode($client_name)) ? Clients::checkClient($bookId, utf8_encode($client_name))->id : $client->newClient($clientInfo);
                    $packageId = Clients::hasPackage($bookId, $clientId) ? Clients::hasPackage($bookId, $clientId)->id : null;
                    $price = $rnow->field31 ? $rnow->field31 : NULL;
                    $notes = isset($rnow->field33) ? utf8_encode($rnow->field33) : "";
                    $days = $this->getDaysFromDaysOpt($rnow->days) ? $this->getDaysFromDaysOpt($rnow->days) : 0;
                    $state = $rnow->field32 ? $rnow->field32 : null;
                    $hotel = new Hotels();
                    $hotelId = $hotel->findHotel($rnow->hotel);
                    $tuachenNrPax = $rnow->members;
                    $tauchenQty = $rnow->noRooms;
                    $checkin = $this->convertDate('d.m.y', 'd-m-Y', $rnow->checkIn);
                    $checkout = $this->convertDate('d.m.y', 'd-m-Y', $rnow->checkOut);


                    if (empty($packageId) && !empty($clientId)) {

                        if (empty($packageId) && !empty($clientId)) {

                            if ($rnow->hotel == 'FNC501') {

                                $wanderType = $this->getWanderType($rnow->hotel);

                                $package->newPackage($bookId, $wanderType, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);

                            } elseif ($this->searchField($rnow->field33, "Wander") || $this->searchField($rnow->field33, "Wander-paket") || $this->searchField($rnow->hotelName, "Wander") || $this->searchField($rnow->hotelName, "Wanderk")) {


                                $wanderType = $this->getWanderType($rnow->hotel);

                                $package->newPackage($bookId, $wanderType, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);


                            } elseif ($this->searchField($rnow->field33, "Wellness") || ($rnow->catering == "Wellness") || $this->searchField($rnow->hotelName, "Wellnesszuschlag") || $this->searchField($rnow->roomName, "Wellness")) {


                                $package->newPackage($bookId, 5, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);

                            } elseif ((trim($rnow->roomName) == "Genuss-Paket") || $this->searchField($rnow->field33, "Packet") || $this->searchField($rnow->field33, "Genuss-Paket")) {


                                $package->newPackage($bookId, 4, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);


                            } elseif ((trim($rnow->roomName) == "Tribünenkarte Blumenfest") || $this->searchField($rnow->field33, "Tribünenkarte Blumenfest") || ($rnow->hotel == 'FNC511') || ($rnow->hotel == 'FNC510')) {


                                $package->newPackage($bookId, 8, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);


                            } elseif ((trim($rnow->roomName) == "Privilege") || (trim($rnow->roomName) == "Privilege-Paket")) {


                                $package->newPackage($bookId, 11, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);

                            } elseif ((trim($rnow->roomName) == "Tauchen") || ((trim($rnow->roomName) == "Tauchpaket")) && ($this->searchField($rnow->hotelName, "Tauchen"))) {


                                $package->newPackage($bookId, 12, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);


                            } elseif (substr_count($rnow->field33, 'Hochsaisonzuschlag') > 0) {

                                for ($i = 1; $i <= substr_count($rnow->field33, 'Hochsaisonzuschlag'); $i++) {


                                    $package->newPackage($bookId, 9, $clientId, $tuachenNrPax, $tauchenQty, $price, $notes, $days, $state, $hotelId ? $hotelId->id : 190, $checkin, $checkout);
                                }


                            }
                        }
//                    else{
//
//                        Packages::updatePackage($packageId,$price);
//
//                    }

                    }


                }
            }


        }

    }

    private function createCarForBooking($requestedReservations, $reservation)
    {
        /* Not working as expected */

        //
        //Log::info("Trying to create Car");
        //$bookingInvoiceNumber = $reservation->invoice;
        //$bookingState = $reservation->field32;
        //
        //Log::info("InvoiceNumber: " . $bookingInvoiceNumber);
        //Log::info("State: " . $bookingState);
        //
        //
        //$booking = Bookings::findBookingbyBookingNumberAndStatus(trim($bookingInvoiceNumber), trim($bookingState));
        //if (empty($booking)) {
        //    Log::info("Booking não existe");
        //    $tmsId = $reservation->checkIn ? TMS::findTMSId($this->convertDate('d.m.y', 'd-m-Y', $reservation->checkIn)) : NULL;
        //
        //    $bookingDate = $reservation->field30 ? $this->convertDate('d.m.y', 'd-m-Y', $reservation->field30) : NULL;
        //    $bookingTmsId = isset($tmsId) ? $tmsId->ID : NULL;
        //
        //    $bookingInfo = $this->bookingInfo($bookingInvoiceNumber, $bookingState, $bookingDate, $bookingTmsId);
        //
        //    $booking = new Bookings();
        //    $bookingId = $booking->newBoking($bookingInfo);
        //    Log::info("Novo Booking ID: " . $bookingId);
        //} else {
        //    $bookingId = $booking->id;
        //    Log::info("Booking ID: " . $bookingId);
        //}
        //
        //$this->createCar($requestedReservations, $reservation, $bookingId);

    }

    private function createCar($requestedReservationsProcessed, $requestedReservation, $bookId)
    {

        foreach ($requestedReservationsProcessed as $rnow) {

            if ($rnow->hotel == "FNC700" && $requestedReservation->invoice == $rnow->invoice && $requestedReservation->field32 == $rnow->field32) {


                $carPrice = preg_match('/^[0-9]+(\\.[0-9]+)?$/', $rnow->field31) ? $rnow->field31 : $rnow->field29;
                $cartype = $rnow->room ? $rnow->room : NULL;
                $carConfirm = false;
                $carNotes = isset($rnow->field33) ? utf8_encode($rnow->field33) : "";
                $rentaCarId = 0;
                $initDate = $rnow->checkIn ? $this->convertDate('d.m.y', 'd-m-Y', $rnow->checkIn) : NULL;

                $endDAte = $rnow->checkOut ? $this->convertDate('d.m.y', 'd-m-Y', $rnow->checkOut) : NULL;


                $car = new Cars();
                $carId = $car::checkCar($bookId);

                if (empty($carId)) {

                    $carInfo = $this->carsInfo($bookId, $rentaCarId, $cartype, $initDate, $endDAte, $carNotes, $carPrice);
                    $car->newCar($carInfo);

                }

            }
//
        }

    }

    private function fixFlightNumber($flight, $location, $date, $time, $flightId)
    {


        $arrivalFlight = $flight;
        $arrivalLocation = $location;
        $arrivalDate = $date;
        $arrivalTime = $time;


    }


    private function getTransfer($obs, $flightChegadaNo, $flightSaidaNo, $hotelName, $hotelCode, $bookingNo, $fncleerBookings, $bookId)
    {


        $transferRoom = Rooms::checkRoomTransfer($bookId);
        $transferPackage = Packages::checkPackageTransfer($bookId);
        $flight = Flights::checkFlight($bookId);


        if ($transferRoom->count() > 0 || $transferPackage->count() > 0) {
            return "IN+OUT";
        } elseif ($hotelName == 'Transfer') {
            return "IN+OUT";
        } else if (empty($flight->arrival_flight) && !empty($flight->departure_flight)) {
            //  Log::info( $bookingNo . ' -  ' . 'Verificar' . PHP_EOL );
            return "Verificar";
        } else if (!empty($flight->arrival_flight) && empty($flight->departure_flight)) {
            //    Log::info( $bookingNo . ' -  ' . 'Verificar 1' . PHP_EOL );
            return "Verificar";
        } else if (empty($flight->arrival_flight) && empty($flight->departure_flight)) {

            return "NaoTem";
        } else if (stripos($hotelCode, "FNC015A") !== false) {
            // Log::info( $bookingNo . ' -  ' . 'Não tem 1' . PHP_EOL );
            return "NaoTem";
        } else if (stripos($hotelCode, "FNC015O") !== false) {
            //  Log::info( $bookingNo . ' -  ' . 'Não tem 2' . PHP_EOL );
            return "NaoTem";
        } else if (stripos($hotelName, "PKW") !== false) {
            // Log::info( $bookingNo . ' -  ' . 'Não tem 3' . PHP_EOL );
            return "NaoTem";
        } else if (in_array($bookingNo, $fncleerBookings)) {
            // Log::info( $bookingNo . ' -  ' . 'Verificar 2' . PHP_EOL );
            return "Verificar";
        } else if ($hotelCode != '') {
            if ($hotelCode[strlen($hotelCode) - 1] == "O") {
                //    Log::info( $bookingNo . ' -  ' . 'Não tem 4' . PHP_EOL );
                return "NaoTem";
            }
            if ($hotelCode[strlen($hotelCode) - 1] == "A") {
                //   Log::info( $bookingNo . ' -  ' . 'Não tem 5' . PHP_EOL );
                return "NaoTem";
            }


        } else {
            //   Log::info( $bookingNo . ' -  ' . 'Verificar 3' . PHP_EOL );
            return "Verificar";
        }


    }

    private function addRoom($requestedReservation, $hotel, $bookId, $x)
    {
        $room = new Rooms();
        $hotelGuia = $hotel->findHotel($requestedReservation->hotel) ? $hotel->findHotel($requestedReservation->hotel)->id : 190;
        $price = preg_match('/^[0-9]+(\\.[0-9]+)?$/', $requestedReservation->field31) ? $requestedReservation->field31 : $requestedReservation->field29;
        $notes = $requestedReservation->field33 !== '' ? utf8_encode($requestedReservation->field33) : utf8_encode($requestedReservation->field31);
        $checkIn = $requestedReservation->checkIn ? $this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->checkIn) : NULL;
        $checkOut = $requestedReservation->checkOut ? $this->convertDate('d.m.y', 'd-m-Y', $requestedReservation->checkOut) : NULL;
        $nights = $this->getDaysFromDaysOpt($requestedReservation->days) ? $this->getDaysFromDaysOpt($requestedReservation->days) : 0;
        $roomType = $requestedReservation->roomName ? $requestedReservation->roomName : NULL;
        $roomCode = $requestedReservation->room . " " . $requestedReservation->cate ? $requestedReservation->room . " " . $requestedReservation->cate : NUll;
        $checkIn_day = $checkIn ? $this->getWeekdayLetter($checkIn) : NULL;
        $essen = $requestedReservation->cate;
        $catering = utf8_encode($requestedReservation->catering);
        $member = $requestedReservation->members;


        //clock($fmember . ' // ' . $rnow->members);


        if (is_numeric($notes)) {
            $notes = $requestedReservation->field33;
        }


        $roomInfo = $this->roomInfo($bookId, $hotelGuia, 'a', $checkIn, $checkOut, $nights, $roomType, $roomCode, $checkIn_day, $notes, $price, $essen, $catering, $member);

        $newRoomId = $room->newRoom($roomInfo);


        return $newRoomId;
    }


    private function getExtras()
    {

        //TO DO

    }

    private function searchField($string, $word)
    {

        return stripos($string, $word) ? true : false;

    }


    private function getTipAuto($hotelName, $hotelCode, $room)
    {

        $tipAuto = "";
        $tipAuto = stripos($hotelName, "MW: Avia Car + Five") || $hotelCode == "FNC700" ? $room : "";

        return $tipAuto;

    }

    private function getDaysFromDaysOpt($daysOpt)
    {

        return explode(" ", $daysOpt)[0]; // Fix DaysOpt, two fields that are together in file

    }

    private function isDBPacket($reserva)
    {

        $pacotesModel = TableRegistry::get('Pacotes');
        $tabelasModel = TableRegistry::get('Tabelascampos');

        $result = array();
        $bookingDate = $this->changeSeparator("/", "-", $reserva->DATA);
        $bookingDate = $this->invertDate("-", $reserva->DATA);

        $packetsInDate = $pacotesModel->find('all', ['conditions' =>
            ['Pacotes.DATA_INICIO <' => $bookingDate,
                'Pacotes.DATA_FIM >' => $bookingDate]
        ])->toArray();
        //$packetsInDate = $pacotesModel->find('all')->toArray();

        foreach ($packetsInDate as $pack) {

            /*$searchBookingField = $tabelasModel->get($pack->tabelascampos_id_SEARCH_KEYWORD_FIELD);
            $keyWordSearch = $pack->SEARCH_KEYWORD_RESERVA;
            $writeBookingField = $tabelasModel->get($pack->tabelascampos_id_ASSOCIATED_PACKET);
            $keyWordWrite = $pack->DEFAULT_ASSOCIATED_PACKET;
            if(stripos($reserva[$writeBookingField->DATA_INDEX], $keyWordSearch) !== false) {
                array_push($result, array("ASSOCIATEDFIELD" => $writeBookingField->DATA_INDEX, "KEYWORD" => $keyWordWrite));
            }*/
        }


        return $result;
    }

    private function updateRentacars($rentacarlist, $recordlist)
    {


        foreach ($rentacarlist as $rentacar) {

            $recordToUpdateID = $this->getRecordIndexByUniqueKeys($rentacar, $recordlist);

            if ($recordToUpdateID != -1) {

                $recordlist[$recordToUpdateID]->RENT_A_CAR = true;
                if (!isset($recordlist[$recordToUpdateID]->OBS_AUTO) || $recordlist[$recordToUpdateID]->OBS_AUTO == "") {
                    $recordlist[$recordToUpdateID]->OBS_AUTO = isset($rentacar->field33) ? $rentacar->field33 : "";
                }
                $recordlist[$recordToUpdateID]->TIP_AUTO = $this->getTipAuto($rentacar->hotelName, $rentacar->hotel, $rentacar->room);

            }

        }

        return $recordlist;


    }

    private function countHocsaison($obs)
    {

        return substr_count($obs, "Hochsaisonzuschlag");

    }

    private function invertDate($separator, $date)
    {

        $dateParts = explode($separator, $date);
        if (count($dateParts) > 1) {
            return $dateParts[2] . $separator . $dateParts[1] . $separator . $dateParts[0];
        }
        return $date;
    }

    private function ordenararray($recordToCompare)
    {
        $a = array();
        if (sizeof($recordToCompare) > 0) {
            foreach ($recordToCompare as $ln) {
                $x = array($ln->prioridade, $ln);
                array_push($a, $x);
            }
            asort($a);
            $xa = array();
            foreach ($a as $ln) {
                array_push($xa, $ln[1]);
            }
            return $xa;
        } else {
            return $recordToCompare;
        }

    }

    private function getRecordIndexByUniqueKeys($recordToCompare, $recordlist)
    {

        $index = -1;
        $counter = 0;

        foreach ($recordlist as $record) {

            if (isset($record["NR_RESERVA"]) && $record["NR_RESERVA"] == $recordToCompare->invoice) {
                return $counter;
            }

            $counter++;

        }

        return $index;

    }

    private function updateDynamicPackets($dynamicPackets, $recordlist)
    {


        foreach ($dynamicPackets as $packet) {

            $recordToUpdateID = $this->getRecordIndexByUniqueKeys($packet[0], $recordlist);
            if ($packet[1] == 34) {
                $recordlist[$recordToUpdateID]["GENUSS"] = true;
            }

        }

        return $recordlist;

    }

    private function getSeparateSalutati($mixedSalutati)
    {

        $salutatiList = array('FRAU', 'HERR', 'KIND', 'INF', 'INF.');

        $salutatis = array();
        if (!strlen($mixedSalutati)) {
            return $salutatis;
        }

        $loop = false;
        while (strlen($mixedSalutati) && !$loop) {

            $i = 0;
            $salutatiRecovered = false;
            foreach ($salutatiList as $sal) {

                if (stripos($mixedSalutati, $sal) === 0) {
                    array_push($salutatis, $sal);
                    $mixedSalutati = $this->str_replace_first($sal, "", $mixedSalutati);
                    $salutatiRecovered = true;
                }
                $i++;
            }

            if (!$salutatiRecovered) {
                $loop = true;
            }
        }

        return $salutatis;

    }

    private function str_replace_first($from, $to, $subject)
    {
        $from = '/' . preg_quote($from, '/') . '/';

        return preg_replace($from, $to, $subject, 1);
    }


}